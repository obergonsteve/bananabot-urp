﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;


[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Collider2D))]		// to detonate (by player)
[RequireComponent(typeof(AudioSource))]   

public class Detonator : MonoBehaviour
{
	public SpriteRenderer Plunger;			// pushed down by animation
	public AudioClip PlungerAudio;			// on player collision
	public AudioClip DetonateAudio;			// plunger down
	public Light2D BackgroundGlow;

	private string DetonateTrigger = "Detonate";        // animation
	private bool detonated = false;

	private Bomb ParentBomb;
	private Animator animator;					// sprite animation
	private AudioSource audioSource;

	private Vector2 plungerStart;

	private void Awake()
	{
		ParentBomb = GetComponentInParent<Bomb>();
		animator = GetComponent<Animator>();
		audioSource = GetComponent<AudioSource>();

		detonated = false;
		plungerStart = Plunger.transform.localPosition;
	}

	// animate plunger on collision with player
	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (detonated)
			return;

		if (collision.gameObject.CompareTag("Player"))
			Detonate();
	}

	private void Detonate()
	{
		GameEvents.OnBombStart?.Invoke(ParentBomb, true);

		detonated = true;
		animator.SetTrigger(DetonateTrigger);   // FireFuse() event on pluger down

		if (PlungerAudio != null)
		{
			audioSource.clip = PlungerAudio;
			audioSource.Play();
		}
	}

	// animation event (on plunger down)
	public void FireFuse()
	{
		StartCoroutine(ParentBomb.StartFuse());         // fuse

		if (DetonateAudio != null)
		{
			audioSource.clip = DetonateAudio;
			audioSource.Play();
		}

		Glow(false);
	}

	public void Reset()
	{
		detonated = false;
		Plunger.transform.localPosition = plungerStart;
		Glow(true);
	}

	public void Glow(bool on)
	{
		if (BackgroundGlow != null)
			BackgroundGlow.enabled = on;
	}
}
