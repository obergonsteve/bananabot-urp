﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


[RequireComponent(typeof(AudioSource))]

public class GameManager : MonoBehaviour
{
	public LevelList LevelList;     // scenes

	//public GameLevel CurrentLevel => LevelList.CurrentLevel;
	public GameScores LevelScores => LevelList.CurrentLevel != null ? LevelList.CurrentLevel.LevelScores : null;

	public enum GameState
	{
		NotStarted = 0,
		ReadyToStart,       // 'tap to play!'
		InPlay,             // in air / on platform
		InWater,            // if not game over	
		Paused,             // by the player
		GameOver            // no health / fuel / time left
	}
	public GameState CurrentState;

	public enum GameOverState
	{
		None = 0,
		ZeroFuel,
		ZeroHealth,  
		ZeroTime
	}

	public AudioClip TimeTickAudio;
	public float TimeLimit;                 // seconds
	private float gameTimeRemaining = 0;        // seconds
	private float timerInterval = 1f;       // seconds
	private Coroutine timeCountdownCoroutine;
	private bool timerRunning = false;

	//public bool OptionsShowing;

	private DateTime focusLostTime;
	private DateTime pauseStartTime;

	public TimeSpan pausedTime = TimeSpan.Zero;     // lost focus + paused

	private string introScene = "IntroScene";       // list of scenes / levels

	private AudioSource audioSource;

	public void OnEnable()
	{
		GameEvents.OnPlayerTap += OnPlayerTap;
		GameEvents.OnPlayerTakeFruit += OnPlayerTakeFruit;
		GameEvents.OnUpdateGameScore += OnUpdateGameScore;

		GameEvents.OnZeroHealth += OnZeroHealth;
		GameEvents.OnZeroFuel += OnZeroFuel;
		GameEvents.OnZeroTime += OnZeroTime;
		//GameEvents.OnGameOptions += OnGameOptions;
		GameEvents.OnStartNewGame += StartNewGame;

		GameEvents.OnSceneQuit += OnSceneQuit;
	}

	public void OnDisable()
	{
		GameEvents.OnPlayerTap -= OnPlayerTap;
		GameEvents.OnPlayerTakeFruit -= OnPlayerTakeFruit;
		GameEvents.OnUpdateGameScore -= OnUpdateGameScore;

		GameEvents.OnZeroHealth -= OnZeroHealth;
		GameEvents.OnZeroFuel -= OnZeroFuel;
		GameEvents.OnZeroTime -= OnZeroTime;
		//GameEvents.OnGameOptions -= OnGameOptions;
		GameEvents.OnStartNewGame -= StartNewGame;

		GameEvents.OnSceneQuit -= OnSceneQuit;
	}


	// disable Debug.Log messages when not building for the Unity editor (for performance)
	private void Awake()
	{
#if UNITY_EDITOR
		Debug.unityLogger.logEnabled = true;
#else
		Debug.unityLogger.logEnabled = false;
#endif
		LeanTween.init(3200);
	}

	private void Start()
	{
		audioSource = GetComponent<AudioSource>();      // required component

		SetState(GameState.ReadyToStart);
	}

	// player tap used to start play as well as swipes
	private void OnPlayerTap(GameState gameState, int tapcount)
	{
		switch (gameState)
		{
			case GameState.NotStarted:
				break;

			case GameState.ReadyToStart:
				NewGame();
				break;

			case GameState.InPlay:
				break;

			case GameState.InWater:
				//BeatEvents.OnGameRestart?.Invoke();
				break;

			case GameState.Paused:
				break;

			case GameState.GameOver:
				//StartNewGame();
				break;
		}
	}


	private void NewGame()
	{
		if (LevelScores != null)     // level selected via IntroScene
		{
			LevelScores.PlayCount++;
			LevelScores.GameReset();
		}

		StopTimer();

		GameEvents.OnScoreReset?.Invoke();

		SetState(GameState.InPlay);
		GameEvents.OnTapToPlay?.Invoke(LevelScores);

		gameTimeRemaining = TimeLimit;
		GameEvents.OnTimeRemainingChanged?.Invoke(0, gameTimeRemaining, TimeLimit, false);
		timeCountdownCoroutine = StartCoroutine(TimeCountdown());
	}

	private void StartNewGame()
	{
		GameEvents.OnNewGameStarted?.Invoke(ReloadScene);
	}

	private void ReloadScene()
	{
		Scene currentScene = SceneManager.GetActiveScene();
		SceneManager.LoadScene(currentScene.name);
	}

	private void SetState(GameState newState)
	{
		if (CurrentState == newState)
			return;

		//Debug.Log("GameManager.SetState " + newState);

		GameEvents.OnGameStateChanged?.Invoke(CurrentState, newState);
		CurrentState = newState;
	}

	// increase / decrease game time remaining
	private void OnPlayerTakeFruit(float fruitFuel, float fruitHealth, bool isRotten, float fruitToxicity, float timeIncrease, bool isToxic,
																		CardManager.CardBackType cardBackType, Color scoreColour, Vector3 fruitPosition, string scoreMessage)
	{
		//Debug.Log($"OnPlayerTakeFruit: fruitFuel = {fruitFuel}, fruitHealth = {fruitHealth}, fruitToxicity = {fruitToxicity}");

		if (isToxic || isRotten)
			timeIncrease = -timeIncrease;

		// secondsRemaining can be negative (eg. red fireballs)
		if (Mathf.Abs(timeIncrease) > 0)
		{
			gameTimeRemaining += timeIncrease;
			gameTimeRemaining = Mathf.Clamp(gameTimeRemaining, 0f, TimeLimit);

			GameEvents.OnTimeRemainingChanged?.Invoke(timeIncrease, gameTimeRemaining, TimeLimit, true);
		}

		if (gameTimeRemaining <= 0f)
		{
			gameTimeRemaining = 0f;
			GameEvents.OnZeroTime?.Invoke();
		}
	}

	private IEnumerator TimeCountdown()
	{
		if (timerRunning)
			yield break;

		timerRunning = true;

		while (timerRunning)
		{
			if (CurrentState == GameState.GameOver)
			{
				timerRunning = false;
				yield break;
			}

			yield return new WaitForSeconds(timerInterval);

			gameTimeRemaining -= timerInterval;
			GameEvents.OnTimeRemainingChanged?.Invoke(-timerInterval, gameTimeRemaining, TimeLimit, true);

			//if (TimeTickAudio != null)
				//AudioSource.PlayClipAtPoint(TimeTickAudio, Vector3.zero, AudioManager.SFXVolume);

			if (TimeTickAudio != null)
			{
				audioSource.clip = TimeTickAudio;
				audioSource.Play();
			}

			if (gameTimeRemaining <= 0f)
			{
				gameTimeRemaining = 0f;
				GameEvents.OnZeroTime?.Invoke();
			}
		}

		yield return null;
	}

	private void StopTimer()
	{
		if (timeCountdownCoroutine != null)
		{
			StopCoroutine(timeCountdownCoroutine);
			timeCountdownCoroutine = null;
		}
		timerRunning = false;
	}

	private void OnZeroHealth()
	{
		GameOver(GameOverState.ZeroHealth);
	}

	private void OnZeroFuel()
	{
		GameOver(GameOverState.ZeroFuel);
	}

	private void OnZeroTime()
	{
		GameOver(GameOverState.ZeroTime);
	}

	private void GameOver(GameOverState state)
	{
		StopTimer();

		if (CurrentState == GameState.GameOver)
			return;

		SetState(GameState.GameOver);
		//playEndTime = DateTime.Now;
		bool newPB = false;

		if (LevelScores != null)     // level selected properly via IntroScene
		{
			LevelScores.SetGamePlayTime(pausedTime);
			newPB = LevelScores.UpdatePersonalBest();
		}

		GameEvents.OnFinalGameScore?.Invoke(LevelScores, newPB, state);

		if (newPB)
			GameEvents.OnNewPersonalBest?.Invoke(LevelScores, false);
	}


	// Game scores only updated with positive values (never decreases)
	private void OnUpdateGameScore(float healthChange, float fuelChange, float timeChange, int scoreFactor, Color scoreColour, Vector3 fruitPosition, string scoreMessage)
	{
		if (CurrentState == GameState.GameOver)
			return;

		if (LevelScores == null)     // level not selected properly
			return;

		if (healthChange > 0)
			LevelScores.GameHealthGained += healthChange;

		if (fuelChange > 0)
			LevelScores.GameFuelGained += fuelChange;

		if (timeChange > 0)
			LevelScores.GameTimeGained += timeChange;

		float bonusScore = 0;

		if (scoreFactor > 1)
		{
			int bonusFactor = scoreFactor - 1;		// already accounted for in scores above

			bonusScore = (healthChange * bonusFactor) + (fuelChange * bonusFactor) + (timeChange * bonusFactor);
			if (bonusScore > 0)
				LevelScores.GameBonusScore += bonusScore;
		}

		float scoreChange = healthChange + fuelChange + timeChange + bonusScore;
		//Debug.Log($"OnUpdateGameScore scoreFactor {scoreFactor}, scoreChange {scoreChange}, scoreMessage {scoreMessage}");

		// negative changes only affect health, fuel and time ... not score
		if (scoreChange < 0)
			scoreChange = 0;

		GameEvents.OnGameScoreUpdated?.Invoke(LevelScores.RunningScore, (int)scoreChange, scoreColour, fruitPosition, scoreMessage);
	}


	//private void OnGameOptions(bool show)
	//{
	//	OptionsShowing = show;
	//}

	private void OnSceneQuit()
	{
		SceneManager.LoadScene(introScene);
	}


	// TODO: do these get called at app startup?
	private void OnApplicationFocus(bool focus)
	{
		//if (!focus)
		//	focusLostTime = DateTime.Now;
		//else
		//	pausedTime += DateTime.Now.Subtract(focusLostTime);
	}

	private void OnApplicationPause(bool paused)
	{
		//if (paused)
		//	pauseStartTime = DateTime.Now;
		//else
		//	pausedTime += DateTime.Now.Subtract(pauseStartTime);
	}

	private void OnApplicationQuit()
	{
		//GameSettings.LogoTreeAnimationPlayed = false;
	}
}
