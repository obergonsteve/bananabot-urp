﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
	public Fruit FruitPrefab;

	[Space]
	public float MaxFuel = 10f;				// initial fuel - added to player on hit - 'rots' down
	public float MaxHealth = 10f;           // initial health - added to player on hit- 'rots' down

	[Space]
	public bool IsToxic = false;        // if true, reduces health, fuel, time
	public Color OverrideColour = Color.clear;

	[Header("Time Fruit")]
	public float ClockTime;				// initial time added to player on hit - counts down
	[Tooltip("Multiplied by fruit's remaining time, when taken by player")]
	public float TimeFactor = 2f;       // multiplied by remaining time when fruit taken by player

	[Space]
	public Vector2 LaunchForce;
	public float LaunchTorque = 100f;

	[Range(0f, 1f)]
	public float GravityScale = 0f;

	[Header("Spawning")]
	public bool RandomSpawn = false;     // random spawn intervals and quantities

	[Header("Random spawning")]
	public float MinRandomDelay = 0.5f;       // seconds to first spawn
	public float MaxRandomDelay = 5f;       // seconds to first spawn
	public float MinRandomInterval = 1f;    // seconds between spawns
	public float MaxRandomInterval = 10f;    // seconds between spawns
	public int MinRandomQuantity = 1;       // qty of prefab to spawn each time
	public int MaxRandomQuantity = 3;       // qty of prefab to spawn each time

	private float RandomSpawnDelay => UnityEngine.Random.Range(MinRandomDelay, MaxRandomDelay);
	private float RandomSpawnInterval => UnityEngine.Random.Range(MinRandomInterval, MaxRandomInterval);
	private float RandomSpawnQuantity => UnityEngine.Random.Range(MinRandomQuantity, MaxRandomQuantity);

	[Header("Non-random spawning")]
	public float SpawnDelay = 1f;		// seconds to first spawn
	public float SpawnInterval = 1f;    // seconds between spawns
	public int SpawnQuantity = 1;       // qty of prefab to spawn each time

	private bool spawning = false;

	[Header("Optional 'parent' spawner (attached to)")]
	public AttachedSpawner AttachedTo;      // this spawn point follows another object (eg. cloud) but is not a child of it
	//public Transform SpawnedFruit;			// optional folder to parent spawned fruit (eg. so not parented to moving SpawnPoint)

	private void OnEnable()
	{
        GameEvents.OnTapToPlay += OnNewGame;
        GameEvents.OnGameStateChanged += OnGameStateChanged;
	}

    private void OnDisable()
    {
        GameEvents.OnTapToPlay -= OnNewGame;
		GameEvents.OnGameStateChanged -= OnGameStateChanged;
	}


	private void Update()
	{
		if (AttachedTo != null)
			transform.position = AttachedTo.transform.position;
	}


	private void OnNewGame(GameScores scores)
	{
		//if (! RandomSpawn)       // spawned via FruitSpawner
			StartCoroutine(StartSpawning());
	}

	private void OnGameStateChanged(GameManager.GameState currentState, GameManager.GameState newState)
	{
		if (newState == GameManager.GameState.GameOver)
			spawning = false;
	}


	private IEnumerator StartSpawning()
	{
		if (spawning)
			yield break;

		spawning = true;

		while (spawning)
		{
			yield return new WaitForSeconds(RandomSpawn ? RandomSpawnDelay : SpawnDelay);

			var spawnQty = RandomSpawn ? RandomSpawnQuantity : SpawnQuantity;

			for (int i = 0; i < spawnQty; i++)
			{
				SpawnFruit();
			}

			yield return new WaitForSeconds(RandomSpawn ? RandomSpawnInterval : SpawnInterval);
		}

		yield return null;
	}

	private void SpawnFruit()
	{
		// parent new fruit to parent folder to avoid parenting to a moving SpawnPoint - eg. attached to cloud
		var newFruit = Instantiate(FruitPrefab, (transform.parent != null) ? transform.parent.transform : transform);

		if (transform.parent != null)
			newFruit.transform.position = transform.position;

		newFruit.Spawn(this);
	}

	public void StopSpawning()
	{
		spawning = false;
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere(transform.position, 1f);

		// draw connection to a 'parent' object (eg. cloud's AttachedSpawner)
		if (AttachedTo != null)
			Gizmos.DrawLine(transform.position, AttachedTo.transform.position);
	}
}
