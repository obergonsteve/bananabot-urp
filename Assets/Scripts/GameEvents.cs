﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvents
{
    // scenes / levels

    public delegate void OnSceneSelectedDelegate(GameLevel level);
    public static OnSceneSelectedDelegate OnSceneSelected;

    public delegate void OnSceneLoadedDelegate(GameLevel level);
    public static OnSceneLoadedDelegate OnSceneLoaded;

    public delegate void OnSceneQuitDelegate();
    public static OnSceneQuitDelegate OnSceneQuit;

    // game / health / fuel / time

    public delegate void OnGameStateChangedDelegate(GameManager.GameState currentState, GameManager.GameState newState);
    public static OnGameStateChangedDelegate OnGameStateChanged;

    public delegate void OnStartPlayDelegate();
    public static OnStartPlayDelegate OnStartPlay;          // on 'tap to play!' (first tap)

    public delegate void OnPlayerHealthFuelChangedDelegate(float healthChange, float healthLevel, float maxHealth, float fuelChange, float fuelLevel, float maxFuel, bool pulse);
    public static OnPlayerHealthFuelChangedDelegate OnPlayerHealthFuelChanged;

    public delegate void OnTimeRemainingChangedDelegate(float timeChange, float timeRemaining, float timeLimit, bool pulse);
    public static OnTimeRemainingChangedDelegate OnTimeRemainingChanged;

    public delegate void OnZeroHealthDelegate();
	public static OnZeroHealthDelegate OnZeroHealth;    // -> game over state

	public delegate void OnZeroFuelDelegate();
	public static OnZeroFuelDelegate OnZeroFuel;    // -> game over state

	public delegate void OnZeroTimeDelegate();
	public static OnZeroTimeDelegate OnZeroTime;    // -> game over state

	public delegate void OnTapToPlayDelegate(GameScores scores);           // after game over
    public static OnTapToPlayDelegate OnTapToPlay;

    public delegate void OnStartNewGameDelegate();
    public static OnStartNewGameDelegate OnStartNewGame;            // restart / play again

    public delegate void OnNewGameStartedDelegate(Action restart);
	public static OnNewGameStartedDelegate OnNewGameStarted;

    public delegate void OnResetDataDelegate();
    public static OnResetDataDelegate OnResetData;

    // UI

    public delegate void OnToggleOptionsDelegate();
    public static OnToggleOptionsDelegate OnToggleOptions;

    public delegate void OnGameOptionsDelegate(bool showing);
    public static OnGameOptionsDelegate OnGameOptions;

    public delegate void OnToggleLeaderboardDelegate();
    public static OnToggleLeaderboardDelegate OnToggleLeaderboard;

    // audio

    public delegate void OnMusicVolumeChangedDelegate(float vol);
    public static OnMusicVolumeChangedDelegate OnMusicVolumeChanged;

    public delegate void OnSFXVolumeChangedDelegate(float vol);
    public static OnSFXVolumeChangedDelegate OnSFXVolumeChanged;

    // scoring

    public delegate void OnUpdateGameScoreDelegate(float healthChange, float fuelChange, float secondsRemaining, int scoreFactor, Color scoreColour, Vector3 fruitPosition, string scoreMessage);
    public static OnUpdateGameScoreDelegate OnUpdateGameScore;

    public delegate void OnGameScoreUpdatedDelegate(long runningScore, int scoreChange, Color scoreColour, Vector3 fruitPosition, string scoreMessage); 
    public static OnGameScoreUpdatedDelegate OnGameScoreUpdated;

    public delegate void OnScoreResetDelegate();                                        // on start new game
    public static OnScoreResetDelegate OnScoreReset;

    public delegate void OnFinalGameScoreDelegate(GameScores scores, bool newPB, GameManager.GameOverState state);           // on game over
    public static OnFinalGameScoreDelegate OnFinalGameScore;

    public delegate void OnNewPersonalBestDelegate(GameScores scores, bool reloadScores);           // on game over
    public static OnNewPersonalBestDelegate OnNewPersonalBest;

    public delegate void OnBonusScoreFeedbackDelegate(Color colour, Vector3 position, string message, int bonusScore);    
    public static OnBonusScoreFeedbackDelegate OnBonusScoreFeedback;            // UI -> update game score

    public delegate void OnTimeIncreaseFeedbackDelegate(Color colour, Vector3 position, string message, int timeIncrease, int bonusScore);
    public static OnTimeIncreaseFeedbackDelegate OnTimeIncreaseFeedback;            // UI -> update game score

    public delegate void OnBonusScoreIncrementDelegate(int bonusScore);
    public static OnBonusScoreIncrementDelegate OnBonusScoreIncrement;          // update game score

    // stick figure

    //public delegate void OnFigureStateChangedDelegate(StickFigure.AnimationState oldState, StickFigure.AnimationState newState);
    //public static OnFigureStateChangedDelegate OnFigureStateChanged;

    public delegate void OnFigurePositionChangedDelegate(Vector2 lastPosition, Vector2 figurePosition);
	public static OnFigurePositionChangedDelegate OnFigurePositionChanged;

    public delegate void OnFigureFlipDelegate(StickFigure.Facing facing);
    public static OnFigureFlipDelegate OnFigureFlip;

	public delegate void OnFigureHitFruitDelegate(Fruit fruit);
	public static OnFigureHitFruitDelegate OnFigureHitFruit;            // TODO: not currently used

    public delegate void OnMoonBossResetDelegate();
    public static OnMoonBossResetDelegate OnMoonBossReset;

    public delegate void OnBossDefeatTeleportDelegate(PortalEndPoint portalEntry);
    public static OnBossDefeatTeleportDelegate OnBossDefeatTeleport;      // if boss has linked teleport

    public delegate void OnMoonFruitTeleportDelegate();
    public static OnMoonFruitTeleportDelegate OnMoonFruitTeleport;      // if moon has linked teleport

    public delegate void OnFigureHitGroundDelegate(Ground ground);
    public static OnFigureHitGroundDelegate OnFigureHitGround;

    public delegate void OnFigureHitPlatformDelegate(Platform platform);
    public static OnFigureHitPlatformDelegate OnFigureHitPlatform;

    public delegate void OnFigureHitObstacleDelegate(Obstacle obstacle);
    public static OnFigureHitObstacleDelegate OnFigureHitObstacle;

    //public delegate void OnFigureLandedDelegate();
    //public static OnFigureLandedDelegate OnFigureLanded;

    public delegate void OnFigureHitWaterDelegate(Vector3 figurePosition);
    public static OnFigureHitWaterDelegate OnFigureHitWater;

    public delegate void OnFigureLeaveWaterDelegate(Vector3 figurePosition);
    public static OnFigureLeaveWaterDelegate OnFigureLeaveWater;

    public delegate void OnFigureImpulseDelegate(Vector3 direction, float impulse);
    public static OnFigureImpulseDelegate OnFigureImpulse;

    public delegate void OnFigureInCrossHairDelegate(bool active, StickFigure targeted, LaserSource laserSource);
    public static OnFigureInCrossHairDelegate OnFigureInCrossHair;

    // fruit

    public delegate void OnThrowFruitDelegate(Fruit fruitPrefab, Vector3 origin, Vector3 direction, float impulse);
    public static OnThrowFruitDelegate OnThrowFruit;

    public delegate void OnPlayerHitFruitDelegate(float fuel, float health, bool isRotten, float toxicity, float timeIncrease, bool isToxic,
                            CardManager.CardBackType cardBackType, Color scoreColour, Vector3 fruitPosition, string scoreMessage);
    public static OnPlayerHitFruitDelegate OnPlayerTakeFruit;

    // platforms

    public delegate void OnPlatformFlashDelegate(Platform platform);
    public static OnPlatformFlashDelegate OnPlatformFlash;

    public delegate void OnPlatformExpiredDelegate(Platform platform);
    public static OnPlatformExpiredDelegate OnPlatformExpired;           // rest time limit expired (fall under gravity)

    public delegate void OnPlatformRemovedDelegate(Platform platform);
    public static OnPlatformRemovedDelegate OnPlatformRemoved;           // hit water or another platform

    // portals

    //public delegate void OnPortalEntryDelegate(Transportable thing, PortalEnd end);
    //public static OnPortalEntryDelegate OnPortalEntry;

    //public delegate void OnPortalExitDelegate(Transportable thing, PortalEnd end);
    //public static OnPortalExitDelegate OnPortalExit;

    // touch input

    public delegate void OnPlayerTapDelegate(GameManager.GameState gameState, int tapCount);
    public static OnPlayerTapDelegate OnPlayerTap;

    public delegate void OnSwipeStartDelegate(Vector2 startPosition, int touchCount);      // validSwipe false if 'spamming'
    public static OnSwipeStartDelegate OnSwipeStart;

    public delegate void OnSwipeMoveDelegate(Vector2 movePosition, int touchCount);
    public static OnSwipeMoveDelegate OnSwipeMove;

    public delegate void OnOnSwipeEndDelegate(Vector2 endPosition, Vector2 direction, float speed, int touchCount, int tapCount);   
    public static OnOnSwipeEndDelegate OnSwipeEnd;

	// leaderboard

	public delegate void OnInternetCheckDelegate(bool internetReachable);
	public static OnInternetCheckDelegate OnInternetCheck;

	public delegate void OnLookupPlayerProfileDelegate(string playerName, string schoolName);
    public static OnLookupPlayerProfileDelegate OnLookupPlayerProfile;

    public delegate void OnLookupPlayerProfileResultDelegate(string playerName, string schoolName, bool playerFound, bool schoolFound);
    public static OnLookupPlayerProfileResultDelegate OnLookupPlayerProfileResult;


    public delegate void OnSavePlayerProfileDelegate(string playerName, string school, PlayerProfile playerProfile);
    public static OnSavePlayerProfileDelegate OnSavePlayerProfile;

    public delegate void OnSavePlayerProfileResultDelegate(string playerName, bool success, bool loadScores);
    public static OnSavePlayerProfileResultDelegate OnSavePlayerProfileResult;

    public delegate void OnLookupPlayerScoresDelegate();
    public static OnLookupPlayerScoresDelegate OnLookupPlayerScores;

    public delegate void OnLookupSchoolScoresDelegate(string schoolName);
    public static OnLookupSchoolScoresDelegate OnLookupSchoolScores;

    public delegate void OnPlayerScoresUpdatedDelegate(GameScores scores, bool success, bool reloadScores);
    public static OnPlayerScoresUpdatedDelegate OnPlayerScoresUpdated;

    public delegate void OnPlayerScoresRetrievedDelegate(List<LeaderboardScore> scores);
    public static OnPlayerScoresRetrievedDelegate OnPlayerScoresRetrieved;

    public delegate void OnSchoolScoresRetrievedDelegate(string schoolName, List<LeaderboardScore> scores);
    public static OnSchoolScoresRetrievedDelegate OnSchoolScoresRetrieved;

    public delegate void OnLookupSchoolsDelegate();
    public static OnLookupSchoolsDelegate OnLookupSchools;

    public delegate void OnSchoolsRetrievedDelegate(List<string> schools);
    public static OnSchoolsRetrievedDelegate OnSchoolsRetrieved;

    // bomb

    public delegate void OnStartBombFuseDelegate(Bomb bomb, bool cameraFollow);
    public static OnStartBombFuseDelegate OnBombStart;      // on hit detonator

    public delegate void OnFuseStartDelegate(ParticleSystem fuse, bool cameraFollow);
    public static OnFuseStartDelegate OnFuseStart;          // fuse starts

    public delegate void OnFuseEndDelegate(ParticleSystem fuse, bool cameraFollow);
    public static OnFuseEndDelegate OnFuseEnd;              // fuse starts

    public delegate void OnStopBombFuseDelegate(Bomb bomb, bool cameraFollow);
    public static OnStopBombFuseDelegate OnBombEnd;         // after shockwave

    // heart fuse
    public delegate void OnFireHeartFuseDelegate();
    public static OnFireHeartFuseDelegate OnFireHeartFuse;

    public delegate void OnHeartFuseStartDelegate(HeartPath heart);
    public static OnHeartFuseStartDelegate OnHeartFuseStart;

    public delegate void OnHeartFuseEndDelegate(HeartPath heart);
    public static OnHeartFuseEndDelegate OnHeartFuseEnd;

    // proximity trigger / gauge

    public delegate void OnFigureProximityEnterDelegate(ProximityTrigger scannerTrigger, ProximityTrigger intruderTrigger);
    public static OnFigureProximityEnterDelegate OnFigureProximityEnter;

    public delegate void OnFigureProximityStayDelegate(ProximityTrigger scannerTrigger, ProximityTrigger intruderTrigger, float proximityPercent); //, Color proximityColour);
    public static OnFigureProximityStayDelegate OnFigureProximityStay;

    public delegate void OnProximityBlocksChangedDelegate(float proximityPercent, int filledBlocks);
    public static OnProximityBlocksChangedDelegate OnProximityBlocksChanged;

    public delegate void OnFigureProximityLeaveDelegate(ProximityTrigger scannerTrigger, ProximityTrigger intruderTrigger);
    public static OnFigureProximityLeaveDelegate OnFigureProximityLeave;

    // maze zones

    public delegate void OnMazeZoneEnterDelegate(MazeZoneVisitor visitor, MazeZone mazeZone);
    public static OnMazeZoneEnterDelegate OnMazeZoneEnter;

    public delegate void OnMazeZoneEjectDelegate();
    public static OnMazeZoneEjectDelegate OnMazeZoneEject;

    public delegate void OnMazeZoneExitDelegate(MazeZoneVisitor visitor, MazeZone mazeZone);
    public static OnMazeZoneExitDelegate OnMazeZoneExit;
}
