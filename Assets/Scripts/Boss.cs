﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Experimental.Rendering.Universal;
using System;


// shifts periodically from good to evil once 'intruded'
// otherwise stays in default 'good' state
// attacks player periodically once intruded
// inflicts damage if player hits while 'evil'
// otherwise takes damage from player and thrown 'fruit' hits
// thrown fruit has no effect on evil boss - but causes an immediate strike back if in range

[RequireComponent(typeof(Collider2D))]

public class Boss : MonoBehaviour
{
    public GameObject BossMain;              // head / body
    public Light2D BossLight;
    public SpriteRenderer BossSprite;

	public CircleCollider2D IntruderTrigger;
	public CircleCollider2D AttackTrigger;          // attack range

    [Header("Good & Evil")]
    public Color GoodColour = Color.yellow;
    public Color EvilColour = Color.red;
    [HideInInspector]
    public Gradient ShiftGradient;

    [Space]
    public Color PauseLightColour = Color.blue;     // back light while paused (crosshair)

    [Space]
    public ParticleSystem GoodParticles;
    public ParticleSystem EvilParticles;

    [Header("Halo / Horns")]
    public Transform Halo;
    private SpriteRenderer HaloSprite;
    private Light2D HaloLight;
    private float haloLightIntensity;

    public Transform Horns;
    private SpriteRenderer HornsSprite;
    private Light2D HornsLight;
    private float hornsLightIntensity;

    [Header("Movement")]
    public float MovementMinPause = 2f;
    public float MovementMaxPause = 2f;
    private float MovementPause => UnityEngine.Random.Range(MovementMinPause, MovementMaxPause);

    public float InactiveMinPause = 2f;
    public float InactiveMaxPause = 2f;
    private float InactivePause => UnityEngine.Random.Range(InactiveMinPause, InactiveMaxPause);

    [Header("Patrolling")]
    public bool PatrolToDestination = true;
    public Transform PatrolDestination;
    public float PatrolTime = 6f;
    public float PatrolDelay = 2f;           // at start of patrolling only
    public float PatrolMinPause = 0.25f;
    public float PatrolMaxPause = 2f;
    private float PatrolPause => UnityEngine.Random.Range(PatrolMinPause, PatrolMaxPause);

    private Vector3 startingPosition;
    private Vector3 patrolTargetPostion;

    [Header("Rotating")]
    public float RotateTime = 6f;
    public float RotateAngle = 45f;
    public float RotateDelay = 2f;           // at start of rotating only
    public float RotateMinPause = 0.25f;
    public float RotateMaxPause = 2f;
    private float RotatePause => UnityEngine.Random.Range(RotateMinPause, RotateMaxPause);

    [Header("Flip 180")]
    public float FlipTime = 1.5f;             // 180 deg

    [Header("Shapeshifting")]
    public float ShiftTime;             // good <-> evil
    private bool shifting = false;
    private Coroutine shiftCoroutine;

    [Header("Attacking")]
    public float StrikeTime;                    // round trip time
      
    private Coroutine attackCoroutine;
    private bool attacking = false;     // random strikes
    private bool striking = false;

    private float strikeLungeTime = 0.4f;         // % of StrikeTime that is lunge (remander is return to original state)
    private float strikeWindupTime = 0.4f;       // % of lunge time that is considered 'windup' (ie. shrink)

    private float strikeWindupScale = 0.6f;
    private float strikeLungeScale = 1.25f;

    private float MaxShiftInterval = 5f;                    // when start shifting
    private float shiftIntervalDecreaseFactor = 0.05f;      // shift interval decreases (by eg. 5%) on each shift
    private float MaxStrikeInterval = 3f;                   // when start attacking
    private float strikeIntervalDecreaseFactor = 0.1f;      // strike interval decreases (by eg. 10%) on each strike

    [Header("Health")]
    public float MaxHealth = 1000f;
    private float CurrentHealth = 0f;
    public float MinionSpawnHealth = 0.75f;             // % threshold when minions are spawned
    private bool SpawnMinionHealth => (CurrentHealth / MaxHealth) < MinionSpawnHealth;       // triggered when health falls below threshold
    private bool MinionsAlive => SpawnsMinions && minionsSpawned && minionSpawner.MinionCount(true) > 0;    // can't sustain damage?  
    private bool minionsSpawned = false;

    public Slider HealthBar;
    public float HealthBarUpdateTime = 0.5f;
    private float PulseScale = 1.25f;       
    private float PulseTime = 0.3f;	

    [Header("Damage")]
    public float PlayerDamage = 50f;        // inflicted on player when 'evil'
    public int BonusScore = 10000;          // on boss death

    [Header("Splat")]
    public ParticleSystem SplatPrefab;      // on boss death
    public Color SplatColour = Color.red;   // on boss death
    private float destroyDelay = 2f;        // to allow splat particles to play out
    private float bossDisableTime = 3f;     // sprite / collider disabled after player 'hit'

    [Space]
    public PortalEndPoint PortalEntry;      // on boss death

    [Header("Audio")]
    public AudioClip DamageAudio;             // on take damage
    public AudioClip WhiffAudio;             // on strike start
    public AudioClip StrikeAudio;             // on strike player
    public AudioClip DeadAudio;             // on boss death

    public enum BossState
    {
        Good,
        ShiftingGood,
        Evil,
        ShiftingEvil,
        Dead
    }
    public BossState State { get; private set; }

    private bool CanStrike => (State == BossState.Evil || State == BossState.ShiftingEvil);
    private bool Vulnerable => SwitchVulnerability ? CanStrike : State != BossState.Evil;

    public bool SwitchVulnerability = false;        // if true, vulnerable when CanStrike

    public enum AttackState
    {
        Idle,
        Striking,
        Shockwave,
        Throwing
    }
    public AttackState BossAttackState { get; private set; }

    public enum PatrolState
    {
        InActive,
        Patrolling,         // along path
        Scanning,           // rotating
        Flip                // 180 deg
    }
    public PatrolState BossPatrolState { get; private set; }

    private PatrolState RandomPatrolState
    {
        get
        {
            var array = Enum.GetValues(typeof(PatrolState));
            return (PatrolState)array.GetValue(UnityEngine.Random.Range(0, array.Length));
        }
    }

    public Animator BossAnimator;

    private StickFigure intruderFigure;         // to get range and direction for attack
    private bool intruderInAttackRange;

    private GameManager.GameState gameState;

    private Collider2D bossCollider;
    private AudioSource audioSource;

    private LaserSource laserSource;
    public bool HasLaser => laserSource != null;

    private BossSpawner minionSpawner;
    public bool SpawnsMinions => minionSpawner != null;

    public BossSpawner MinionParent { get; private set; }       // spawner that spawned this boss (as a minion)
    public bool IsMinion => MinionParent != null;

    public bool FlippedMinion;           // spawns rotated 180 around y if true

    private int tweenId = -1;
    private bool lockedOntoTarget = false;
    private Color lightPausedColour;        // cache backlight colour when lockedOnToTarget (crosshair)


    private void Awake()
    {
        bossCollider = GetComponent<CircleCollider2D>();
        audioSource = GetComponent<AudioSource>();
        laserSource = GetComponent<LaserSource>();
        minionSpawner = GetComponent<BossSpawner>();

        if (Halo != null)
        {
            HaloSprite = Halo.GetComponent<SpriteRenderer>();
            HaloLight = Halo.GetComponent<Light2D>();
            haloLightIntensity = HaloLight.intensity;
        }

        if (Horns != null)
        {
            HornsSprite = Horns.GetComponent<SpriteRenderer>();
            HornsLight = Horns.GetComponent<Light2D>();
            hornsLightIntensity = HornsLight.intensity;
        }

        SetShiftGradient();
        ResetDefault();

        if (PortalEntry != null)
            PortalEntry.EnableTriggerZone(false);       // to prevent teleporting while boss is 'destroyed'
    }


    private void OnEnable()
    {
        GameEvents.OnGameStateChanged += OnGameStateChanged;
    }

    private void OnDisable()
    {
        GameEvents.OnGameStateChanged -= OnGameStateChanged;
    }

    public void InitMinionBoss(BossSpawnData spawnData, BossSpawner spawner)
    {
        MinionParent = spawner;

        transform.localScale = Vector3.zero;
        transform.position = spawner.transform.position;

        EnableCollider(false);

        // flip head / body / laser (not UI) if required
        if (spawnData.IsFlipped)
            Flip(0.01f);     // ie. instant

        if (spawnData.PatrolDestination != null)
            PatrolDestination = spawnData.PatrolDestination;
    }


    private void ActivateNext()
    {
        if (gameState == GameManager.GameState.GameOver)
            return;

        if (laserSource != null)
        {
            //laserSource.Reset();                                    // stop firing and searching

            if (UnityEngine.Random.Range(0f, 1f) >= 0.5f)           // 50% chance of laser on
                laserSource.ActivateLaser();                        // stop searching, start firing
            else
                laserSource.DeactivateLaser(true);                      // start searching, stop firing
        }

        StartCoroutine(NextMovement());
    }

    private IEnumerator NextMovement()
    {
        while (lockedOntoTarget)
        {
            yield return null;
        }

        yield return new WaitForSeconds(MovementPause);

        tweenId = -1;
        BossPatrolState = RandomPatrolState;

        //Debug.Log($"NextMovement: {name} BossPatrolState = {BossPatrolState}");

        switch (BossPatrolState)
        {
            case PatrolState.InActive:
            default:
                yield return new WaitForSeconds(InactivePause);
                ActivateNext();
                break;

            case PatrolState.Patrolling:
                Patrol();
                break;

            case PatrolState.Scanning:
                RotateScan();
                break;

            case PatrolState.Flip:
                Flip(FlipTime);
                yield return new WaitForSeconds(InactivePause);
                ActivateNext();
                break;
        }

        yield return null;
    }


    public void PauseMovement()
    {
        //Debug.Log($"PauseMovement: tweenId = {tweenId}  isTweening = {LeanTween.isTweening(tweenId)}");
        if (tweenId != -1) // && LeanTween.isTweening(tweenId))
        {
            LeanTween.pause(tweenId);
            lockedOntoTarget = true;

            lightPausedColour = BossLight.color;
            BossLight.color = PauseLightColour;
        }
    }

    public void ResumeMovement()
    {
        //Debug.Log($"ResumeMovement: tweenId = {tweenId}  isPaused = {LeanTween.isPaused(tweenId)}");
        if (tweenId != -1) // && LeanTween.isPaused(tweenId))
        {
            LeanTween.resume(tweenId);
            lockedOntoTarget = false;

            // restore light
            BossLight.color = lightPausedColour;
        }
    }

    // strike periodically (decreasing)
    // while intruder in attack range
    private IEnumerator AttackIntruder()
    {
        if (attacking)
            yield break;

        while (lockedOntoTarget)
        {
            yield return null;
        }

        attacking = true;
        //Debug.Log($"AttackIntruder: State = {State} CanStrike = {CanStrike}");

        float strikeInterval = MaxStrikeInterval;

        while (attacking)
        {
            if (State == BossState.Dead || gameState == GameManager.GameState.GameOver)
            {
                attacking = false;
                yield break;
            }

            if (!striking && CanStrike && ! HasLaser)
            {
                Strike();
                strikeInterval -= strikeInterval * strikeIntervalDecreaseFactor;
            }

            yield return new WaitForSecondsRealtime(StrikeTime + strikeInterval);
        }
    }

    private void StopAttacking()
    {
        attacking = false;

        if (attackCoroutine != null)
        {
            StopCoroutine(attackCoroutine);
            attackCoroutine = null;
        }
    }

    private void Strike()
    {
        //Debug.Log($"Strike: State = {State} CanStrike = {CanStrike}");

        if (gameState == GameManager.GameState.GameOver)
            return;

        if (striking || !CanStrike)
            return;

        if (intruderFigure == null || !intruderInAttackRange)
            return;

        striking = true;
        HealthBar.gameObject.SetActive(false);

        PlayAudio(WhiffAudio);

        Vector3 startPosition = transform.position;
        Vector3 startScale = transform.localScale;

        // lunge towards player
        float lungeTime = StrikeTime * strikeLungeTime;
        float returnTime = StrikeTime - lungeTime;

        LeanTween.move(gameObject, intruderFigure.transform.position, lungeTime)
                        .setEaseInBack()
                        .setOnComplete(() =>
                        {
                            // return to start position
                            LeanTween.move(gameObject, startPosition, returnTime)
                                            .setEaseOutBack()
                                            .setOnComplete(() =>
                                            {
                                                striking = false;
                                                HealthBar.gameObject.SetActive(true);
                                            });
                        });

        // shrink for 'wind up'
        float windupTime = lungeTime * strikeWindupTime;
        float strikeTime = lungeTime - windupTime;

        LeanTween.scale(gameObject, Vector3.one * strikeWindupScale, windupTime)
                        .setEaseInBack()
                        .setOnComplete(() =>
                        {
                            // enlarge for remainder of 'lunge' time
                            LeanTween.scale(gameObject, Vector3.one * strikeLungeScale, strikeTime)
                                            .setEaseOutBack()
                                            .setOnComplete(() =>
                                            {
                                                // return to start scale
                                                LeanTween.scale(gameObject, startScale, returnTime)
                                                                .setEaseOutBack();
                                            });
                        });
    }

    private void Patrol()
    {
        if (PatrolTime <= 0)
            return;

        if (PatrolDestination.localPosition == Vector3.zero)     // same as boss position!
            return;

        startingPosition = transform.position;
        patrolTargetPostion = PatrolDestination.position;

        tweenId = LeanTween.move(gameObject, patrolTargetPostion, PatrolTime)
                    .setDelay(PatrolDelay)
                    .setEaseInOutSine()
                    .setOnComplete(() =>
                    {
                        LeanTween.move(gameObject, startingPosition, PatrolTime)
                                .setDelay(PatrolPause)
                                .setEaseInOutSine()
                                .setOnComplete(() =>
                                {
                                    //if (gameState == GameManager.GameState.InPlay)
                                        ActivateNext();
                                });
                    }).id;
    }

    public void RotateScan()
    {
        if (RotateTime <= 0)
            return;

        // rotate up
        tweenId = LeanTween.rotateAround(BossMain, transform.forward, -RotateAngle, RotateTime * 0.25f)
                        .setEaseInOutQuart()
                        .setOnComplete(() =>
                        {
                            // rotate down
                            LeanTween.rotateAround(BossMain, transform.forward, RotateAngle * 2f, RotateTime * 0.5f)
                                .setDelay(RotatePause)
                                .setEaseInOutQuart()
                                .setOnComplete(() =>
                                {
                                    // back to start
                                    LeanTween.rotateAround(BossMain, transform.forward, -RotateAngle, RotateTime * 0.25f)
                                        .setDelay(RotatePause)
                                        .setEaseInOutQuart()
                                        .setOnComplete(() =>
                                        {
                                            //if (gameState == GameManager.GameState.InPlay)
                                                ActivateNext();
                                        });
                                });
                        }).id;
    }

    // intruder and attack trigger colliders entry / exit

    public void IntruderEntry(StickFigure figure)
    {
        if (gameState == GameManager.GameState.GameOver)
            return;

        if (intruderFigure == figure)
            return;

        intruderFigure = figure;

        StopShifting();
        shiftCoroutine = StartCoroutine(ShapeShift());
    }

    public void IntruderExit(StickFigure figure)
    {
        if (intruderFigure == null)
            return;

        intruderFigure = null;

        StopShifting();
        ShiftToGood();
    }

    public void EnterAttackRange(StickFigure figure)
    {
        if (gameState == GameManager.GameState.GameOver)
            return;

        intruderInAttackRange = true;

        StopAttacking();
        attackCoroutine = StartCoroutine(AttackIntruder());
    }

    public void ExitAttackRange(StickFigure figure)
    {
        if (! intruderInAttackRange)
            return;

        intruderInAttackRange = false;
        StopAttacking();
    }

    // take damage from player / thrown fruit
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (State == BossState.Dead)
            return;

        if (gameState == GameManager.GameState.GameOver)
            return;

        if (collision.gameObject.CompareTag("Fruit"))
        {
            var hitByFruit = collision.gameObject.GetComponent<Fruit>();
            if (!hitByFruit.IsThrown)
                return;

            if (Vulnerable)        // thrown fruit inflicts damage on boss
            {
                UpdateHealth(-hitByFruit.BossDamage);
            }
            // thrown fruit has no effect on 'evil' boss
            // but causes immediate retaliation if in attack range
            else if (intruderInAttackRange)
            {
                if (CanStrike && ! HasLaser)
                    Strike();           
            }
        }
        else if (collision.gameObject.CompareTag("Player"))
        {
            var figure = collision.gameObject.GetComponent<StickFigure>();

            if (Vulnerable)        // player inflicts damage on boss
            {
                UpdateHealth(-figure.BossDamage);
            }
            else            // boss inflicts damage on player
            {
                figure.TakeBossDamage(PlayerDamage, collision.contacts[0].point, true);
                PlayAudio(StrikeAudio);
            }
        }
    }


    private void UpdateHealth(float increment)
    {
        if (State == BossState.Dead)
            return;

        if (gameState == GameManager.GameState.GameOver)
            return;

        if (increment == 0)
            return;

        float healthBeforeUpdate = CurrentHealth;

        CurrentHealth += increment;
        CurrentHealth = Mathf.Clamp(CurrentHealth, 0f, MaxHealth);

        HealthBar.maxValue = MaxHealth;
        LeanTween.value(HealthBar.value, CurrentHealth, HealthBarUpdateTime)
                    .setOnUpdate((float f) => { HealthBar.value = f; })
                    .setEaseOutQuart();

        if (increment < 0)
            PlayAudio(DamageAudio);

        Pulse(HealthBar.gameObject);

        // check for boss death...
        if (CurrentHealth <= 0)
        {
            CurrentHealth = 0;
            State = BossState.Dead;

            StopShifting();
            StopAttacking();

            SetVisibility(false);

            // teleport stick figure through reset portal while collider disabled
            StartCoroutine(ResetTeleport());

            SplatParticles();
            PlayAudio(DeadAudio);
        }
        else if (SpawnsMinions && ! minionsSpawned && SpawnMinionHealth)
        {
            minionSpawner.SpawnMinions();
            minionsSpawned = true;
        }
    }

    private IEnumerator ResetTeleport()
    {
        yield return new WaitForSeconds(bossDisableTime);

        if (PortalEntry != null)
            GameEvents.OnBossDefeatTeleport?.Invoke(PortalEntry);

        GameEvents.OnMoonBossReset?.Invoke();
        ResetDefault();
    }

    private void SplatParticles()
    {
        if (SplatPrefab == null)
            return;

        var splat = Instantiate(SplatPrefab, transform.position, Quaternion.identity);
        var splatParticles = splat.GetComponent<ParticleSystem>();

        var splatMain = splatParticles.main;
        splatMain.startColor = SplatColour;
        splatParticles.Play();

        Destroy(splat, destroyDelay);
    }

    private void SetVisibility(bool visible)
    {
        BossSprite.enabled = visible;
        BossLight.enabled = visible;

        EnableCollider(visible);

        HaloSprite.enabled = visible;
        HaloLight.enabled = visible;

        HornsSprite.enabled = visible;
        HornsLight.enabled = visible;

        HealthBar.gameObject.SetActive(visible);
    }

    private void EnableCollider(bool enable)
    {
        bossCollider.enabled = enable;
    }


    // reset all boss elements to default 'Good' state
    // at start and after reset teleport (boss death)
    private void ResetDefault()
	{
        State = BossState.Good;
        BossAnimator.SetTrigger("Good");

        StopShifting();
        StopAttacking();
        striking = false;

        EvilParticles.Stop();
        GoodParticles.Play();

        BossSprite.color = GoodColour;
        BossLight.color = GoodColour;

        HaloSprite.color = Color.white;
        HaloLight.intensity = haloLightIntensity;

        HornsSprite.color = Color.clear;
        HornsLight.intensity = 0f;

        SetVisibility(true);
        UpdateHealth(MaxHealth);

        intruderFigure = null;
        intruderInAttackRange = false;

        minionsSpawned = false;
    }


    // state shifting

    // Good <-> Evil
    // starts when intruder enters
    // gradually decreasing intervals between shifts
    private IEnumerator ShapeShift()
    {
        if (shifting)
            yield break;

        while (lockedOntoTarget)
        {
            yield return null;
        }

        shifting = true;

        float goodTime = MaxShiftInterval;
        float evilTime = MaxShiftInterval;

        float shiftInterval = MaxShiftInterval;

        while (shifting)
        {
            //Debug.Log($"ShapeShift: goodTime = {goodTime} evilTime = {evilTime}");

            if (State == BossState.Dead || gameState == GameManager.GameState.GameOver)
            {
                shifting = false;
                yield break;
            }

            if (State == BossState.Good)
            {
                ShiftToEvil();
                shiftInterval = evilTime;
            }
            else if (State == BossState.Evil)
            {
                ShiftToGood();
                shiftInterval = goodTime;
            }

            // decrease good time and increase evil time each time round loop
            goodTime -= goodTime * shiftIntervalDecreaseFactor;
            evilTime += evilTime * shiftIntervalDecreaseFactor;

			yield return new WaitForSeconds(ShiftTime + shiftInterval); 
		}
    }

    private void StopShifting()
    {
        shifting = false;

        if (shiftCoroutine != null)
        {
            StopCoroutine(shiftCoroutine);
            shiftCoroutine = null;
        }
    }

    private void ShiftToEvil()
    {
        State = BossState.ShiftingEvil;

        FadeHorns(true);
        FadeHalo(false);

        EvilParticles.Play();
        GoodParticles.Stop();

        // boss -> evil colour
        LeanTween.value(gameObject, 0f, 1f, ShiftTime)
            //.setEaseOutSine()
            .setOnUpdate((float f) =>
            {
                var shiftColour = ShiftGradient.Evaluate(f);
                BossSprite.color = shiftColour;
                BossLight.color = shiftColour;
            })
            .setOnComplete(() =>
            {
                State = BossState.Evil;
                BossAnimator.SetTrigger("Evil");
            });
    }

    private void ShiftToGood()
    {
        State = BossState.ShiftingGood;

		FadeHorns(false);
        FadeHalo(true);

        EvilParticles.Stop();
        GoodParticles.Play();

        // boss -> good colour
        LeanTween.value(gameObject, 1f, 0f, ShiftTime)
            //.setEaseOutSine()
            .setOnUpdate((float f) =>
            {
                var shiftColour = ShiftGradient.Evaluate(f);
                BossSprite.color = shiftColour;
                BossLight.color = shiftColour;
            })
            .setOnComplete(() =>
            {
                State = BossState.Good;
                BossAnimator.SetTrigger("Good");
            });
    }


    private void FadeHorns(bool fadeIn)
    {
		LeanTween.color(Horns.gameObject, fadeIn ? Color.white : Color.clear, ShiftTime)
                    //.setEaseOutSine()
                    .setOnUpdate((Color c) =>
                    {
                        HornsSprite.color = c;
                    });

        LeanTween.value(fadeIn ? 0f : hornsLightIntensity, fadeIn ? hornsLightIntensity : 0f, ShiftTime)
                    //.setEaseOutSine()
                    .setOnUpdate((float f) =>
                    {
                        HornsLight.intensity = f;
                    });
    }

    private void FadeHalo(bool fadeIn)
    {
        LeanTween.color(Halo.gameObject, fadeIn ? Color.white : Color.clear, ShiftTime)
                    //.setEaseOutSine()
                    .setOnUpdate((Color c) =>
                    {
                        HaloSprite.color = c;
                    });

        LeanTween.value(fadeIn ? 0f : haloLightIntensity, fadeIn ? haloLightIntensity : 0f, ShiftTime)
                    //.setEaseOutSine()
                    .setOnUpdate((float f) =>
                    {
                        HaloLight.intensity = f;
                    });
    }


    private void OnGameStateChanged(GameManager.GameState currentState, GameManager.GameState newState)
    {
        gameState = newState;

        if (gameState == GameManager.GameState.InPlay && ! IsMinion)
        {
            Activate();
        }
    }

    public void Activate()
    {
        //if (SpawnsMinions)
        //    minionSpawner.SpawnMinions();

        EnableCollider(true);
        ActivateNext();
    }


    // flip head / body / laser (not UI)
    public void Flip(float flipTime)
    {
        LeanTween.rotateAround(BossMain.gameObject, transform.up, 180f, flipTime)
                .setEaseOutCubic();
    }


    private void SetShiftGradient()
    {
        GradientColorKey[] colourKeys = new GradientColorKey[2];
        colourKeys[0].color = GoodColour;
        colourKeys[0].time = 0f;
        colourKeys[1].color = EvilColour;
        colourKeys[1].time = 1f;

        GradientAlphaKey[] alphaKeys = new GradientAlphaKey[2];
        alphaKeys[0].alpha = 1f;
        alphaKeys[0].time = 0f;
        alphaKeys[1].alpha = 1f;
        alphaKeys[1].time = 1f;

        ShiftGradient.SetKeys(colourKeys, alphaKeys);
    }


    private void Pulse(GameObject UIObject, Action onComplete = null)
    {
        var largeScale = Vector2.one * PulseScale;

        LeanTween.scale(UIObject.gameObject, largeScale, PulseTime)
            .setEaseInBack()
            .setOnComplete(() =>
            {
                LeanTween.scale(UIObject.gameObject, Vector2.one, PulseTime * 0.5f)
                    .setEaseOutBack()
                    .setOnComplete(() =>
                    {
                        onComplete?.Invoke();
                    });
            });
    }

    private void PlayAudio(AudioClip clip)
    {
        if (audioSource != null && clip != null)
            audioSource.PlayOneShot(clip);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, IntruderTrigger.radius);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, AttackTrigger.radius);

        Gizmos.color = Color.white;

        if (PatrolDestination.localPosition != Vector3.zero)
        {
            Gizmos.DrawLine(transform.position, PatrolDestination.position);
            Gizmos.DrawWireSphere(PatrolDestination.position, 2f);
        }
    }
}
