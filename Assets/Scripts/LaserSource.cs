﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// shoots a laser beam!

public class LaserSource : MonoBehaviour
{
    [Header("Laser Beam")]
    public LineRenderer LaserBeam;
    public Transform FirePoint;                    // laser start
    public Transform FireDirection;
    public float FireRange = 40f;

    [Header("Searching")]
    public bool ShowSearchBeam = true;
    public LineRenderer SearchBeam;

    public GameObject StartVFX;
    public GameObject EndVFX;

    public Color LaserColour = Color.yellow;
    //[HideInInspector]
    private float LaserIntensity = 2f;

    [Header("Damage Inflicted")]
    public float HealthDamage = 0.5f;           // damage inflicted on LaserTarget - each frame!

    private List<ParticleSystem> particleList = new List<ParticleSystem>();

    [Header("Laser Hit")]
    public LayerMask LaserLayers;           // for ray cast hits
    public AudioSource LaserAudio;          // looping

    private float CrossHairDelay = 2f;       // before laser starts firing
    private float crossHairTime = 0f;       // time player in crosshair

    private bool firing = false;            // laser on
    private bool searching = false;         // laser off - scanning

    private Boss parentBoss;


    private void Awake()
    {
        parentBoss = GetComponent<Boss>();
    }

    // Start is called before the first frame update
    private void Start()
    {
        PopulateParticleList();
        SetLaserColour(LaserColour, LaserIntensity);        // default colour

        DeactivateLaser(false);

        CeaseFire();
    }

    private IEnumerator FireLaser()
    {
        if (firing)
            yield break;

        firing = true;

        Vector2 fireDirection;
        Vector2 laserEndPoint;

        RaycastHit2D hit;

        LaserBeam.enabled = true;
        SearchBeam.enabled = false;

        while (firing)
        {
            fireDirection = (FireDirection.position - FirePoint.position).normalized;
            hit = LaserCast(fireDirection);

            if (hit.collider != null)
            {
                laserEndPoint = hit.point;

                var laserTarget = hit.collider.GetComponent<LaserTarget>();
                if (laserTarget != null)
                {
                    laserTarget.TakeLaserDamage(HealthDamage, laserEndPoint);
                }
            }
            else            // no hit - full range
            {
                laserEndPoint = (Vector2)FirePoint.position + (fireDirection * FireRange);
            }

            LaserBeam.SetPosition(0, FirePoint.position);
            LaserBeam.SetPosition(1, laserEndPoint);

            StartVFX.transform.position = FirePoint.position;
            EndVFX.transform.position = laserEndPoint;

            yield return null;
        }

        LaserBeam.enabled = false;
    }

    private IEnumerator SearchForFigure()
    {
        if (searching)
            yield break;

        searching = true;

        RaycastHit2D hit;
        Vector2 lookDirection;

        if (ShowSearchBeam)
            SearchBeam.enabled = true;      // line renderer

        Vector2 searchEndPoint;             // only used if ShowSearchBeam

        while (searching)
        {
            lookDirection = (FireDirection.position - FirePoint.position).normalized;       // moving while searching!
            hit = LaserCast(lookDirection);

            if (hit.collider != null)
            {
                searchEndPoint = hit.point;

                var stickFigure = hit.collider.GetComponent<StickFigure>();
                if (stickFigure != null)
                    ActivateCrossHair(stickFigure);
                else    // hit something else
                    DeativateCrossHair();
            }
            else        // no raycast hit
            {
                searchEndPoint = (Vector2)FirePoint.position + (lookDirection * FireRange); 
                DeativateCrossHair();
            }

            if (ShowSearchBeam)
            {
                SearchBeam.SetPosition(0, FirePoint.position);
                SearchBeam.SetPosition(1, searchEndPoint);
            }

            yield return null;
        }

        if (ShowSearchBeam)
            SearchBeam.enabled = false; 
    }


    private RaycastHit2D LaserCast(Vector2 direction)
    {
        return Physics2D.CircleCast(FirePoint.position, LaserBeam.endWidth / 2f, direction, FireRange, LaserLayers);
    }


    // also pauses boss movement
    private void ActivateCrossHair(StickFigure stickFigure)
    {
        crossHairTime += Time.deltaTime;
        Debug.Log($"ActivateCrossHair: crossHairTime = {crossHairTime}");

        if (crossHairTime >= CrossHairDelay)        // stop searching and fire laser!
        {
            searching = false;
            DeativateCrossHair();
            ActivateLaser();
        }
        else
            GameEvents.OnFigureInCrossHair?.Invoke(true, stickFigure, this);

        if (parentBoss != null)
            parentBoss.PauseMovement();
    }

    // also resumes paused boss movement
    private void DeativateCrossHair()
    {
        crossHairTime = 0f;     // reset timer

        GameEvents.OnFigureInCrossHair?.Invoke(false, null, this);

        if (parentBoss != null)
            parentBoss.ResumeMovement();
    }


    public void ActivateLaser()
    {
        GameEvents.OnFigureInCrossHair?.Invoke(false, null, this);

        CeaseFire();

        StartCoroutine(FireLaser());
        ActivateParticles();

        if (LaserAudio != null)
            LaserAudio.Play();
    }


    public void DeactivateLaser(bool activateSearch)
    {
        LaserBeam.enabled = false;
        GameEvents.OnFigureInCrossHair?.Invoke(false, null, this);

        CeaseFire();

        if (activateSearch)
            StartCoroutine(SearchForFigure());

        DeactivateParticles();

        if (LaserAudio != null)
            LaserAudio.Stop();
    }


    private void ActivateParticles()
    {
        foreach (var particles in particleList)
        {
            particles.Play();
        }
    }

    private void DeactivateParticles()
    {
        foreach (var particles in particleList)
        {
            particles.Stop();
        }
    }

    private void SetLaserWidth(float width)
    {
        LaserBeam.startWidth = width;
        LaserBeam.endWidth = width;
    }

    public void SetLaserColour(Color colour, float intensity)
    {
        foreach (var particles in particleList)
        {
            var main = particles.main;
            main.startColor = colour;
        }

        LaserBeam.startColor = colour;
        LaserBeam.endColor = colour;

        // set shader property value
        //LaserBeam.material.SetColor("LaserColour", colour * intensity);
    }

    private void PopulateParticleList()
    {
        foreach (Transform child in StartVFX.transform)
        {
            var particles = child.GetComponent<ParticleSystem>();

            if (particles != null)
                particleList.Add(particles);
        }

        foreach (Transform child in EndVFX.transform)
        {
            var particles = child.GetComponent<ParticleSystem>();

            if (particles != null)
                particleList.Add(particles);
        }
    }

    private void CeaseFire()
    {
        firing = false;
        searching = false;

        crossHairTime = 0;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;

        Gizmos.DrawWireSphere(FirePoint.position, LaserBeam.startWidth / 2f);
        Gizmos.DrawWireSphere(FireDirection.position, LaserBeam.endWidth / 2f);
        Gizmos.DrawLine(FirePoint.position, FireDirection.position);
    }
}
