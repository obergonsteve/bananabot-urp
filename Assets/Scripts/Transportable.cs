﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// script for objects that can teleport through portals
[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]

public class Transportable : MonoBehaviour
{
    public bool IsActive = true;
    public List<GameObject> Lights;               // lights, particles, sprites etc. turned off while teleporting
    
    public bool InTransit { get; private set; }

    private PortalEndPoint portalEntry;            // used during teleport
    private PortalEndPoint portalExit;             // used during teleport

    private Vector2 entryScale;                     // set on entry to portal
    private Vector3 entryVelocity;                  // set on entry to portal
    private float entryTorque;                      // set on entry to portal

    private Rigidbody2D transRigidbody;
    private Collider2D transCollider;


    private void Awake()
    {
        transRigidbody = GetComponent<Rigidbody2D>();       // required component
        transCollider = GetComponent<Collider2D>();         // required component

        InTransit = false;
    }


    public void Teleport(PortalEndPoint entry)
    {
        if (!IsActive)
            return;

        if (InTransit)
            return;

        portalEntry = entry;
        portalExit = entry.OtherEndPoint;

        EnterPortal();
    }

    // scale down and move to entry, then warp to exit, scale up again and expel with force
    private void EnterPortal()
    {
        if (InTransit)
            return;

        entryScale = transform.localScale;
        entryVelocity = transRigidbody.velocity;
        entryTorque = transRigidbody.angularVelocity;

        // stop
        transRigidbody.velocity = Vector3.zero;
        transRigidbody.angularVelocity = 0f;

        portalEntry.Zone.enabled = false;          // disable entry trigger collider
        portalExit.Zone.enabled = false;          // disable exit trigger collider

        transCollider.enabled = false;              // no collisions while teleporting

        InTransit = true;

        portalEntry.EntryAudio();

        // shrink down
        LeanTween.scale(gameObject, Vector3.zero, portalEntry.ShrinkTime)
                    .setEaseOutQuart();
           
		// move to teleport point
		LeanTween.move(gameObject, portalEntry.TeleportPoint, portalEntry.ShrinkTime)
	                .setEaseOutQuad()
	                .setOnComplete(() =>
	                {
		                portalEntry.TravelAudio();

						// hide while travelling (lights, trail, etc. off)
						ActivateLights(false);

						// travel to exit
						LeanTween.move(gameObject, portalExit.ExitPoint, portalExit.TravelTime)
						            .setEaseOutQuad()
						            .setOnComplete(() =>
						            {
							            ActivateLights(true);  // lights, etc. back on
									    ExitPortal();
						            });
	                });
	}

    // scale back up and add force to 'expel'
    private void ExitPortal()
    {
        if (! InTransit)
            return;

        portalExit.ExitAudio();

        if (portalEntry.ParentPortal.MaintainVelocity)
            transRigidbody.AddForce(entryVelocity, ForceMode2D.Impulse);
        else
            transRigidbody.AddForce(portalExit.ExitVector, ForceMode2D.Impulse);

        transRigidbody.AddTorque(entryTorque);

        transCollider.enabled = true;

        // scale back up
        LeanTween.scale(gameObject, entryScale, portalExit.GrowTime)
                            .setEaseOutQuad()
                            .setOnComplete(() =>
                            {
                                StartCoroutine(ReactivatePortal());
                                InTransit = false;
                            });
    }

    private IEnumerator ReactivatePortal()
    {
        yield return new WaitForSeconds(portalExit.ParentPortal.ReactivateDelay);
        portalEntry.Zone.enabled = true;          // re-enable entry trigger collider
        portalExit.Zone.enabled = true;          // re-enable exit trigger collider
        yield return null;
    }

    // lights, particles, etc. off while 'InTransit'
    private void ActivateLights(bool on)
    {
        foreach (var light in Lights)
        {
            light.SetActive(on);

            var particles = light.GetComponent<ParticleSystem>();
            if (particles != null)
            {
                if (on)
                    particles.Play();
                else
                    particles.Stop();
            }
        }
    }
}
