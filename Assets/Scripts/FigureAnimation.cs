﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FigureAnimation : MonoBehaviour
{
	private StickFigure parentFigure;

	private void Start()
	{
		parentFigure = GetComponentInParent<StickFigure>();
	}


	// animator event (end of Landing)
	public void OnLanded()
	{
		parentFigure.OnFigureLanded();
		//GameEvents.OnFigureLanded?.Invoke();
	}
}
