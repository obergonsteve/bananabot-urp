﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// component for objects that can take damage from a laser
public class LaserTarget : MonoBehaviour
{
    public bool LaserCanSeek;        // can laser 'seek' this target?
    public bool LaserCanLockOn;      // can laser 'lock on' to this target?

    public void TakeLaserDamage(float healthDamage, Vector2 contactPoint)
    {
        var fruit = GetComponentInParent<Fruit>();
        if (fruit != null)
            fruit.LaserDestroy();
        else
        {
            var figure = GetComponentInParent<StickFigure>();
            if (figure != null)
                figure.TakeBossDamage(healthDamage, contactPoint, false);
        }
    }
}
