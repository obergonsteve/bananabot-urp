﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

[RequireComponent(typeof(Transportable))]
[RequireComponent(typeof(AudioSource))]

public class Fruit : MonoBehaviour
{
	[Header("Rotting")]
	public float RotTime = 10f;
	public float RotDelay = 1f;
	public Color FreshColour = Color.white;
	public Color RottenColour = Color.grey;
	public Gradient RotColourGradient;                  // fresh to rotten

	public float MaxFuel { get; private set; }          // decreased with 'rotting'
	public float MaxHealth { get; private set; }        // decreased with 'rotting'

	[Header("Damage")]
	public float ObstacleDamage = 10f;              // only if thrown
	public bool DestroyOnHitWall = false;

	public float BossDamage = 10f;					// only if thrown and hits boss

	[Header("Spawning")]
	public float SpawnTime;
	public float ShrinkTime;                        // when taken
	public float RottenShrinkTime = 30f;            // slow shrink when rotten
	public float FallTime = 0.5f;
	public float GrowScale;
	public float ThrowTorque;

	public AudioClip SpawnAudio;

	private Vector2 fruitScale;

	[Header("Timer")]
	public AudioClip TimeTickAudio;
	public float TickScale = 2f;
	public float TickTime = 0.25f;

	private float timerInterval = 1f;       // seconds
	private Coroutine timeCountdownCoroutine;
	private bool timerRunning = false;

	private float clockTime;            // initial time - decreased by timer countdown
	private float secondsRemaining;
	private float timeFactor;       // multiplied by SecondsRemaining when fruit taken by player

	[Header("Light / Trail")]
	public Light2D SpriteLight;
	public Light2D PointLight;
	public ParticleSystem Trail;

	// health and fuel values decrease with rotting
	public float HealthValue => MaxHealth - Toxicity;
	public float HealthPercent => HealthValue / MaxHealth * 100f;
	public float FuelValue => MaxFuel - (MaxFuel * rotLevel);
	public float FuelPercent => FuelValue / MaxFuel * 100f;

	public float Toxicity => MaxHealth * rotLevel;
	public bool IsRotten { get; private set; }
	public bool IsToxic { get; private set; }

	[Header("Splat")]
	public GameObject SplatPrefab;        // when destroyed
	public Color SplatColour = Color.red;
	private float destroyDelay = 2f;        // to allow splat particles to play out

	[Header("Score Feedback")]
	public float PerfectPercent = 95f;          // HealthValue >=
	public float RipePercent = 75f;         // HealthValue >=
	public float JuicyPercent = 50f;         // HealthValue >=

	public string PerfectMessage = "PERFECT!!";
	public string RipeMessage = "RIPE!";
	public string JuicyMessage = "JUICY!";
	public string MoonMessage = "MOON BONUS!!";
	public string RottenMessage = "YUK!";
	public string ToxicMessage = "TOXIC!";      // if IsToxic

	public Color PerfectScoreColour = Color.green;
	public Color RipeScoreColour = Color.yellow;
	public Color JuicyScoreColour = Color.cyan;
	public Color OkScoreColour = Color.grey;

	public Color MoonScoreColour = Color.white;
	public Color RottenScoreColour = Color.red;
	public Color ToxicScoreColour = Color.magenta;

	public CardManager.CardBackType CardType = CardManager.CardBackType.Gates;  // TODO: Any!		// to correspond to card type

	[Header("Time Feedback")]
	public string TimeMessage = "TIME BONUS!";
	public Color TimeColour = Color.white;


	public string ScoreMessage => (IsMoon ? MoonMessage :
										IsToxic ? ToxicMessage :
										IsRotten ? RottenMessage :
										HealthPercent >= PerfectPercent ? PerfectMessage :
										HealthPercent >= RipePercent ? RipeMessage :
										HealthPercent >= JuicyPercent ? JuicyMessage : "");

	public Color ScoreColour => (IsMoon ? MoonScoreColour :
										IsToxic ? ToxicScoreColour :
										IsRotten ? RottenScoreColour :
										HealthPercent >= PerfectPercent ? PerfectScoreColour :
										HealthPercent >= RipePercent ? RipeScoreColour :
										HealthPercent >= JuicyPercent ? JuicyScoreColour :
										OkScoreColour);

	[Header("Moon Fruit")]
	public bool IsMoon = false;
	public float MoonFuel = 100f;
	public float MoonHealth = 100f;

	public Light2D MoonLight;
	public Color MoonDisableColour = Color.cyan;
	public PortalEndPoint MoonPortalEntry;

	private float moonDisableTime = 10f;     // moon collider disabled after player 'hit'

	[Header("Audio")]
	public AudioClip HitGroundAudio;
	public AudioClip HitFruitAudio;
	public AudioClip HitMoonAudio;
	public AudioClip HitPlayerAudio;
	public AudioClip ThrowAudio;

	public bool IsThrown { get; private set; }

	private bool isTaken = false;       // by player / thrown fruit collision
	private bool onGround = false;
	private bool onPlatform = false;
	private float rotLevel = 0f;        // from 0 (fresh) to 1 (black)

	private GameManager.GameState gameState;
	private bool GameOver => (gameState == GameManager.GameState.GameOver);

	private SpriteRenderer spriteRenderer;
	private Rigidbody2D rigidBody;
	private Collider2D fruitCollider;
	private Transportable transportable;		// required
	private AudioSource audioSource;			// required
	private Animator fruitAnimator;             // optional

	//private Boss boss;                          // optional
	//private bool IsBoss => (boss != null);

	private void Awake()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		rigidBody = GetComponent<Rigidbody2D>();
		fruitCollider = GetComponent<Collider2D>();
		transportable = GetComponent<Transportable>();
		fruitAnimator = GetComponent<Animator>();
		audioSource = GetComponent<AudioSource>();

		//boss = GetComponentInChildren<Boss>();

		fruitScale = transform.localScale;
		onGround = false;

		if (IsMoon)
		{
			MaxFuel = MoonFuel;
			MaxHealth = MoonHealth;
		}
	}

	private void OnEnable()
	{
		GameEvents.OnGameStateChanged += OnGameStateChanged;
	}

	private void OnDisable()
	{
		GameEvents.OnGameStateChanged -= OnGameStateChanged;
	}

	// set initial fuel, health and launch parameters from SpawnPoint
	private void InitSpawn(SpawnPoint spawnPoint)
	{
		//RotTime = spawnPoint.RotTime;
		MaxFuel = spawnPoint.MaxFuel;
		MaxHealth = spawnPoint.MaxHealth;

		clockTime = spawnPoint.ClockTime;
		timeFactor = spawnPoint.TimeFactor;
		secondsRemaining = 0;

		IsToxic = spawnPoint.IsToxic;			// reduces health / fuel / time

		if (!IsMoon)
		{
			if (spawnPoint.LaunchForce != Vector2.zero)
				rigidBody.AddForce(spawnPoint.LaunchForce, ForceMode2D.Impulse);

			if (Mathf.Abs(spawnPoint.LaunchTorque) > 0)
				rigidBody.AddTorque(spawnPoint.LaunchTorque);

			rigidBody.gravityScale = spawnPoint.GravityScale;
		}

		SetColour(spawnPoint.OverrideColour != Color.clear ? spawnPoint.OverrideColour : FreshColour, RottenColour, SplatColour);
	}

	public void SetColour()
	{
		SetColour(FreshColour, RottenColour, SplatColour);
	}

	private void SetColour(Color freshColour, Color rottenColour, Color splatColour)
	{
		spriteRenderer.color = freshColour;

		GradientColorKey[] colourKeys = new GradientColorKey[2];
		colourKeys[0].color = freshColour;
		colourKeys[0].time = 0f;
		colourKeys[1].color = rottenColour;
		colourKeys[1].time = 1f;

		GradientAlphaKey[] alphaKeys = new GradientAlphaKey[2];
		alphaKeys[0].alpha = 1f;
		alphaKeys[0].time = 0f;
		alphaKeys[1].alpha = 1f;
		alphaKeys[1].time = 1f;

		RotColourGradient.SetKeys(colourKeys, alphaKeys);

		//if (SpriteLight != null)
		//	SpriteLight.color = colour;

		if (PointLight != null)
			PointLight.color = freshColour;

		if (Trail != null)
		{
			var trailMain = Trail.main;
			trailMain.startColor = freshColour;
		}
	}

	private void SplatParticles()
	{
		if (SplatPrefab == null)
			return;

		var splat = Instantiate(SplatPrefab, transform.position, Quaternion.identity);
		var splatParticles = splat.GetComponent<ParticleSystem>();

		var splatMain = splatParticles.main;
		splatMain.startColor = SplatColour;
		splatParticles.Play();

		Destroy(splat, destroyDelay);
	}


	private void OnGameStateChanged(GameManager.GameState currentState, GameManager.GameState newState)
	{
		if (newState == GameManager.GameState.GameOver)
			StopTimer();
	}

	private void StartRotting()
	{
		if (IsMoon)
			return;

		if (IsRotten)
			return;

		spriteRenderer.color = RotColourGradient.Evaluate(0f);

		if (SpriteLight != null)
			SpriteLight.color = RotColourGradient.Evaluate(0f);
		if (PointLight != null)
			PointLight.color = RotColourGradient.Evaluate(0f);

		rotLevel = 0f;

		LeanTween.value(gameObject, 0f, 1f, RotTime)
				.setEaseInCubic()
				.setDelay(RotDelay)
				.setOnUpdate((float f) =>
				{
					rotLevel = f;
					spriteRenderer.color = RotColourGradient.Evaluate(f);

					if (SpriteLight != null)
						SpriteLight.color = RotColourGradient.Evaluate(f);
					if (PointLight != null)
						PointLight.color = RotColourGradient.Evaluate(f);
				})
				.setOnComplete(() =>
				{
					// start falling gradually
					LeanTween.value(gameObject, rigidBody.gravityScale, 1f, FallTime)
							.setEaseInCubic()
							.setOnUpdate((float f) =>
							{
								rigidBody.gravityScale = f;
							});

					IsRotten = true;

					// shrink away slowly
					Shrink(RottenShrinkTime, true);
				});
	}

	private void TurnOff()
	{
		if (SpriteLight != null)
			SpriteLight.gameObject.SetActive(false);
		if (PointLight != null)
			PointLight.gameObject.SetActive(false);

		if (Trail != null)
			Trail.Stop();

		if (fruitAnimator != null)
			fruitAnimator.enabled = false;
	}

	public void Grow(bool thrown)
	{
		if (IsMoon)
			return;

		//var fruitScale = transform.localScale;
		var largeScale = fruitScale * GrowScale;
		transform.localScale = Vector3.zero;

		//fruitCollider.enabled = false;

		if (!thrown)
		{
			fruitCollider.enabled = false;

			LeanTween.scale(gameObject, largeScale, SpawnTime)
					.setEaseInBack()
					.setOnComplete(() =>
					{
						//fruitCollider.enabled = true;

						LeanTween.scale(gameObject, fruitScale, SpawnTime)
							.setEaseOutBack()
							.setOnComplete(() =>
							{
								fruitCollider.enabled = true;

								if (RotTime > 0)
									StartRotting();

								if (clockTime > 0)
									StartTimer();
							});
					});
		}
		else		// thrown - scale up faster and leave collider enabled
		{
			rigidBody.gravityScale = 1f;

			LeanTween.scale(gameObject, fruitScale, SpawnTime * 0.5f)
							.setEaseOutBack()
							.setOnComplete(() =>
							{
								if (RotTime > 0)
									StartRotting();
							});
		}
	}

	public void LaserDestroy()
	{
		Shrink(ShrinkTime, true);
	}

	private void Shrink(float shrinkTime, bool destroy)
	{
		if (IsMoon)
			return;

		if (destroy)
		{
			TurnOff();
			StopTimer();
		}

		LeanTween.scale(gameObject, Vector3.zero, shrinkTime)
					.setEaseInBack()
					.setOnComplete(() =>
					{
						if (destroy)
						{
							SplatParticles();
							//if (Splat != null)
							//	Splat.Play();

							spriteRenderer.enabled = false;
							fruitCollider.enabled = false;

							Destroy(gameObject, destroyDelay);
						}
					});
	}

	private IEnumerator TimeCountdown()
	{
		if (timerRunning)
			yield break;

		if (IsMoon)
			yield break;

		if (secondsRemaining < 0)		// don't countdown negative time
			yield break;

		timerRunning = true;
		secondsRemaining = clockTime;

		while (timerRunning)
		{
			yield return new WaitForSeconds(timerInterval);

			secondsRemaining -= timerInterval;

			var fruitScale = transform.localScale;
			var largeScale = fruitScale * TickScale;

			LeanTween.scale(gameObject, largeScale, TickTime)
					.setEaseInBack()
					.setOnComplete(() =>
					{
						if (TimeTickAudio != null)
						{
							audioSource.clip = TimeTickAudio;
							audioSource.Play();
						}
	
						LeanTween.scale(gameObject, fruitScale, TickTime)
							.setEaseOutBack()
							.setOnComplete(() =>
							{

							});
					});

			if (secondsRemaining <= 0f)
			{
				secondsRemaining = 0f;
				StopTimer();
			}
		}

		yield return null;
	}

	private void StartTimer()
	{
		StopTimer();
		timeCountdownCoroutine = StartCoroutine(TimeCountdown());
	}

	private void StopTimer()
	{
		if (timeCountdownCoroutine != null)
		{
			StopCoroutine(timeCountdownCoroutine);
			timeCountdownCoroutine = null;
		}

		timerRunning = false;
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag("Ground"))
		{
			if (! IsThrown)
				HitGround();
		}

		if (transportable.InTransit)
			return;

		// thrown fruit can damage obstacles
		if (collision.gameObject.CompareTag("Obstacle"))
		{
			if (IsThrown && ObstacleDamage > 0)
			{
				var obstacle = collision.gameObject.GetComponent<Obstacle>();

				if (obstacle.IsBreakable && !obstacle.BreakByBombOnly && obstacle.Strength > 0)
				{
					obstacle.TakeDamage(ObstacleDamage);
					ObstacleDamage = 0;		// damage on first hit only
				}
			}
		}

		if (collision.gameObject.CompareTag("Wall"))
		{
			if (DestroyOnHitWall)
				Shrink(ShrinkTime, true);
		}

		// fruit hit by thrown fruit is same as player colliding with it
		// ... until thrown fruit becomes 'rotten'
		if (collision.gameObject.CompareTag("Fruit"))
		{
			if (IsMoon)
				return;

			//Debug.Log($"Fruit {name} hit Fruit!");
			if (IsThrown && ! IsRotten)
			{
				var otherFruit = collision.gameObject.GetComponent<Fruit>();

				if (otherFruit.IsMoon)		// have to hit moon 'in person'
					return;

				if (! GameOver)
				{
					//if (otherFruit.IsMoon)
					//{
					//	Debug.Log($"Fruit {name} hit Moon!");
					//	StartCoroutine(otherFruit.MoonDisable(null)); // temp disable collider

					//	GameEvents.OnPlayerTakeFruit?.Invoke(otherFruit.FuelValue, otherFruit.HealthValue, otherFruit.IsRotten, otherFruit.Toxicity, otherFruit.secondsRemaining * timeFactor, otherFruit.IsToxic,
					//							otherFruit.CardType, otherFruit.ScoreColour, transform.position, otherFruit.ScoreMessage);

					//	if (HitMoonAudio != null)
					//	{
					//		audioSource.clip = HitMoonAudio;
					//		audioSource.Play();
					//	}
					//}
					//else if (!otherFruit.isTaken)
					if (! otherFruit.isTaken)
					{
						otherFruit.isTaken = true;
						GameEvents.OnPlayerTakeFruit?.Invoke(otherFruit.FuelValue, otherFruit.HealthValue, otherFruit.IsRotten, otherFruit.Toxicity, otherFruit.secondsRemaining * timeFactor, otherFruit.IsToxic,
												otherFruit.CardType, otherFruit.ScoreColour, transform.position, otherFruit.ScoreMessage);

						if (HitFruitAudio != null)
						{
							audioSource.clip = HitFruitAudio;
							audioSource.Play();
						}
	
						otherFruit.Shrink(ShrinkTime, true);
					}
				}
			}
		}

		if (collision.gameObject.CompareTag("Player"))
		{
			//Debug.Log($"Fruit {name} hit Player! ");

			if (!GameOver)
			{
				if (IsMoon)
				{
					var stickFigure = collision.gameObject.GetComponent<StickFigure>();

					Debug.Log($"Player {name} hit Moon!");

					StartCoroutine(MoonDisable(stickFigure)); // temp disable collider

					GameEvents.OnPlayerTakeFruit?.Invoke(FuelValue, HealthValue, IsRotten, Toxicity, secondsRemaining * timeFactor, IsToxic,
												CardType, ScoreColour, transform.position, ScoreMessage);

					GameEvents.OnMoonBossReset?.Invoke();

					if (HitMoonAudio != null)
					{
						audioSource.clip = HitMoonAudio;
						audioSource.Play();
					}
				}
				else if (!IsThrown || onGround)
				{
					if (!isTaken)
					{
						isTaken = true;
						GameEvents.OnPlayerTakeFruit?.Invoke(FuelValue, HealthValue, IsRotten, Toxicity, secondsRemaining * timeFactor, IsToxic,
													CardType, ScoreColour, transform.position, ScoreMessage);
					}

					Shrink(ShrinkTime, true);

					if (HitPlayerAudio != null)
					{
						audioSource.clip = HitPlayerAudio;
						audioSource.Play();
					}
				}
			}
		}
	}


	// collider disabled after player 'hit'
	private IEnumerator MoonDisable(StickFigure stickFigure)
	{
		if (! IsMoon)
			yield break;

		MoonLight.color = MoonDisableColour;
		fruitCollider.enabled = false;

		// teleport stick figure through moon's portal while collider disabled
		if (stickFigure != null && MoonPortalEntry != null)
			stickFigure.ResetTeleport(MoonPortalEntry);

		yield return new WaitForSeconds(moonDisableTime);

		fruitCollider.enabled = true;
		MoonLight.color = Color.white;
	}

	private void HitGround()
	{
		if (!onGround)
		{
			Land(true);
			onGround = true;
		}
	}

	private void Stop()
	{
		rigidBody.velocity = Vector2.zero;
		rigidBody.angularVelocity = 0;
	}


	private void Land(bool stop)
	{
		// TODO: splat animation?

		if (stop)
			Stop();

		if (HitGroundAudio != null)
		{
			audioSource.clip = HitGroundAudio;
			audioSource.Play();
		}
	}

	public void Throw(Vector2 throwPoint, Vector2 direction, float force)
	{
		transform.position = throwPoint;

		Grow(true);

		IsThrown = true;
		rigidBody.AddForce(direction * force, ForceMode2D.Impulse);
		rigidBody.AddTorque(ThrowTorque * force, ForceMode2D.Impulse);

		if (ThrowAudio != null)
		{
			audioSource.clip = ThrowAudio;
			audioSource.Play();
		}
	}

	public void Spawn(SpawnPoint spawnPoint)
	{
		//Debug.Log($"Spawn: {spawnPoint.name}");

		InitSpawn(spawnPoint);
		Grow(false);

		if (SpawnAudio != null)
		{
			audioSource.clip = SpawnAudio;
			audioSource.Play();
		}
	}

	private void OnDrawGizmos()
	{
		if (IsMoon)
		{
			Gizmos.color = Color.white;
			Gizmos.DrawWireSphere(transform.position, 5f);
		}
	}
}