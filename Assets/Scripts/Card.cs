﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;


[RequireComponent(typeof(AudioSource))]

public class Card : MonoBehaviour
{
    public CardManager CardManager;             // cards placed in scene, not instantiated

    public CardFace CardFront;
    public CardFace CardBack;

    public Light2D Hilight;
    private float flashOnDuration = 1f;
    private float flashOffDuration = 0.5f;

    [Header("Audio")]
    public AudioClip FlipAudio;
    public AudioClip FlippedAudio;

    public int FrontTimeOut = 10;               // time front face is shown before flipping back
    private int frontTimeRemaining = 6;         // front face remaining time

    private float flipTime = 0.25f;

    public Card PairedWith { get; private set; }

    public CardManager.CardBackType BackType { get; private set; }
    public CardManager.CardFrontType FrontType { get; private set; }
    public int CardValue { get; private set; }      // score factor

    public bool BackShowing { get; private set; }

    private bool flipping = false;


    private BoxCollider2D boxCollider;
    private AudioSource audioSource;


    // Start is called before the first frame update
    void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();     // trigger
        audioSource = GetComponent<AudioSource>();

        BackShowing = true;
        SetFrontBack();
    }

    private void SetFrontBack()
    {
        CardFront.gameObject.SetActive(!BackShowing);
        CardBack.gameObject.SetActive(BackShowing);
    }

    public void ShowBack()
    {
        if (BackShowing)
            return;

        Flip(true, false);
    }

    public void ShowFront(bool countdown)
    {
        if (! BackShowing)
            return;

        Flip(true, countdown);
    }

    private void Flip(bool audio, bool countdown)
    {
        if (flipping)
            return;

        flipping = true;

        if (audio && FlipAudio != null)
        {
            audioSource.clip = FlipAudio;
            audioSource.Play();
        }

        LeanTween.rotateAround(GetComponent<RectTransform>(), Vector3.up, 90f, flipTime)
                    .setEaseOutCubic()
                    .setOnComplete(() =>
                    {
                        LeanTween.rotateAround(GetComponent<RectTransform>(), Vector3.up, -90f, flipTime)
                            //.setDelay(0.5f)
                            .setEaseInCubic()
							.setOnStart(() =>
							{
                                // flip back/front sprites etc
                                BackShowing = !BackShowing;
                                SetFrontBack();
                            })
							.setOnComplete(() =>
                            {
                                flipping = false;

                                if (audio & FlippedAudio != null)
                                {
                                    audioSource.clip = FlippedAudio;
                                    audioSource.Play();
                                }
        
                                if (!BackShowing)
                                {
                                    if (countdown)
                                    {
                                        frontTimeRemaining = FrontTimeOut;
                                        StartCoroutine(FrontShowCountdown());
                                    }
                                }
                                else
                                    frontTimeRemaining = 0;
                            });
                    });
    }

    private IEnumerator FrontShowCountdown()
    {
        if (BackShowing)
            yield break;

        if (frontTimeRemaining <= 0)
            yield break;

        CardFront.Countdown.text = frontTimeRemaining.ToString();

        while (frontTimeRemaining > 0)
        {
            yield return new WaitForSeconds(1f);
            frontTimeRemaining--;

            CardFront.Countdown.text = frontTimeRemaining.ToString();
            yield return null;
        }

        CardFront.Countdown.text = frontTimeRemaining.ToString();
        Flip(true, false);
    }

	//private void OnTriggerEnter2D(Collider2D collision)
	//{
 //       if (collision.gameObject.CompareTag("Fruit"))
 //       {
 //           //Flip(true);
 //       }
 //       else if (collision.gameObject.CompareTag("Player"))
 //       {
	//		//Flip(true);
	//	}
 //   }

    public IEnumerator Flash(int numFlashes)
    {
        for (int i = 0; i < numFlashes; i++)
        {
            Hilight.enabled = true;
            yield return new WaitForSeconds(flashOnDuration);
            Hilight.enabled = false;
            yield return new WaitForSeconds(flashOffDuration);
        }
    }

    // card front represents health, fuel or time +/-
    // card back represents a certain character (aka fruit)

    // currently, each card represents a character/clock and a score factor
    // front type is not yet considered
    public void SetupCard(CardManager.CardFrontType frontType, CardManager.CardBackType backType, int frontValue)
    {
        FrontType = frontType;
        BackType = backType;
        var frontSprite = CardManager.CardFrontSprites[(int)frontType];
        var backSprite = CardManager.CardBackSprites[(int)backType];

        //CardFront.TopLeftSprite.sprite = frontSprite;
        //CardFront.BottomRightSprite.sprite = frontSprite;
        CardFront.TopLeftSprite.sprite = backSprite;
        CardFront.BottomRightSprite.sprite = backSprite;
        //CardFront.CentreSprite.sprite = frontSprite;

        CardFront.SetValue(frontValue);         // * (health + fuel + time)

        // corners of card backs are logos
        //CardBack.TopLeftSprite.sprite = backSprite;
        //CardBack.BottomRightSprite.sprite = backSprite;
        CardBack.CentreSprite.sprite = backSprite;
    }
}
