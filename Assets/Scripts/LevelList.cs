﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelList", menuName = "LevelList", order = 1)]
public class LevelList : ScriptableObject
{
	public List<GameLevel> Levels;

	public GameLevel CurrentLevel { get; private set; }


	private void OnEnable()
	{
		GameEvents.OnSceneSelected += OnSceneSelected;
	}

	private void OnDisable()
	{
		GameEvents.OnSceneSelected -= OnSceneSelected;
	}

	private void OnSceneSelected(GameLevel level)
	{
		CurrentLevel = level;
	}
}