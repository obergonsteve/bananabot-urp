﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardManager : MonoBehaviour
{
    public List<Card> CardsInPlay = new List<Card>();

    public enum CardBackType
    {
        //None = 0,
        Gates = 0,
        Andrews = 1,
        Clinton = 2,
        Fauci = 3,
        Biden = 4
    }

    public enum CardFrontType
    {
        //None = 0,
        Health = 0,
        Fuel = 1,
        Time = 2
    }

    private int minScoreFactor = 2;
    private int maxScoreFactor = 20;

    public List<Sprite> CardBackSprites = new List<Sprite>();       // indexed by CardBackType enum value
    public CardBackType RandomCardBackType
    {
        get
        {
            var array = Enum.GetValues(typeof(CardBackType));
            return (CardBackType) array.GetValue(UnityEngine.Random.Range(0, array.Length));
        }
    }

    public List<Sprite> CardFrontSprites = new List<Sprite>();      // indexed by CardFrontType enum value
    public CardFrontType RandomCardFrontType
    {
        get
        {
            var array = Enum.GetValues(typeof(CardFrontType));
            return (CardFrontType)array.GetValue(UnityEngine.Random.Range(0, array.Length));
        }
    }

	private void OnEnable()
	{
        GameEvents.OnTapToPlay += OnTapToPlay;
    }

	private void OnDisable()
    {
        GameEvents.OnTapToPlay -= OnTapToPlay;
    }


    private void OnTapToPlay(GameScores scores)
	{
        ShuffleCards();
    }

	private void ShuffleCards()
    {
        // TODO set random sequence

        foreach (var card in CardsInPlay)
        {
            // reset all cards to back showing
            card.ShowBack();
            card.SetupCard(RandomCardFrontType, RandomCardBackType, UnityEngine.Random.Range(minScoreFactor, maxScoreFactor));
        }
    }

    public int ShowingCardsFactor(CardBackType backType)
    {
        int totalCardValue = 0;

        foreach (var card in CardsInPlay)
        {
            // only consider cards with front showing
            if (card.BackShowing)
                continue;

            if (card.BackType == backType)
                totalCardValue += card.CardValue;
        }

        //Debug.Log($"ShowingCardsFactor {backType} totalCardValue = {totalCardValue}");
        return totalCardValue;
    }
}
