﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


public class LeaderboardUI : MonoBehaviour
{
	public LevelList LevelList;

	[Header("Player / School")]
	public Text PlayerName;
	public Text LevelName;

	public Button AllScoresButton;     // all player scores leaderboard
	public Button SchoolScoresButton;     // school scores leaderboard

	public Image RegisterPanel;

	public InputField PlayerNameInput;
	public Text NameMessage;
	public Dropdown SchoolDropdown;

	public Button RegisterPlayerButton;

	public AudioClip ErrorAudio;

	[Header("Leaderboard")]
	public Image LeaderboardPanel;
	public Transform LeaderboardContents;           // ScrollView content
	public Image NoInternet;
	public Text RetrievingScores;

	public GameObject EntryPrefab;
	public int TopScoreCount = 100;

	public Color PlayerColour;          // colour for this player
	public Color OtherPlayerColour;           // colour for other players

	public Color HomeSchoolColour;      // colour for this player's school
	public Color OtherSchoolColour;      // colour for other schools

	public Button CloseButton;

	public PlayerProfile PlayerProfile;
	public GameManager GameManager;

	private bool leaderboardShowing = false;
	private bool populatingScores = false;
	private List<string> schoolList;
	private string playerName = "";
	private string selectedSchool = "";

	private bool BelongsToSchool => !string.IsNullOrEmpty(PlayerProfile.SchoolName);


	public enum LeaderboardType
	{
		None,
		Player,
		School,
	}

	public LeaderboardType LeaderboardView;

	private float resultsPauseTime = 0.2f;
	private int playerBoardCount = 10;       // for animation / audio


	private void OnEnable()
	{
		AllScoresButton.onClick.AddListener(OnAllScoresButtonClicked);
		SchoolScoresButton.onClick.AddListener(OnSchoolScoresButtonClicked);

		PlayerNameInput.onValueChanged.AddListener(OnPlayerNameChanged);
		SchoolDropdown.onValueChanged.AddListener(OnSchoolChanged);

		RegisterPlayerButton.onClick.AddListener(OnRegisterButtonClicked);
		CloseButton.onClick.AddListener(OnCloseButtonClicked);

		// leaderboard callbacks
		GameEvents.OnLookupPlayerProfileResult += OnLookupPlayerProfileResult;
		GameEvents.OnSavePlayerProfileResult += OnPlayerNameSaved;
		GameEvents.OnPlayerScoresRetrieved += OnPlayerScoresRetrieved;
		GameEvents.OnSchoolScoresRetrieved += OnSchoolScoresRetrieved;
		GameEvents.OnPlayerScoresUpdated += OnPlayerScoresUpdated;
		GameEvents.OnSchoolsRetrieved += OnSchoolsRetrieved;
	}

	private void OnDisable()
	{
		AllScoresButton.onClick.RemoveListener(OnAllScoresButtonClicked);
		SchoolScoresButton.onClick.RemoveListener(OnSchoolScoresButtonClicked);

		PlayerNameInput.onValueChanged.RemoveListener(OnPlayerNameChanged);
		SchoolDropdown.onValueChanged.RemoveListener(OnSchoolChanged);

		RegisterPlayerButton.onClick.RemoveListener(OnRegisterButtonClicked);
		CloseButton.onClick.RemoveListener(OnCloseButtonClicked);

		GameEvents.OnLookupPlayerProfileResult -= OnLookupPlayerProfileResult;
		GameEvents.OnSavePlayerProfileResult -= OnPlayerNameSaved;
		GameEvents.OnPlayerScoresRetrieved -= OnPlayerScoresRetrieved;
		GameEvents.OnSchoolScoresRetrieved -= OnSchoolScoresRetrieved;
		GameEvents.OnPlayerScoresUpdated -= OnPlayerScoresUpdated;
		GameEvents.OnSchoolsRetrieved -= OnSchoolsRetrieved;
	}


	public void Populate()
	{
		SetLeveName();
		GetPlayerAndSchoolName();
		InitScoresButtons();

		bool internetReachable = LeaderboardManager.InternetReachable;

		NoInternet.gameObject.SetActive(!internetReachable);

		if (internetReachable)
		{
			GetSchools();
			GetLeaderboardScores(LeaderboardType.Player);
		}
	}

	// Current level
	private void SetLeveName()
	{
		LevelName.text = "\"" + (LevelList.CurrentLevel != null ? LevelList.CurrentLevel.LevelName : "??") + "\"";
	}


	private void GetSchools()
	{
		selectedSchool = "";
		GameEvents.OnLookupSchools?.Invoke();
	}

	// Player name

	private void GetPlayerAndSchoolName()
	{
		playerName = "";

		if (!string.IsNullOrEmpty(PlayerProfile.PlayerName))     // name already entered - only recorded in PlayerProfile on successful db save
		{
			SetPlayerAndSchool();
		}
		else  // prompt for player / school name
		{
			RegisterPanel.gameObject.SetActive(true);
			LeaderboardPanel.gameObject.SetActive(false);
			leaderboardShowing = false;
		}
	}

	private void SetPlayerAndSchool()
	{
		RegisterPanel.gameObject.SetActive(false);
		LeaderboardPanel.gameObject.SetActive(true);
		leaderboardShowing = true;

		PlayerName.text = PlayerProfile.PlayerName;

		if (BelongsToSchool)
			PlayerName.text += " @ " + PlayerProfile.SchoolName;
	}


	// scores buttons only relevant if the player belongs to a school
	private void InitScoresButtons()
	{
		AllScoresButton.gameObject.SetActive(BelongsToSchool);
		SchoolScoresButton.gameObject.SetActive(BelongsToSchool);
	}

	// save name / school in db
	private void OnRegisterButtonClicked()
	{
		if (string.IsNullOrEmpty(playerName))
			return;

		// check db for duplicate player / invalid school name
		GameEvents.OnLookupPlayerProfile?.Invoke(playerName, selectedSchool);

		RegisterPlayerButton.interactable = false;
	}

	// async callback
	private void OnLookupPlayerProfileResult(string playerName, string schoolName, bool playerOk, bool schoolOk)
	{
		if (playerOk)              // name not already registered
		{
			NameMessage.text = "";
		}
		else
		{
			if (ErrorAudio != null)
				AudioSource.PlayClipAtPoint(ErrorAudio, Vector3.zero);

			NameMessage.text = "'" + playerName + "' is already taken!";
		}

		if (playerOk && schoolOk)
		{
			GameEvents.OnSavePlayerProfile?.Invoke(playerName, schoolName, PlayerProfile);
		}

		RegisterPlayerButton.interactable = true;
	}


	// async callback
	private void OnPlayerNameSaved(string playerName, bool success, bool loadScores)
	{
		if (success)
		{
			PlayerName.text = playerName;
			PlayerProfile.PlayerName = playerName;

			SetPlayerAndSchool();
			InitScoresButtons();

			// populate leaderboard
			if (loadScores)
				LookupScores();
		}
	}


	private void GetLeaderboardScores(LeaderboardType view)
	{
		LeaderboardView = view;
		LookupScores();
	}

	private void OnAllScoresButtonClicked()
	{
		GetLeaderboardScores(LeaderboardType.Player);
		AllScoresButton.interactable = false;
	}

	private void OnSchoolScoresButtonClicked()
	{
		GetLeaderboardScores(LeaderboardType.School);
		SchoolScoresButton.interactable = false;
	}


	// remove any unwanted chars (eg. firebase illegal), profanity, etc.
	private void OnPlayerNameChanged(string name)
	{
		playerName = LeaderboardManager.CleanName(name);
		if (playerName != name)       // illegal chars stripped out
		{
			if (ErrorAudio != null)
				AudioSource.PlayClipAtPoint(ErrorAudio, Vector3.zero);

			PlayerNameInput.text = playerName;
			NameMessage.text = "";
		}
	}


	private void OnSchoolChanged(int index)
	{
		if (index == 0)
			selectedSchool = "";        // no school
		else
			selectedSchool = schoolList[index];
	}


	private void OnSchoolsRetrieved(List<string> schools)
	{
		schoolList = schools;

		SchoolDropdown.ClearOptions();
		SchoolDropdown.AddOptions(schools);
	}

	public void LookupScores()
	{
		if (populatingScores)
			return;

		switch (LeaderboardView)
		{
			case LeaderboardType.None:
				break;

			case LeaderboardType.Player:
				GameEvents.OnLookupPlayerScores?.Invoke();
				populatingScores = true;
				//RetrievingScores.gameObject.SetActive(true);		// TODO: reinstate?
				break;

			case LeaderboardType.School:
				GameEvents.OnLookupSchoolScores?.Invoke(PlayerProfile.SchoolName);
				populatingScores = true;
				//RetrievingScores.gameObject.SetActive(true);		// TODO: reinstate?
				break;
		}

	}

	private void OnPlayerScoresRetrieved(List<LeaderboardScore> scores)
	{
		if (scores == null)
			return;

		if (!leaderboardShowing)
			return;

		StartCoroutine(PopulateLeaderboard(scores));

		AllScoresButton.interactable = false;
		SchoolScoresButton.interactable = true;
	}

	private void OnSchoolScoresRetrieved(string schoolName, List<LeaderboardScore> scores)
	{
		if (scores == null)
			return;

		if (!leaderboardShowing)
			return;

		StartCoroutine(PopulateLeaderboard(scores));

		AllScoresButton.interactable = true;
		SchoolScoresButton.interactable = false;
	}


	private IEnumerator PopulateLeaderboard(List<LeaderboardScore> scores)
	{
		float initAngle = 90f;
		int scoreCount = 0;

		ClearLeaderboard();
		RetrievingScores.gameObject.SetActive(false);

		var sortedScores = scores.OrderByDescending(x => x.PlayerScore).Take(TopScoreCount).ToList();

		foreach (LeaderboardScore score in sortedScores)
		{
			if (!populatingScores)
				yield break;

			yield return new WaitForSeconds(resultsPauseTime);

			var newEntry = Instantiate(EntryPrefab, LeaderboardContents);
			LeaderboardEntry entry = newEntry.GetComponent<LeaderboardEntry>();

			if (++scoreCount <= playerBoardCount)
			{
				entry.transform.localRotation = Quaternion.Euler(new Vector2(initAngle, 0f));     // ie. not visible
				entry.FlipIn(initAngle, true);
			}

			entry.Rank.text = scoreCount.ToString();
			entry.SchoolName.text = score.SchoolName;
			entry.PlayerName.text = score.PlayerName;
			entry.Score.text = String.Format("{0:N0}", score.PlayerScore);      // thousands separator

			entry.LBButton.GetComponent<Image>().color = score.PlayerName == PlayerProfile.PlayerName ? PlayerColour : OtherPlayerColour;
			entry.SchoolName.color = (BelongsToSchool && score.SchoolName == PlayerProfile.SchoolName) ? HomeSchoolColour : OtherSchoolColour;
		}

		populatingScores = false;
		yield return null;
	}


	public void CancelPopulate()
	{
		populatingScores = false;
	}


	private void OnPlayerScoresUpdated(GameScores scores, bool success, bool reloadScores)
	{
		if (leaderboardShowing && success && reloadScores)
		{
			LookupScores();
		}
	}

	private void ClearLeaderboard()
	{
		foreach (Transform entry in LeaderboardContents)
		{
			Destroy(entry.gameObject);
		}
	}

	private void OnCloseButtonClicked()
	{
		GameEvents.OnToggleLeaderboard?.Invoke();
	}
}
