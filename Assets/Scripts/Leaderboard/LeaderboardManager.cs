﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using System.Threading.Tasks;
using System.Linq;

public class LeaderboardManager : MonoBehaviour
{
	public LevelList LevelList;
	public PlayerProfile PlayerProfile;

	private DatabaseReference databaseRoot;
	private const string dbURL = "https://wholeself-bananabot-default-rtdb.firebaseio.com/";

	private string CurrentLevelName => LevelList.CurrentLevel != null ? LevelList.CurrentLevel.LevelName : "Unknown Level";

	public static char SchoolSeparator = '@';      // used to separate player name and school

	private GameScores scoresToUpload = null;       // cached until player registers for leaderboard


	public static bool InternetReachable => (Application.internetReachability != NetworkReachability.NotReachable);


	//private bool InternetReachable
	//{
	//	get
	//	{
	//		bool internetReachable = (Application.internetReachability != NetworkReachability.NotReachable);
	//		//bool localNetworkReachable = (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork);

	//		BeatEvents.OnInternetCheck?.Invoke(internetReachable);
	//		return internetReachable;
	//	}
	//}


	private void OnEnable()
	{
		GameEvents.OnLookupSchools += RetrieveSchoolNames;
		GameEvents.OnLookupPlayerProfile += ValidatePlayerAndSchool;
		GameEvents.OnSavePlayerProfile += SavePlayerProfile;
		GameEvents.OnLookupPlayerScores += RetrievePlayerScores;
		GameEvents.OnLookupSchoolScores += RetrieveSchoolScores;
		GameEvents.OnNewPersonalBest += OnNewPersonalBest;      // save score
	}

	private void OnDisable()
	{
		GameEvents.OnLookupSchools -= RetrieveSchoolNames;
		GameEvents.OnLookupPlayerProfile -= ValidatePlayerAndSchool;
		GameEvents.OnSavePlayerProfile -= SavePlayerProfile;
		GameEvents.OnLookupPlayerScores -= RetrievePlayerScores;
		GameEvents.OnLookupSchoolScores -= RetrieveSchoolScores;
		GameEvents.OnNewPersonalBest -= OnNewPersonalBest;
	}

	private void Start()
	{
		// set the DB url before calling into the realtime database
		FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(dbURL);

		// get the root reference location of the database
		databaseRoot = FirebaseDatabase.DefaultInstance.RootReference;

		//SetupLevelNodes();
	}

	//private void SetupLevelNodes()
	//{
	//	Dictionary<string, object> childUpdates = new Dictionary<string, object>();

	//	foreach (var levelName in GameManager.LevelNames)
	//	{
	//		string playerScores = String.Format("{0}/{1}", levelName, "playerScores");
	//		string schoolScores = String.Format("{0}/{1}", levelName, "schoolScores");

	//		childUpdates[playerScores] = "";
	//		childUpdates[playerScores] = levelName;
	//	}

	//	databaseRoot.UpdateChildrenAsync(childUpdates).ContinueWith(task =>
	//	{
	//		if (task.IsCompleted)
	//		{
	//			Debug.Log("SetupLevelNodes success!");
	//		}
	//		else
	//		{
	//			Debug.Log("SetupLevelNodes: error = " + task.Exception.Message);
	//		}
	//	}, TaskScheduler.FromCurrentSynchronizationContext());     // task continuation on UI thread
	//}

	public void RetrieveSchoolNames()
	{
		var schoolsNode = databaseRoot.Child("schools");

		schoolsNode.GetValueAsync().ContinueWith(task =>
		{
			if (task.IsCompleted)
			{
				if (task.Result == null || task.Result.Value == null)
				{
					Debug.Log("RetrieveSchoolNames failed!");
				}
				else
				{
					DataSnapshot snapshot = task.Result;
					var schools = snapshot.Children;

					// TODO: prolly a more elegant way of making a list!
					var schoolList = new List<string>();
					schoolList.Add("No School");        // blank school at index 0

					foreach (var school in schools)
					{
						schoolList.Add(school.Key);
					}

					//Debug.Log($"RetrieveSchoolNames {schoolList.Count}");
					GameEvents.OnSchoolsRetrieved?.Invoke(schoolList);
				}
			}
			else
			{
				Debug.Log("RetrieveSchoolNames: error = " + task.Exception.Message);
			}
		}, TaskScheduler.FromCurrentSynchronizationContext());     // task continuation on UI thread
	}


	private void ValidatePlayerAndSchool(string playerName, string schoolName)
	{
		if (string.IsNullOrEmpty(playerName))
			return;

		var cleanPlayerName = CleanName(playerName);    // make sure name is clean of any illegal characters
		var playerNode = databaseRoot.Child(CurrentLevelName + "/players").Child(cleanPlayerName);

		playerNode.GetValueAsync().ContinueWith(task =>
		{
			if (task.IsCompleted)
			{
				if (task.Result == null || task.Result.Value == null)        // playerName does not exist
				{
					Debug.Log("ValidatePlayerAndSchool: player " + cleanPlayerName + " not found!");
					// player name ok (as it does not already exist), so check the school name exists...
					ValidateSchool(playerName, schoolName);
				}
				else
				{
					Debug.Log("ValidatePlayerAndSchool: player " + cleanPlayerName + " found");
					GameEvents.OnLookupPlayerProfileResult?.Invoke(cleanPlayerName, schoolName, false, true);
				}
			}
		}, TaskScheduler.FromCurrentSynchronizationContext());     // task continuation on UI thread
	}


	// assumes player name is ok (ie. does not exist)
	private void ValidateSchool(string playerName, string schoolName)
	{
		if (string.IsNullOrEmpty(schoolName))
		{
			GameEvents.OnLookupPlayerProfileResult?.Invoke(playerName, schoolName, true, true);
		}
		else
		{
			var cleanSchoolName = CleanName(schoolName);    // make sure name is clean of any illegal characters
			var schoolNode = databaseRoot.Child("schools").Child(cleanSchoolName);

			schoolNode.GetValueAsync().ContinueWith(task =>
			{
				if (task.IsCompleted)
				{
					if (task.Result == null || task.Result.Value == null)        // schoolName does not exist
					{
						Debug.Log("ValidateSchool: school " + cleanSchoolName + " not found!");
						GameEvents.OnLookupPlayerProfileResult?.Invoke(playerName, schoolName, true, false);
					}
					else
					{
						Debug.Log("ValidateSchool: school " + cleanSchoolName + " found");
						GameEvents.OnLookupPlayerProfileResult?.Invoke(playerName, cleanSchoolName, true, true);
					}
				}
			}, TaskScheduler.FromCurrentSynchronizationContext());     // task continuation on UI thread
		}
	}

	private void SavePlayerProfile(string playerName, string schoolName, PlayerProfile profile)
	{
		if (!string.IsNullOrEmpty(playerName))
		{
			profile.PlayerName = playerName;
			profile.DateCreated = DateTime.Now;
			profile.SchoolName = schoolName;

			string player = String.Format("{0}{1}", "/players/", playerName);
			string dateCreated = String.Format("{0}/{1}", player, "dateCreated");
			string lastPlay = String.Format("{0}/{1}", player, "lastPlay");
			string school = String.Format("{0}/{1}", player, "school");

			//string playCount = String.Format("{0}/{1}", player, "playCount");

			Dictionary<string, object> childUpdates = new Dictionary<string, object>();
			childUpdates[dateCreated] = profile.DateCreated.ToUniversalTime().ToShortDateString() + " " + profile.DateCreated.ToUniversalTime().ToShortTimeString();
			//childUpdates[playCount] = profile.PlayCount;
			childUpdates[lastPlay] = profile.LastPlay.ToUniversalTime().ToString();
			childUpdates[school] = profile.SchoolName;

			databaseRoot.UpdateChildrenAsync(childUpdates).ContinueWith(task =>
			{
				if (task.IsCompleted)
				{
					Debug.Log("SavePlayerProfile: " + playerName + " success!");

					// load scores if no scores to be uploaded
					GameEvents.OnSavePlayerProfileResult?.Invoke(playerName, true, scoresToUpload != null);

					if (scoresToUpload != null)
					{
						OnNewPersonalBest(scoresToUpload, true);        // load scores on completion
						//scoresToUpload = null;
					}
				}
				else
				{
					Debug.Log("SavePlayerProfile: error = " + task.Exception.Message);
					GameEvents.OnSavePlayerProfileResult?.Invoke(playerName, false, false);
				}
			}, TaskScheduler.FromCurrentSynchronizationContext());     // task continuation on UI thread
		}
	}

	// post player score to leaderboard
	// the school name is added to the playername as the compound key value for the playerScores node
	// realodScores true if score cached (scoresToUpload) until player/school registered
	private void OnNewPersonalBest(GameScores scores, bool reloadScores)
	{
		Debug.Log("OnNewPersonalBest: " + scores.RunningScore);

		if (PlayerProfile.HasRegisteredName)
		{
			// atomic updates of new personal best 
			Dictionary<string, object> childUpdates = new Dictionary<string, object>();

			var playerName = PlayerProfile.PlayerName;

			if (!string.IsNullOrEmpty(PlayerProfile.SchoolName))
			{
				childUpdates["/" + CurrentLevelName + "/schoolScores/" + PlayerProfile.SchoolName + "/" + playerName] = scores.PersonalBest;

				// append the school name to the player name for playerScores leaderboard
				playerName += SchoolSeparator + PlayerProfile.SchoolName;
			}

			childUpdates["/" + CurrentLevelName + "/playerScores/" + playerName] = scores.PersonalBest;

			databaseRoot.UpdateChildrenAsync(childUpdates).ContinueWith(task =>
			{
				if (task.IsCompleted)
				{
					GameEvents.OnPlayerScoresUpdated?.Invoke(scores, true, reloadScores);

					if (reloadScores)
						scoresToUpload = null;
				}
				else
				{
					//Debug.Log("OnNewPersonalBest: error = " + task.Exception.Message);
					GameEvents.OnPlayerScoresUpdated?.Invoke(scores, false, false);
				}
			}, TaskScheduler.FromCurrentSynchronizationContext());     // task continuation on UI thread
		}
		else
			scoresToUpload = scores;            // once registered
	}

	// gets a list of the scores for all players
	private void RetrievePlayerScores()
	{
		var playerScoresNode = databaseRoot.Child(CurrentLevelName + "/playerScores");

		playerScoresNode.GetValueAsync().ContinueWith(task =>
		{
			if (task.IsCompleted)
			{
				if (task.Result == null || task.Result.Value == null)
				{
					Debug.Log($"RetrievePlayerScores failed! - {CurrentLevelName}");
					// no scores ... 'return' empty list
					GameEvents.OnPlayerScoresRetrieved?.Invoke(new List<LeaderboardScore>());
				}
				else
				{
					DataSnapshot snapshot = task.Result;
					var scores = snapshot.Children;

					var leaderboardScores = new List<LeaderboardScore>();
					Debug.Log($"RetrievePlayerScores - {CurrentLevelName}");

					foreach (var score in scores)
					{
						string playerName = score.Key;
						string schoolName = "";

						string[] nameSplit = playerName.Split(SchoolSeparator);

						// if there is a second element, use it as the school name
						if (nameSplit.Length == 2)
						{
							playerName = nameSplit[0];
							schoolName = nameSplit[1];
						}

						Debug.Log($"RetrievePlayerScores PlayerName {playerName} PlayerScore {(long)score.Value}");

						var playerScore = new LeaderboardScore { PlayerName = playerName, PlayerScore = (long)score.Value, SchoolName = schoolName };
						leaderboardScores.Add(playerScore);
					}

					GameEvents.OnPlayerScoresRetrieved?.Invoke(leaderboardScores);
				}
			}
			else
			{
				Debug.Log("RetrievePlayerScores: error = " + task.Exception.Message);
			}
		}, TaskScheduler.FromCurrentSynchronizationContext());     // task continuation on UI thread
	}

	//private void SchoolNode_ValueChanged(object sender, ValueChangedEventArgs e)
	//{
	//	throw new NotImplementedException();
	//}

	private void RetrieveSchoolScores(string schoolName)
	{
		var schoolScoresNode = databaseRoot.Child(CurrentLevelName + "/schoolScores").Child(schoolName);

		schoolScoresNode.GetValueAsync().ContinueWith(task =>
		{
			if (task.IsCompleted)
			{
				if (task.Result == null || task.Result.Value == null)
				{
					Debug.Log("RetrieveSchoolScores failed!");
					// no scores ... 'return' empty list
					GameEvents.OnSchoolScoresRetrieved?.Invoke(schoolName, new List<LeaderboardScore>());
				}
				else
				{
					DataSnapshot snapshot = task.Result;
					var scores = snapshot.Children;

					var leaderboardScores = new List<LeaderboardScore>();

					// add each entry to the top of the list to reverse the sort order to descending
					foreach (var score in scores)
					{
						var playerScore = new LeaderboardScore { PlayerName = score.Key, PlayerScore = (long)score.Value, SchoolName = schoolName };
						leaderboardScores.Add(playerScore);
					}

					GameEvents.OnSchoolScoresRetrieved?.Invoke(schoolName, leaderboardScores);
				}
			}
			else
			{
				Debug.Log("RetrieveSchoolScores: error = " + task.Exception.Message);
			}
		}, TaskScheduler.FromCurrentSynchronizationContext());     // task continuation on UI thread
	}



	public static string CleanName(string playerName)
	{
		// remove all firebase illegal chars
		string cleanPlayerName = playerName.Replace(".", "");
		cleanPlayerName = cleanPlayerName.Replace("$", "");
		cleanPlayerName = cleanPlayerName.Replace("#", "");
		cleanPlayerName = cleanPlayerName.Replace("[", "");
		cleanPlayerName = cleanPlayerName.Replace("]", "");
		cleanPlayerName = cleanPlayerName.Replace("/", "");

		// remove potential group separator (multiplayer)
		cleanPlayerName = cleanPlayerName.Replace("|", "");

		// char reserved for school name separator
		cleanPlayerName = cleanPlayerName.Replace(SchoolSeparator.ToString(), "");

		return cleanPlayerName;
	}
}



