﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]

public class LeaderboardEntry : MonoBehaviour
{
    public Text Rank;
    public Text SchoolName;
    public Text PlayerName;
    public Text Score;

    public Button LBButton;

    [Header("Audio")]
    public AudioClip FlipAudio;
    public AudioClip FlippedAudio;

    private float flipTime = 0.3f;

    private AudioSource audioSource;


	private void Awake()
	{
        audioSource = GetComponent<AudioSource>();
	}

	private void OnEnable()
    {
        LBButton.onClick.AddListener(OnLBEntryClick);
    }

    private void OnDisable()
    {
        LBButton.onClick.RemoveListener(OnLBEntryClick);
    }


    public void FlipIn(float initAngle, bool audio)
    {
        if (audio)
            PlaySFX(FlipAudio);

        LeanTween.rotateAround(LBButton.GetComponent<RectTransform>(), Vector3.left, 180f + initAngle, flipTime)
                    .setEaseInCubic()
                    .setOnComplete(() =>
                    {
                        LeanTween.rotateAround(LBButton.GetComponent<RectTransform>(), Vector3.left, 180f, flipTime)
                                    .setEaseOutCubic()
                                    .setOnComplete(() =>
                                    {
                                        if (audio)
                                            PlaySFX(FlippedAudio);
                                    });
                    });
    }


    private void OnLBEntryClick()
    {
        //GameEvents.OnLeaderboardEntryClicked?.Invoke();
    }

    private void PlaySFX(AudioClip sfx)
    {
        if (sfx == null)
            return;

        audioSource.clip = sfx;
        audioSource.Play();
    }
}
