﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// parent object follows the path 'PathName'
public class FollowPath : MonoBehaviour
{
    public string PathName;
    public float PathTime;

    public iTween.EaseType TweenType = iTween.EaseType.easeOutSine;

    private Vector3[] path;

    public void Start()
    {
        //path = iTweenPath.GetPath(PathName);
    }

    public void Follow()
    {
        iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath(PathName), "easetype", TweenType, "time", PathTime));
    }
}
