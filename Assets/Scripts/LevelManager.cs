﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


// handles game levels (ie. scenes)
public class LevelManager : MonoBehaviour
{
	public LevelList LevelList;
	public Text LevelsTitle;
	public Image Logo;

	public CanvasGroup LevelsCanvas;
	public Transform LevelContent;
	public GameObject LevelEntryPrefab;

	public Image Blackout;
	public Color BlackoutColour;

	private float fadeToBlackTime = 0.5f;
	private float levelsFadeTime = 1.5f;

	[Header("Options")]
	public Button OptionsButton;
	public CanvasGroup OptionsPanel;

	public Button ResetButton;                  // reset data (scores, player profile)
	public Button ConfirmResetButton;                   // reset data (scores, player profile)
	public Button QuitButton;
	public Button ConfirmQuitButton;

	public float ConfirmTimeout = 3f;

	private float optionsScaleTime = 0.75f;
	private bool optionsPanelShowing = false;

	[Header("Animation Repeat")]
	public GameSettings GameSettings;
	public bool RepeatTreeAnimation = true;			// replay on every load of IntroScene


	private void OnEnable()
	{
		GameEvents.OnSceneSelected += OnSceneSelected;
		GameEvents.OnHeartFuseEnd += OnHeartFuseEnd;

		OptionsButton.onClick.AddListener(ToggleOptionsPanel);

		ResetButton.onClick.AddListener(OnResetButton);
		QuitButton.onClick.AddListener(OnQuitButton);

		ConfirmResetButton.onClick.AddListener(ConfirmResetClicked);
		ConfirmQuitButton.onClick.AddListener(ConfirmQuitClicked);
	}

	private void OnDisable()
	{
		GameEvents.OnSceneSelected -= OnSceneSelected;
		GameEvents.OnHeartFuseEnd -= OnHeartFuseEnd;          // heart fuses

		OptionsButton.onClick.RemoveListener(ToggleOptionsPanel);

		ResetButton.onClick.RemoveListener(OnResetButton);
		QuitButton.onClick.RemoveListener(OnQuitButton);

		ConfirmResetButton.onClick.RemoveListener(ConfirmResetClicked);
		ConfirmQuitButton.onClick.RemoveListener(ConfirmQuitClicked);
	}


	private void Start()
	{
		bool animateTree = RepeatTreeAnimation || !GameSettings.LogoTreeAnimationPlayed;

		LevelsTitle.enabled = !animateTree;
		Logo.enabled = !animateTree;

		OptionsButton.enabled = !animateTree;

		if (animateTree)
		{
			GameEvents.OnFireHeartFuse?.Invoke();				// LoadLevels() OnHeartFuseEnd
			GameSettings.LogoTreeAnimationPlayed = true;
		}
		else
		{
			LoadLevels();
		}
	}

	private void LoadLevels()
	{
		LevelsCanvas.alpha = 0f;

		foreach (var level in LevelList.Levels)
		{
			var levelUI = Instantiate(LevelEntryPrefab, LevelContent);
			var levelListEntry = levelUI.GetComponent<LevelListEntry>();

			levelListEntry.SetLevel(level);
		}

		// fade in level list
		LeanTween.value(LevelsCanvas.gameObject, 0f, 1f, levelsFadeTime)
						.setEaseInSine()
						.setOnUpdate((float f) => { LevelsCanvas.alpha = f; });
	}


	private void OnSceneSelected(GameLevel level)
	{
		Blackout.color = Color.clear;
		Blackout.gameObject.SetActive(true);

		LeanTween.color(Blackout.gameObject, BlackoutColour, fadeToBlackTime)
				.setOnUpdate((Color c) => { Blackout.color = c; })
				.setEaseOutCubic()
				.setOnComplete(() =>
				{
					LoadScene(level.SceneName);
					GameEvents.OnSceneLoaded?.Invoke(level);

					LeanTween.color(Blackout.gameObject, Color.clear, fadeToBlackTime)
							.setOnUpdate((Color c) => { Blackout.color = c; })
							.setEaseInCubic()
							.setOnComplete(() =>
							{
								Blackout.gameObject.SetActive(false);
							});
				});
	}

	private void LoadScene(string sceneName)
	{
		SceneManager.LoadScene(sceneName);
	}

	// heart fuse

	private void OnHeartFuseEnd(HeartPath heart)
	{
		LoadLevels();

		LevelsTitle.enabled = true;
		Logo.enabled = true;

		OptionsButton.enabled = true;
	}

	// options

	private void ToggleOptionsPanel()
	{
		optionsPanelShowing = !optionsPanelShowing;
		GameEvents.OnGameOptions?.Invoke(optionsPanelShowing);

		ScaleOptionsPanel(optionsPanelShowing);
	}

	private void ScaleOptionsPanel(bool showing)
	{
		if (showing)
		{
			OptionsPanel.alpha = 0f;
			OptionsPanel.transform.localScale = Vector3.zero;
			OptionsPanel.gameObject.SetActive(true);

			ResetButtons();

			LeanTween.scale(OptionsPanel.gameObject, Vector3.one, optionsScaleTime)
							.setEaseOutElastic();

			LeanTween.value(OptionsPanel.gameObject, 0f, 1f, optionsScaleTime)
							.setEaseOutElastic()
							.setOnUpdate((float f) => { OptionsPanel.alpha = f; });
		}
		else
		{
			LeanTween.scale(OptionsPanel.gameObject, Vector3.zero, optionsScaleTime * 0.5f)
							.setEaseInCubic();

			LeanTween.value(OptionsPanel.gameObject, 1f, 0f, optionsScaleTime * 0.5f)
							.setEaseInCubic()
							.setOnUpdate((float f) => { OptionsPanel.alpha = f; })
							.setOnComplete(() =>
							{
								OptionsPanel.gameObject.SetActive(false);
							});
		}
	}

	private void ResetButtons()
	{
		ResetButton.gameObject.SetActive(true);
		ResetButton.interactable = true;
		ConfirmResetButton.gameObject.SetActive(false);

		QuitButton.gameObject.SetActive(true);
		QuitButton.interactable = true;
		ConfirmQuitButton.gameObject.SetActive(false);
	}

	private void OnResetButton()
	{
		StartCoroutine(ConfirmResetTimer());
	}

	private void ConfirmResetClicked()
	{
		ToggleOptionsPanel();       // hide options

		foreach (var level in LevelList.Levels)
		{
			level.LevelScores.ResetData();
		}

		GameEvents.OnResetData?.Invoke();		// player profile etc

		ResetButton.gameObject.SetActive(true);
		ConfirmResetButton.gameObject.SetActive(false);

		QuitButton.interactable = true;
	}

	private IEnumerator ConfirmResetTimer()
	{
		ResetButton.gameObject.SetActive(false);
		ConfirmResetButton.gameObject.SetActive(true);

		QuitButton.interactable = false;

		yield return new WaitForSeconds(ConfirmTimeout);

		ResetButton.gameObject.SetActive(true);
		ConfirmResetButton.gameObject.SetActive(false);

		QuitButton.interactable = true;

		yield return null;
	}

	private void OnQuitButton()
	{
		StartCoroutine(ConfirmQuitTimer());
	}

	private void ConfirmQuitClicked()
	{
		Application.Quit();
	}

	private IEnumerator ConfirmQuitTimer()
	{
		QuitButton.gameObject.SetActive(false);
		ConfirmQuitButton.gameObject.SetActive(true);

		ResetButton.interactable = false;

		yield return new WaitForSeconds(ConfirmTimeout);

		QuitButton.gameObject.SetActive(true);
		ConfirmQuitButton.gameObject.SetActive(false);

		ResetButton.interactable = true;

		yield return null;
	}
}
