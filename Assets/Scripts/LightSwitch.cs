﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

[RequireComponent(typeof(AudioSource))]

public class LightSwitch : MonoBehaviour
{
    public Light2D SwitchLight;

    public float TurnOnTime = 0.5f;
    public float TurnOffTime = 0.25f;

	public AudioClip TurnOnAudio;
	public AudioClip TurnOffAudio;

	private float lightIntensity;

	private AudioSource audioSource;


	private void Start()
	{
		audioSource = GetComponent<AudioSource>();

        lightIntensity = SwitchLight.intensity;
        SwitchLight.enabled = false;
    }


	public void TurnOn()
    {
		if (SwitchLight == null)
			return;

        SwitchLight.intensity = 0f;
        SwitchLight.enabled = true;

		LeanTween.value(SwitchLight.gameObject, 0f, lightIntensity, TurnOnTime)
					.setEaseInQuad()
					.setOnUpdate((float f) =>
					{
						SwitchLight.intensity = f;
					})
					.setOnComplete(() =>
					{
						if (TurnOnAudio != null)
						{
							audioSource.clip = TurnOnAudio;
							audioSource.Play();
						}
					});
	}

	public void TurnOff()
	{
		if (SwitchLight == null)
			return;

		SwitchLight.enabled = true;

		if (TurnOffAudio != null)
		{
			audioSource.clip = TurnOffAudio;
			audioSource.Play();
		}

		LeanTween.value(SwitchLight.gameObject, SwitchLight.intensity, 0f, TurnOffTime)
					.setEaseOutQuad()
					.setOnUpdate((float f) =>
					{
						SwitchLight.intensity = f;
					})
					.setOnComplete(() =>
					{
						SwitchLight.enabled = false;
					});
	}

	private void OnDrawGizmos()
	{
		if (SwitchLight == null)
			return;

		Gizmos.color = Color.blue;
		Gizmos.DrawLine(transform.position, SwitchLight.transform.position);
		Gizmos.DrawSphere(transform.position, 0.5f);
		Gizmos.DrawSphere(SwitchLight.transform.position, 1.5f);
	}
}
