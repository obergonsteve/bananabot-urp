﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandingCollider : MonoBehaviour
{
	private StickFigure parentFigure;

	private void Start()
	{
		parentFigure = GetComponentInParent<StickFigure>();
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		parentFigure.Land(collision);
	}
}

