﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class GameScores
{
    public int PlayCount = 0;           // total number of times game started

    public DateTime PlayStartTime;      // play start timestamp 

    [Header("Current game scores")]
    public int GamePlayTime;           // play start -> game over (seconds)
    public float GameHealthGained;
    public float GameFuelGained;
    public float GameTimeGained;

    public float GameBonusScore;

    private int GameMoonBossCount = 0;       // number of times figure has touched moon fruit
    private float MoonCountPercent = 10f;			// % score increased each time moon touched

    private float MoonCountFactor => 1 + (GameMoonBossCount * MoonCountPercent / 100f);

    [Header("Personal best scores")]
	public int PBPlayTime;
	public float PBHealthGained;
    public float PBFuelGained;
    public float PBTimeGained;

    public float PBBonusScore;

    private int PBMoonBossCount = 0;       // number of times figure has touched moon fruit

    public long PersonalBest;            // total score (calculated below)

    public const float ScoreFactor = 10f;			// multiply score (for effect)

    // reset on starting new game
    public void GameReset()
    {
        GamePlayTime = 0;
        GameHealthGained = 0;
        GameFuelGained = 0;
        GameTimeGained = 0;
        GameBonusScore = 0;

        GameMoonBossCount = 0;

        PlayStartTime = DateTime.Now;
    }

	//public long RunningScore => (long)((GameHealthGained + (GameFuelGained > 0 ? GameFuelGained : 1f) + (GameTimeGained > 0 ? GameTimeGained : 1f))); // + (GamePlayTime * 10));
	public long RunningScore => (long)((GameHealthGained + GameFuelGained + GameTimeGained + GameBonusScore) * ScoreFactor * MoonCountFactor); 
	//public long RunningScore => (long)(GameHealthGained * ScoreFactor); 

    public string AnalyticsData => string.Format("Time: {0} {1}\nScore: {2}\nPlayTime: {3}\nGameHealthGained: {4}\nGameFuelGained: {5}\nGameTimeGained: {6}\nPersonalBest: {}", DateTime.UtcNow.ToShortDateString(), DateTime.UtcNow.ToShortTimeString(), RunningScore, GamePlayTime, GameHealthGained, GameFuelGained, GameTimeGained, PersonalBest);
	//public string AnalyticsData => string.Format("Time: {0} {1}\nScore: {2}\nPlayTime: {3}\nLivesSaved: {4}\nPersonalBest: {5}", DateTime.UtcNow.ToShortDateString(), DateTime.UtcNow.ToShortTimeString(), RunningScore, GamePlayTime, GameLivesSaved, PersonalBest);


	public void OnEnable()
	{
		GameEvents.OnMoonBossReset += OnMoonBossReset;
	}

	public void OnDisable()
	{
		GameEvents.OnMoonBossReset -= OnMoonBossReset;
	}


	public void SetGamePlayTime(TimeSpan pausedTime)
    {
        //Debug.Log("SetGamePlayTime: pausedTime = " + pausedTime.TotalSeconds + " PlayStartTime" + PlayStartTime.ToLongTimeString());
        TimeSpan span = DateTime.Now.Subtract(PlayStartTime).Subtract(pausedTime);
        GamePlayTime = (int) span.TotalSeconds;
    }


    // update PB scores on all lives lost
    // return true if new PB
    public bool UpdatePersonalBest()
    {
        if (GameHealthGained > PBHealthGained)
        {
            PBHealthGained = GameHealthGained;
        }

        if (GameFuelGained > PBFuelGained)
        {
            PBFuelGained = GameFuelGained;
        }

        if (GameTimeGained > PBTimeGained)
        {
            PBTimeGained = GameTimeGained;
        }

        if (GameBonusScore > PBBonusScore)
        {
            PBBonusScore = GameBonusScore;
        }

        if (GameMoonBossCount > PBMoonBossCount)
        {
            PBMoonBossCount = GameMoonBossCount;
        }

        if (GamePlayTime > PBPlayTime)
        {
            PBPlayTime = GamePlayTime;
        }

        if (RunningScore > PersonalBest)
        {
            PersonalBest = RunningScore;
            return true;
        }

        return false;
    }


    private void OnMoonBossReset()
    {
        GameMoonBossCount++;
    }

    public void ResetData()
    {
        GameReset();

        PBPlayTime = 0;
        PBHealthGained = 0;
        PBFuelGained = 0;
        PBTimeGained = 0;
        PBBonusScore = 0;

        PersonalBest = 0;

        PlayCount = 0;
        PlayStartTime = DateTime.MinValue;
    }
}