﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(FollowPath))]

public class Fuse : MonoBehaviour
{
	public Light2D GlowLight;			// optional
	public AudioClip FuseAudio;

	public bool FollowedByCamera = true;

	private ParticleSystem FuseParticles;
	private FollowPath FusePath;
	private Vector3 fuseStartPosition;

	private AudioSource audioSource;			// looping

	public float FuseTime => FusePath.PathTime;


	private void Awake()
	{
		FuseParticles = GetComponentInParent<ParticleSystem>();
		FusePath = GetComponent<FollowPath>();
		audioSource = GetComponent<AudioSource>();

		fuseStartPosition = transform.position;

		Glow(false);
	}

	public void StartFuse()
	{
		GameEvents.OnFuseStart?.Invoke(FuseParticles, FollowedByCamera);

		FuseParticles.Play();
		Glow(true);
		FusePath.Follow();

		if (FuseAudio != null)
		{
			audioSource.clip = FuseAudio;
			audioSource.Play();
		}
	}

	public void StopFuse()
	{
		FuseParticles.Stop();
		audioSource.Stop();
		Glow(false);

		GameEvents.OnFuseEnd?.Invoke(FuseParticles, FollowedByCamera);
	}

	public void Reset()
	{
		transform.position = fuseStartPosition;
		Glow(false);
	}

	private void Glow(bool on)
	{
		if (GlowLight != null)
			GlowLight.enabled = on;
	}
}
