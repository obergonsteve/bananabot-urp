﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// A fruit spawner (SpawnPoint) cannot be physically attached to a parent (eg. cloud) because of spawned fruit scaling issues.
// This class, as a child of the spawning 'parent' (eg. cloud), allows positioning of 'attached' spawners.
// A SpawnPoint can then be connected to this, in which case its position tracks this, without becoming a physical child of the eg. cloud

public class AttachedSpawner : MonoBehaviour
{
	private void OnDrawGizmos()
	{
		Gizmos.color = Color.yellow;
		Gizmos.DrawSphere(transform.position, 1f);

		if (transform.parent != null)
			Gizmos.DrawLine(transform.position, transform.parent.position);
	}
}
