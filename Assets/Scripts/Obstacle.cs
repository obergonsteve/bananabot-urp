﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Obstacle : MonoBehaviour
{
	public bool IsBreakable;
	public bool BreakByBombOnly;

	public float Strength;
	public float Damage;

	public Color BreakableColour = Color.yellow;        // initial
	public Color BrokenColour = Color.red;              // destroyed
	public Gradient DamageColourGradient;

	protected Vector3 startScale;
	protected Quaternion startRotation;
	protected Color startColour;

	public ParticleSystem Explosion;        // when destroyed
	public Color ExplosionColour = Color.red;
	//public string ExplodeAnimation = "Explode";        // when destroyed (breakable only)
	public float ExplodeTime = 2f;        // when destroyed (breakable only) - eg. tree felled
	public float FadeTime = 10f;        // when destroyed (breakable only) - eg. tree fades

	protected float destroyDelay = 15f;     // to allow particles / animations, etc. to play out, etc

	public AudioClip BreakAudio;            // at start of DestroyObstacle()
	public AudioClip BrokenAudio;           // at end of DestroyObstacle()

	protected SpriteRenderer obstacleSprite;
	protected Collider2D obstacleCollider;

	protected AudioSource audioSource;
	//private Animator explodeAnimator;

	private LightSwitch lightSwitch;        // optional


	public float DamageStrength => Strength - Damage;
	public float DamagePercentage => Strength > 0 ? Damage / Strength : 0f;


	protected void Start()
	{
		obstacleSprite = GetComponentInChildren<SpriteRenderer>();
		obstacleCollider = GetComponent<Collider2D>();
		audioSource = GetComponent<AudioSource>();

		lightSwitch = GetComponent<LightSwitch>();      // optional!
														//explodeAnimator = GetComponent<Animator>();

		GradientColorKey[] colourKeys = new GradientColorKey[2];
		colourKeys[0].color = BreakableColour;
		colourKeys[0].time = 0f;
		colourKeys[1].color = BrokenColour;
		colourKeys[1].time = 1f;

		GradientAlphaKey[] alphaKeys = new GradientAlphaKey[2];
		alphaKeys[0].alpha = 1f;
		alphaKeys[0].time = 0f;
		alphaKeys[1].alpha = 1f;
		alphaKeys[1].time = 1f;

		DamageColourGradient.SetKeys(colourKeys, alphaKeys);

		if (IsBreakable)
		{
			obstacleSprite.color = BreakableColour;
			Damage = 0f;
		}

		startScale = transform.localScale;
		startRotation = transform.localRotation;
		startColour = obstacleSprite.color;
	}

	public void OnEnable()
	{
		GameEvents.OnMoonFruitTeleport += OnMoonFruitTeleport;
	}

	public void OnDisable()
	{
		GameEvents.OnMoonFruitTeleport -= OnMoonFruitTeleport;
	}

	public void TakeDamage(float damage)
	{
		//Debug.Log($"{name} TakeDamage {damage}");

		if (!IsBreakable || Strength <= 0f)
			return;

		Damage += damage;

		if (Damage > Strength)
			Damage = Strength;

		obstacleSprite.color = DamageColourGradient.Evaluate(DamagePercentage);

		// obstacle destroyed
		if (DamageStrength <= 0)
		{
			DestroyObstacle();        // virtual method
		}
	}


	// different types of obstacle will be 'destroyed' differently (eg. felled tree)
	// play particles and audio
	protected virtual void DestroyObstacle()
	{
		Explode();

		obstacleCollider.enabled = false;
		obstacleSprite.enabled = false;

		StartCoroutine(Deactivate(destroyDelay));      // to allow explosions, animations, etc to play out

		// optional lightswitch on 'destruction'
		if (lightSwitch != null)
			lightSwitch.TurnOn();
	}


	// particles / audio
	protected void Explode()
	{
		if (Explosion != null)      // particles
			Explosion.Play();

		if (audioSource != null && BreakAudio != null)
		{
			audioSource.clip = BreakAudio;
			audioSource.Play();
		}
	}

	//// animator event
	//public void ExplodeEnd()
	//{

	//}

	protected IEnumerator Deactivate(float delay)
	{
		yield return new WaitForSeconds(delay);
		gameObject.SetActive(false);
	}

	private void Reset()
	{
		transform.localScale = startScale;
		transform.localRotation = startRotation;
		obstacleSprite.color = startColour;

		Damage = 0f;

		obstacleSprite.enabled = true;
		obstacleCollider.enabled = true;
		gameObject.SetActive(true);

		if (lightSwitch != null)
			lightSwitch.TurnOff();
	}

	private void OnMoonFruitTeleport()
	{
		Reset();
	}

	private void OnDrawGizmos()
	{
		if (IsBreakable)
		{
			Gizmos.color = Color.red;

			if (BreakByBombOnly)
				Gizmos.DrawWireCube(transform.position, Vector2.one * 6f);
			else
				Gizmos.DrawWireSphere(transform.position, 3f);
		}
	}
}
