﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.Analytics;

////[RequireComponent(typeof(AnalyticsEventTracker))]
//public class AnalyticsManager : MonoBehaviour
//{
//	public PlayerProfile PlayerProfile;
//	public GameScores GameScores;


//	private void OnEnable()
//	{
//		BeatEvents.OnNewGame += OnNewGame;
//		BeatEvents.OnFinalGameScore += OnFinalGameScore;
//		BeatEvents.OnQuitGame += OnQuitGame;
//	}

//	private void OnDisable()
//	{
//		BeatEvents.OnNewGame -= OnNewGame;
//		BeatEvents.OnFinalGameScore -= OnFinalGameScore;
//		BeatEvents.OnQuitGame -= OnQuitGame;
//	}


//	private void OnNewGame(GameScores scores)
//	{
//		AnalyticsEvent.Custom("NewGame", new Dictionary<string, object>
//				{
//					{ PlayerProfile.PlayerSchool, string.Format("Time: {0} {1}\nPlay count: {2}", DateTime.UtcNow.ToShortDateString(), DateTime.UtcNow.ToShortTimeString(), scores.PlayCount) }
//				});
//	}

//	private void OnFinalGameScore(GameScores scores, bool newPB, GameManager.GameOverState state)
//	{
//		AnalyticsEvent.Custom("GameOver", new Dictionary<string, object>
//				{
//					{ PlayerProfile.PlayerSchool, scores.AnalyticsData }
//				});
//	}

//	private void OnQuitGame()
//	{
//		AnalyticsEvent.Custom("QuitGame", new Dictionary<string, object>
//				{
//					{ PlayerProfile.PlayerSchool, GameScores.AnalyticsData }
//				});
//	}
//}