﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CardFace : MonoBehaviour
{
    public SpriteRenderer BackgroundSprite;
    public SpriteRenderer TopLeftSprite;
    public SpriteRenderer BottomRightSprite;
    public SpriteRenderer CentreSprite;
    public TextMeshPro Value;
    public TextMeshPro Countdown;
    public TextMeshPro Sequence;


    public void SetValue(int value)
    {
        Value.text = string.Format("{0}{1}", (value <= 0 ? "" : "x "), value);
    }

}
