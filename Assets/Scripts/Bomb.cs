﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    public Detonator BombDetonator;
	public Fuse BombFuse;
	public TNT BombTNT;


    // play fuse particles, which follow the path and stop after fuse time
    // called after detonator plunger animation
    public IEnumerator StartFuse()
	{
        BombFuse.StartFuse();			// follow path
		yield return new WaitForSeconds(BombFuse.FuseTime);
        BombFuse.StopFuse();

        BombTNT.Explode();      // animation, particles, shockwave, audio
	}

    public void OnEnable()
    {
        GameEvents.OnMoonFruitTeleport += OnMoonFruitTeleport;
    }

    public void OnDisable()
    {
        GameEvents.OnMoonFruitTeleport -= OnMoonFruitTeleport;
    }

    public void Reset()
	{
        BombDetonator.Reset();
        BombTNT.Reset();
        BombFuse.Reset();
    }

    private void OnMoonFruitTeleport()
    {
        Reset();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;

        if (BombDetonator != null && BombTNT != null)
        {
            Gizmos.DrawLine(BombDetonator.transform.position, BombTNT.transform.position);
        }

        if (BombDetonator != null)
        {
            Gizmos.DrawWireCube(BombDetonator.transform.position, Vector2.one * 5f);
        }

        if (BombTNT != null)
        {
            Gizmos.DrawWireSphere(BombTNT.transform.position, BombTNT.ShockWaveRadius);
        }
    }
}
