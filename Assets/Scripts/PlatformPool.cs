﻿//using System;
//using System.Linq;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEditor;

//public class PlatformPool : MonoBehaviour
//{
//    public List<Platform> Pool;
//    public List<Platform> PlatformPrefabs;

//    //public List<Transform> InitialPlatformPositions;           // on ready to start & new game
//    //public Platform InitialPlatformPrefab;       // on ready to start & new game

//    //public float PlatformPoolDelay;           // eg. to allow animation to play out?

//    // overlap rectangle for spacing platforms
//    public LayerMask PlatformOverlapLayer;          // overlap test to ensure spacing between platforms

//    private float PlatformSpacingWidth = 60f;      // width of min spacing rectangle
//    private float PlatformSpacingHeight = 40f;     // height of min spacing rectangle



//    public void OnEnable()
//    {
//        BeatEvents.OnNewGame += OnNewGame;
//        BeatEvents.OnPlatformRemoved += OnPlatformRemoved;
//    }

//    public void OnDisable()
//    {
//        BeatEvents.OnNewGame -= OnNewGame;
//        BeatEvents.OnPlatformRemoved -= OnPlatformRemoved;
//    }

//	private void OnPlatformRemoved(Platform platform)
//	{
//        PoolPlatform(platform);
//	}

//	public void PoolPlatform(Platform platform)
//    {
//        platform.transform.parent = transform;
//        platform.gameObject.SetActive(false);      // ie pooled

//        platform.Reset();
//        //StartCoroutine(DeactivatePlatform(platform));
//    }

//    //private IEnumerator DeactivatePlatform(Platform platform)
//    //{
//    //    yield return new WaitForSeconds(PlatformPoolDelay);

//    //    platform.gameObject.SetActive(false);      // ie pooled

//    //    yield return null;
//    //}

//    // returns a platform prefab within the given altitude range
//    // return null if no available platform prefabs (shouldn't happen!)
//    public Platform GetPlatform(float altitude)
//    {
//        //Debug.Log($"GetPlatform altitude = {altitude}");
//        Platform foundPlatform = null;
//        float altPercent = altitude / StickFigure.MaxAltitude * 100f;

//        // first look for a platform in the pool
//        var poolPlatforms = Pool.Where(x => !x.gameObject.activeSelf && x.MinAltitide <= altPercent && x.MaxAltitide >= altPercent).ToList();

//        // pick a random platform from the pool list
//        if (poolPlatforms != null && poolPlatforms.Count > 0)  
//            foundPlatform = poolPlatforms[UnityEngine.Random.Range(0, poolPlatforms.Count)];

//        if (foundPlatform == null)      // nothing in the pool at the moment
//        {
//            var altPrefabs = PlatformPrefabs.Where(x => x.MinAltitide <= altPercent && x.MaxAltitide >= altPercent).ToList();

//            if (altPrefabs == null || altPrefabs.Count == 0)      // no platform prefabs in altitude range!
//                return null;

//            // pick a random prefab from the list
//            var platformPrefab = altPrefabs[UnityEngine.Random.Range(0, altPrefabs.Count)];
//            if (platformPrefab != null)
//            {
//                foundPlatform = Instantiate(platformPrefab, transform);
//                Pool.Add(foundPlatform);
//            }
//        }

//        if (foundPlatform != null)
//        {
//            foundPlatform.Reset();
//            foundPlatform.gameObject.SetActive(true);
//        }
//        return foundPlatform;
//    }

//    // returns true if the position is clear from other platforms - ie. ok for a new platform
//    public bool TestPlatformPosition(Vector2 position)
//    {
//        Vector2 topLeft = new Vector2(position.x - (PlatformSpacingWidth / 2f), position.y + (PlatformSpacingHeight / 2f));
//        Vector2 bottomRight = new Vector2(position.x + (PlatformSpacingWidth / 2f), position.y - (PlatformSpacingHeight / 2f));

//		Collider2D platformOverlap = Physics2D.OverlapArea(topLeft, bottomRight, PlatformOverlapLayer);
//		//Collider2D platformOverlap = Physics2D.OverlapBox(position, new Vector3(PlatformSpacingWidth, PlatformSpacingHeight), 0f, PlatformOverlapLayer);
//		//Collider[] platformOverlaps = Physics.OverlapBox(position, new Vector3(PlatformSpacingWidth / 2f, PlatformSpacingHeight / 2f, 20f), Quaternion.identity, PlatformOverlapLayer, QueryTriggerInteraction.Collide);
//		//bool platformOverlap = Physics.BoxCast(position, new Vector3(PlatformSpacingWidth / 2f, PlatformSpacingHeight / 2f, -100f), Vector3.forward, Quaternion.identity, 200f, PlatformOverlapLayer, QueryTriggerInteraction.Collide);

//		Vector2 topRight = new Vector2(position.x + (PlatformSpacingWidth / 2f), position.y + (PlatformSpacingHeight / 2f));
//		Vector2 bottomLeft = new Vector2(position.x - (PlatformSpacingWidth / 2f), position.y - (PlatformSpacingHeight / 2f));

//        // debug rectangles to show overlap test
//		float duration = 3f;

//		Debug.DrawLine(topLeft, topRight, platformOverlap != null ? Color.red : Color.green, duration);
//		Debug.DrawLine(topRight, bottomRight, platformOverlap != null ? Color.red : Color.green, duration);
//		Debug.DrawLine(bottomRight, bottomLeft, platformOverlap != null ? Color.red : Color.green, duration);
//		Debug.DrawLine(bottomLeft, topLeft, platformOverlap != null ? Color.red : Color.green, duration);

//		//if (platformOverlap != null)
//		//	Debug.Log($"TestPlatformPosition OVERLAP: platform {platformOverlap.name} at position: {platformOverlap.transform.position} randomPosition: {position} area: {topLeft} / {bottomRight}");
//  //      else
//  //          Debug.Log($"TestPlatformPosition OK: randomPosition: {position} area: {topLeft} / {bottomRight}");

//        return platformOverlap == null;
//    }


//    private void OnNewGame(int startLives, int maxLives, GameScores scores)
//    {
//        // disable (pool) all platforms
//        foreach (var platform in Pool)
//        {
//            if (platform.gameObject.activeSelf)
//            {
//                PoolPlatform(platform);
//            }
//        }

//        //CreateInitialPlatforms();
//	}


//    //private void CreateInitialPlatforms()
//    //{
//        //***Temp Remove code for debugging***
//        // don't create if already there!
//        //foreach (var platformPosition in InitialPlatformPositions)
//        //{
//        //    if (TestPlatformPosition(platformPosition.transform.position))
//        //    {
//        //        var initialPlatform = Instantiate(InitialPlatformPrefab, transform);
//        //        initialPlatform.transform.position = platformPosition.transform.position;
//        //        Pool.Add(initialPlatform);
//        //    }
//        //}
//    //}
//}
