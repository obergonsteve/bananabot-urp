﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// script for 'scanner' trigger collider to detect 'intruder' ProximityTrigger (eg. stick figure) proximity (for UI meter, audio, etc)

public class ProximityTrigger : MonoBehaviour
{
	[Header("Radius of object attached to")]
	public float ObjectRadius;

	[Header("Scanning Trigger Settings")]
	public bool IsScanner;      // if true (requires ScannerTrigger), proximity meter shown.  false signifies an 'intruder' (eg. stick figure)
	public CircleCollider2D ScannerTrigger;        // required if is scanner. set in inspector (for gizmo)

	public bool AudioPitchWithBlocks = true;		// only change audio pitch when blocks change (otherwise constant pitch variation)
	//public Gradient ProximityColourGradient;		// TODO: not currently used (ProximityMeter superceded by ProximityGauge)

	private AudioSource ProximityAudio;				// optional - for scanners only
	private float startingPitch;

	private float TriggerRadius => ScannerTrigger != null ? ScannerTrigger.radius : 0f;
	private float MaxDistance => TriggerRadius - ObjectRadius;      // distance from edge of object to outer edge of trigger (for meter)


	private void Awake()
	{
		ProximityAudio = GetComponent<AudioSource>();       // used if 'scanner'

		if (ProximityAudio != null)
			startingPitch = ProximityAudio.pitch;
	}

	private void OnEnable()
	{
		GameEvents.OnGameStateChanged += OnGameStateChanged;

		if (AudioPitchWithBlocks)
			GameEvents.OnProximityBlocksChanged += OnProximityBlocksChanged;
	}

	private void OnDisable()
	{
		GameEvents.OnGameStateChanged -= OnGameStateChanged;

		if (AudioPitchWithBlocks)
			GameEvents.OnProximityBlocksChanged -= OnProximityBlocksChanged;
	}


	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (!IsScanner)             // only scanners detect intruders
			return;

		//Debug.Log($"OnTriggerEnter2D {collision.gameObject.name}");
		var intruderTrigger = collision.gameObject.GetComponent<ProximityTrigger>();

		if (intruderTrigger != null)
		{
			if (intruderTrigger.IsScanner)              // scanner only detects intruder triggers
				return;

			GameEvents.OnFigureProximityEnter?.Invoke(this, intruderTrigger);

			if (ProximityAudio != null)
			{
				startingPitch = ProximityAudio.pitch;
				ProximityAudio.Play();
			}

			//Debug.Log($"OnTriggerEnter2D {intruderTrigger.gameObject.name}");
		}
	}

	private void OnTriggerStay2D(Collider2D collision)
	{
		if (! IsScanner || ScannerTrigger == null)             // only scanners detect intruders
			return;

		var intruderTrigger = collision.gameObject.GetComponent<ProximityTrigger>();

		if (intruderTrigger != null)
		{
			if (intruderTrigger.IsScanner)				// scanner only detects intruder triggers
				return;

			// distance between two centres
			float objectDistance = Vector3.Distance(transform.position, intruderTrigger.transform.position);

			// distance between two object edges
			float targetDistance = objectDistance - ObjectRadius - intruderTrigger.ObjectRadius;
			targetDistance = Mathf.Clamp(targetDistance, 0f, MaxDistance);

			// distance in relation to MaxDistance (0 = at MaxDistance, 1 = 'touching' scanner)
			float distanceRatio = 1f - (targetDistance / MaxDistance);
			distanceRatio = Mathf.Clamp01(distanceRatio);

			//var colour = ProximityColourGradient.Evaluate(distanceRatio);

			GameEvents.OnFigureProximityStay?.Invoke(this, intruderTrigger, distanceRatio); //, colour);

			// audio triggered by OnFigureProximityAudio event (only changes pitch as gauge blocks change)

			if (ProximityAudio != null && ! AudioPitchWithBlocks)		// constant pitch variation
			{
				// higher pitch as distanceRatio moves from 0 towards 1
				ProximityAudio.pitch = startingPitch * (1 + distanceRatio);
			}

			//Debug.Log($"OnTriggerStay2D distanceRatio = {distanceRatio} pitch = {ProximityAudio.pitch}");
		}
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		if (!IsScanner)             // only scanners detect intruders
			return;

		var intruderTrigger = collision.gameObject.GetComponent<ProximityTrigger>();
		if (intruderTrigger != null)
		{
			if (intruderTrigger.IsScanner)              // scanner only detects intruder triggers
				return;

			GameEvents.OnFigureProximityLeave?.Invoke(this, intruderTrigger);

			if (ProximityAudio != null)
			{
				ProximityAudio.pitch = startingPitch;
				ProximityAudio.Stop();
			}

			//Debug.Log($"OnTriggerExit2D {intruderTrigger.gameObject.name}");
		}
	}

	// audio triggered by OnProximityBlocksChanged event (only changes pitch as gauge blocks change)
	private void OnProximityBlocksChanged(float distanceRatio, int filledBlocks)
	{
		if (! AudioPitchWithBlocks)
			return;

		// higher pitch as distanceRatio moves from 0 towards 1
		if (ProximityAudio != null)
		{
			ProximityAudio.pitch = startingPitch * (1 + distanceRatio);
			//Debug.Log($"OnProximityBlocksChanged distanceRatio = {distanceRatio} pitch = {ProximityAudio.pitch} startingPitch = {startingPitch}");
		}
	}

	private void OnGameStateChanged(GameManager.GameState currentState, GameManager.GameState newState)
	{
		if (newState == GameManager.GameState.GameOver)
		{
			if (ProximityAudio != null)
			{
				ProximityAudio.pitch = startingPitch;
				ProximityAudio.Stop();
			}
		}
	}

	private void OnDrawGizmos()
	{
		if (ScannerTrigger != null)
		{
			//Gizmos.color = new Color(1, 0, 0, 0.25f);           // transparent red
			Gizmos.color = Color.white;
			Gizmos.DrawWireSphere(transform.position, ScannerTrigger.radius);
		}

		Gizmos.color = new Color(0, 0, 1, 0.5f);			// transparent blue
		Gizmos.DrawSphere(transform.position, ObjectRadius);
	}
}
