﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour
{
	public AudioClip LandAudio;

    private void OnDrawGizmos()
    {
        var colliderBoxSize = GetComponent<BoxCollider2D>().size;
        var colliderBoxOffset = GetComponent<BoxCollider2D>().offset;

        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(transform.position, new Vector3((colliderBoxSize.x  * transform.localScale.x) + colliderBoxOffset.x,
                                        (colliderBoxSize.y * transform.localScale.y) + colliderBoxOffset.y, 1));
    }

}
