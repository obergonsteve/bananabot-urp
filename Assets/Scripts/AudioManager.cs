﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


// Class to handle music and some SFX
// SFXAudioSource can play one clip at a time, so AudioManager only
// handles events/clips that are related to the player - ie. cannot happen sumultaneously
//
// SFX that can occur simultaneously (eg. balloons popping) are handled by the respective behaviours
public class AudioManager : MonoBehaviour
{
	public AudioMixer audioMixer;

	[Header("Audio Sources")]
	public AudioSource IntroMusic;
	public AudioSource GameMusic;
	public AudioSource SFXAudioSource;
	public AudioSource WindSource;

	[Header("Audio Clips")]
	public AudioClip FlipAudio;
	public AudioClip SwipeStartAudio;
	public AudioClip SwipeEndAudio;
	//public AudioClip InvalidSwipeAudio;		// 'spamming'

	public AudioClip HitGroundAudio;
	public AudioClip HitPlatformAudio;
	public AudioClip HitObstacleAudio;
	public AudioClip LandAudio;

	public AudioClip StartPlayAudio;
	public AudioClip GameOverAudio;
	public AudioClip RestartGameAudio;
	public AudioClip TimeRemainingAudio;

	//public AudioClip PlatformFlashAudio;
	//public AudioClip PlatformCrumbleAudio;
	public AudioClip FeedbackAudio;
	//public AudioClip BonusScoreAudio;
	public AudioClip PersonalBestAudio;

	public AudioClip ShowOptionsAudio;
	public AudioClip HideOptionsAudio;


	public void OnEnable()
	{
		GameEvents.OnTapToPlay += OnNewGame;
		GameEvents.OnStartPlay += OnStartPlay;
		GameEvents.OnNewGameStarted += OnRestartGame;
		GameEvents.OnGameStateChanged += OnGameStateChanged;

		GameEvents.OnSwipeStart += OnSwipeStart;
		GameEvents.OnSwipeEnd += OnSwipeEnd;
		GameEvents.OnFigureFlip += OnFigureFlip;
		GameEvents.OnFigureHitGround += OnFigureHitGround;
		GameEvents.OnFigureHitPlatform += OnFigureHitPlatform;
		GameEvents.OnFigureHitObstacle += OnFigureHitObstacle;
		//BeatEvents.OnPlatformFlash += OnPlatformFlash;
		//BeatEvents.OnPlatformExpired += OnPlatformExpired;
		GameEvents.OnTimeRemainingChanged += OnTimeRemainingChanged;
		GameEvents.OnBonusScoreFeedback += OnBonusScoreFeedback;
		//BeatEvents.OnBonusScoreIncrement += OnBonusScoreIncrement;
		GameEvents.OnNewPersonalBest += OnNewPersonalBest;

		GameEvents.OnGameOptions += OnGameOptions;
		GameEvents.OnMusicVolumeChanged += OnMusicVolumeChanged;
		GameEvents.OnSFXVolumeChanged += OnSFXVolumeChanged;
	}

	public void OnDisable()
	{
		GameEvents.OnTapToPlay -= OnNewGame;
		GameEvents.OnStartPlay -= OnStartPlay;
		GameEvents.OnNewGameStarted -= OnRestartGame;
		GameEvents.OnGameStateChanged -= OnGameStateChanged;
	
		GameEvents.OnSwipeStart -= OnSwipeStart;
		GameEvents.OnSwipeEnd -= OnSwipeEnd;
		GameEvents.OnFigureFlip -= OnFigureFlip;
		GameEvents.OnFigureHitGround -= OnFigureHitGround;
		GameEvents.OnFigureHitPlatform -= OnFigureHitPlatform;
		GameEvents.OnFigureHitObstacle -= OnFigureHitObstacle;
		//BeatEvents.OnPlatformFlash -= OnPlatformFlash;
		//BeatEvents.OnPlatformExpired -= OnPlatformExpired;
		GameEvents.OnTimeRemainingChanged -= OnTimeRemainingChanged;
		GameEvents.OnBonusScoreFeedback -= OnBonusScoreFeedback;
		//BeatEvents.OnBonusScoreIncrement -= OnBonusScoreIncrement;
		GameEvents.OnNewPersonalBest -= OnNewPersonalBest;

		GameEvents.OnGameOptions -= OnGameOptions;
		GameEvents.OnMusicVolumeChanged -= OnMusicVolumeChanged;
		GameEvents.OnSFXVolumeChanged -= OnSFXVolumeChanged;
	}


	private void Start()
	{
		//DrumBeat.Play();
		WindSource.Play();
	}

	private void OnNewGame(GameScores scores)
	{
		IntroMusic.Stop();
		GameMusic.Play();
		WindSource.Stop();
	}


	private void OnStartPlay()
	{
		PlaySFX(StartPlayAudio);
	}


	private void OnRestartGame(Action restart)
	{
		PlaySFX(RestartGameAudio);
	}

	private void OnGameStateChanged(GameManager.GameState currentState, GameManager.GameState newState)
	{
		if (newState == GameManager.GameState.GameOver)
		{
			PlaySFX(GameOverAudio);
			GameMusic.Stop();
		}
	}

	private void OnSwipeStart(Vector2 startPosition, int touchCount)
	{
		PlaySFX(SwipeStartAudio);
	}

	private void OnSwipeEnd(Vector2 endPosition, Vector2 direction, float speed, int touchCount, int tapCount)
	{
		PlaySFX(SwipeEndAudio);
	}

	private void OnFigureFlip(StickFigure.Facing facing)
	{
		PlaySFX(FlipAudio);
	}

	private void OnFigureHitGround(Ground ground)
	{
		PlaySFX(HitGroundAudio);
	}

	private void OnFigureHitPlatform(Platform platform)
	{
		PlaySFX(platform.LandAudio);
	}

	private void OnFigureHitObstacle(Obstacle obstacle)
	{
		PlaySFX(HitObstacleAudio);
	}

	//private void OnFigureLanded()
	//{
	//	PlaySFX(LandAudio);
	//}

	private void OnTimeRemainingChanged(float timeChange, float timeRemaining, float timeLimit, bool pulse)
	{
		PlaySFX(TimeRemainingAudio);
	}


	private void OnPlatformFlash(Platform platform)
	{
		//PlaySFX(PlatformFlashAudio);
	}

	private void OnBonusScoreFeedback(Color colour, Vector3 position, string message, int bonusScore)
	{
		PlaySFX(FeedbackAudio);
	}

	//private void OnBonusScoreIncrement(int bonusScore)
	//{
	//	PlaySFX(BonusScoreAudio);
	//}

	private void OnNewPersonalBest(GameScores scores, bool reloadScores)
	{
		PlaySFX(PersonalBestAudio);
	}


	private void OnGameOptions(bool showing)
	{
		if (showing)
			PlaySFX(ShowOptionsAudio);
		else
			PlaySFX(HideOptionsAudio);
	}


	private void PlaySFX(AudioClip clip)
	{
		if (clip != null)
		{
			SFXAudioSource.clip = clip;
			SFXAudioSource.Play();
		}
	}

	private void OnMusicVolumeChanged(float sliderVolume)
	{
		SetMusicVolume(sliderVolume);
	}


	private void OnSFXVolumeChanged(float sliderVolume)
	{
		SetSFXVolume(sliderVolume);
	}

	private void SetMusicVolume(float sliderVolume)
	{
		// Log10 to convert slider 0.0001-1 to decibels (0 to -80)
		audioMixer.SetFloat("MusicVolume", Mathf.Log10(sliderVolume) * 20); // volume);			// exposed param
	}

	private void SetSFXVolume(float sliderVolume)
	{
		audioMixer.SetFloat("SFXVolume", Mathf.Log10(sliderVolume) * 20); // volume);			// exposed param
	}
}
