﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameLevel", menuName = "GameLevel", order = 1)]
public class GameLevel : ScriptableObject
{
	public string LevelName;
	public string LevelAuthor;

	[Multiline(5)]
	public string LevelStory;

	public string SceneName;

	public Color LevelColour;
	public Color TextColour;

	public GameScores LevelScores;			// for device
}