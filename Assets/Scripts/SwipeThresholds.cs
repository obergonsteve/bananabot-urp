﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SwipeThresholds
{
    public int consecutiveSwipes;
    public int bonusMultiplier;
}
