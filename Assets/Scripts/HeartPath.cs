﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(AudioSource))]

public class HeartPath : MonoBehaviour
{
    public Fuse RightSparks;
    public Fuse LeftSparks;

    public ParticleSystem StartPulse;
    public AudioClip StartAudio;
    public ParticleSystem EndPulse;
    public AudioClip EndAudio;

    private float LevelsDelay = 2.5f;              // wait after heart / tree fuse before levels are loaded

    private AudioSource audioSource;


    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnEnable()
	{
		GameEvents.OnFireHeartFuse += OnFireHeartFuse;
	}

	private void OnDisable()
	{
		GameEvents.OnFireHeartFuse -= OnFireHeartFuse;
	}


	private void OnFireHeartFuse()
	{
		StartCoroutine(StartFuses());
	}


    private IEnumerator StartFuses()
    {
        GameEvents.OnHeartFuseStart?.Invoke(this);

        if (StartPulse != null)
            StartPulse.Play();

        if (StartAudio != null)
        {
            audioSource.clip = StartAudio;
            audioSource.Play();
        }

        RightSparks.StartFuse();
        LeftSparks.StartFuse();

        yield return new WaitForSeconds(RightSparks.FuseTime);

        RightSparks.StopFuse();
        LeftSparks.StopFuse();

        if (EndPulse != null)
            EndPulse.Play();

        if (EndAudio != null)
        {
            audioSource.clip = EndAudio;
            audioSource.Play();
        }

        yield return new WaitForSeconds(LevelsDelay);

        GameEvents.OnHeartFuseEnd?.Invoke(this);        // load / display levels
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, 3f);
    }
}

