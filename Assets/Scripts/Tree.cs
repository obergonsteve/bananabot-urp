﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Obstacle with overriden DestroyObstacle (tree felled)

public class Tree : Obstacle
{
    [Header("Tree")]
    public float FallDegrees = 90f;     // rotate (around base)
    public float StretchFactor = 2f;

    public float StretchTime = 6f;
    public float SquashTime = 3f;


    // fell tree
    protected override void DestroyObstacle()
    {
        Explode();      // particles / audio

        obstacleCollider.enabled = false;       // so falls cleanly to ground
        Shrink();

        // rotate (around base ... sprite is offset from centre)
        LeanTween.rotateAround(gameObject, Vector3.back, FallDegrees, ExplodeTime)
                .setEaseInOutBounce()
                .setOnComplete(() =>
                {
                    if (audioSource != null && BrokenAudio != null)
                    {
                        audioSource.clip = BrokenAudio;
                        audioSource.Play();
					}

                    //Shrink();
                    FadeAway();
                });
    }


    private void FadeAway()
    {
        // fade colour
        //var fadeColour = new Color(obstacleSprite.color.r, obstacleSprite.color.g, obstacleSprite.color.b, 0f);

        LeanTween.value(gameObject, 1f, 0f, FadeTime)
                .setEaseInQuad()
                //.setOnUpdate((Color c) =>
                .setOnUpdate((float f) =>
                {
                    //obstacleSprite.color = c;
                    obstacleSprite.color = new Color(obstacleSprite.color.r, obstacleSprite.color.g, obstacleSprite.color.b, f);
                })
                .setOnComplete(() =>
				{
					StartCoroutine(Deactivate(destroyDelay));      // to allow explosion to play out
				});
	}

    // stretch / squash
    private void Shrink()
    {
        // stretch Y
        LeanTween.scaleY(gameObject, startScale.y * StretchFactor, StretchTime)
                .setEaseInQuad();

        // squash X
        LeanTween.scaleX(gameObject, 0f, SquashTime)
                .setEaseInQuad();
    }

    private void OnDrawGizmos()
    {
        if (IsBreakable)
        {
            Gizmos.color = Color.red;

            if (BreakByBombOnly)
                Gizmos.DrawWireCube(transform.position, Vector2.one * 6f);
            else
                Gizmos.DrawWireSphere(transform.position, 3f);
        }
    }
}
