﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(AudioSource))]

public class Platform : MonoBehaviour
{
	public Card PlatformCard;			// optional (child) card attached to platform

	public float RestTimeLimit;         // total rest time in secs
	public float RestTimeWarning;       // eg. start flashing if RestTimeRemaining <= this

	public float RestTimeRemaining;     // total rest time remaining in secs

	public AudioClip LandAudio;

	public float FlashTime = 0.5f;
	public Color FlashColour = Color.red;
	public AudioClip FlashAudio;

	public float RotateTime = 0.5f;
	public float RotateAngle = 90f;
	private bool rotating = false;

	public float FallGravity = 1f;       // rigidbody gravity scale
	private float MaxDrag = 100f;       // to prevent from moving under impact

	private float PoolDelay = 6f;

	private bool gameInPlay = false;
	private bool figureResting = false;

	private bool flashing = false;
	private bool expired = false;       // rest time used

	private Color spriteColour;
	private Vector3 startPosition;
	private Quaternion startRotation;

	private SpriteRenderer spriteRenderer;
	private Rigidbody2D rigidBody;
	private Collider2D boxCollider;

	private AudioSource audioSource;


	public void OnEnable()
	{
		GameEvents.OnGameStateChanged += OnGameStateChanged;
		GameEvents.OnMoonFruitTeleport += OnMoonFruitTeleport;
	}

	public void OnDisable()
	{
		GameEvents.OnGameStateChanged -= OnGameStateChanged;
		GameEvents.OnMoonFruitTeleport -= OnMoonFruitTeleport;
	}


	private void Awake()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		spriteColour = spriteRenderer.color;
		rigidBody = GetComponent<Rigidbody2D>();
		boxCollider = GetComponent<Collider2D>();
		audioSource = GetComponent<AudioSource>();

		startPosition = transform.position;
		startRotation = transform.rotation;

		RestTimeRemaining = RestTimeLimit;
		gameInPlay = false;

		Reset();

		if (RotateTime > 0 && !rotating)
			Rotate();
	}

	private void Update()
	{
		if (!gameInPlay)
			return;

		if (!figureResting)
			return;

		if (expired)
			return;

		if (RestTimeLimit == 0)			// no time countdown to platform expiry
			return;

		RestTimeRemaining -= Time.deltaTime;

		if (RestTimeRemaining <= RestTimeWarning && RestTimeRemaining > 0)
		{
			if (! flashing)
				Flash();
		}
		else if (RestTimeRemaining <= 0)
		{
			expired = true;
			RestTimeRemaining = 0;

			GameEvents.OnPlatformExpired?.Invoke(this);

			//Debug.Log($"{name} platform expired!");

			// reset platform
			Reset();
			StartCoroutine(FallUnderGravity());
		}
	}

	private IEnumerator FallUnderGravity()
	{
		rigidBody.bodyType = RigidbodyType2D.Dynamic;
		rigidBody.gravityScale = FallGravity;               // fall under gravity, pool when hit water or another platform
		rigidBody.drag = 0;
		rigidBody.angularDrag = 0;
		boxCollider.enabled = false;

		yield return new WaitForSeconds(PoolDelay);

		//Debug.Log($"{name} OnPlatformRemoved");
		//GameEvents.OnPlatformRemoved?.Invoke(this);     // pools after delay

		gameObject.SetActive(false);

		yield return null;
	}

	private void Reset()
	{
		flashing = false;
		figureResting = false;
		expired = false;

		gameObject.SetActive(true);

		spriteRenderer.color = spriteColour;

		transform.position = startPosition;
		transform.rotation = startRotation;

		rigidBody.gravityScale = 0f;
		rigidBody.drag = MaxDrag;
		rigidBody.angularDrag = MaxDrag;

		boxCollider.enabled = true;

		rigidBody.bodyType = RigidbodyType2D.Kinematic;

		RestTimeRemaining = RestTimeLimit;
	}


	private void Flash()
	{
		if (flashing)
			return;

		flashing = true;

		LeanTween.color(gameObject, FlashColour, FlashTime)
					.setOnUpdate((Color c) => { spriteRenderer.color = c; })
					.setEaseOutSine()
					.setOnComplete(() =>
					{
						LeanTween.color(gameObject, spriteColour, FlashTime)
									.setOnUpdate((Color c) => { spriteRenderer.color = c; })
									.setEaseInSine()
									.setOnComplete(() =>
									{
										if (FlashAudio != null)
										{
											audioSource.clip = FlashAudio;
											audioSource.Play();
										}
										//AudioSource.PlayClipAtPoint(FlashAudio, transform.position, AudioManager.SFXVolume);
										flashing = false;
										//GameEvents.OnPlatformFlash?.Invoke(this);
									});
					});
	}

	// true while figure in collision with platform
	public void FigureResting(bool resting)
	{
		if (figureResting == resting)       // no change!
			return;

		figureResting = resting;

		if (PlatformCard != null)
		{
			if (figureResting)
				PlatformCard.ShowFront(true);		// countdown
			//else
			//	PlatformCard.ShowBack();
		}
	}

	public void Rotate()
	{
		if (rotating)
			return;

		rotating = true;
		LeanTween.rotateAround(gameObject, transform.forward, RotateAngle, RotateTime)
						.setEaseInOutQuart()
						.setLoopPingPong();

		//LeanTween.delayedCall(gameObject, 2f, () =>
		//	{
		//		LeanTween.rotateAround(gameObject, transform.forward, RotateAngle, RotateTime)
		//						.setEaseLinear()
		//						.setLoopPingPong().
		//						setRepeat(2);

		//	}
		//).setRepeat(-1);
	}

	private void OnGameStateChanged(GameManager.GameState currentState, GameManager.GameState newState)
	{
		gameInPlay = (newState == GameManager.GameState.InPlay);
	}

	private void OnMoonFruitTeleport()
	{
		Reset();
	}
}
