﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;


[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(AudioSource))]


public class TNT : MonoBehaviour
{
	[Header("Shockwave")]
	public float ShockWaveTime = 2f;			// trigger collider expands
	public float ShockWaveRadius = 6f;          // max size
	public float BombEndDelay = 2f;				// delay after shockwave completes before OnBombEnd event / bomb reset
	private float startRadius;

	[Header("Damage")]
	public float DamageInflicted = 50f;
	public bool FixedDamage = false;            // does not decrease over duration of 'shockwave'
	private float decreasingDamage;

	[Header("Audio")]
	public AudioClip ExplodeAudio;
	public ParticleSystem Explosion;

	[Header("Light")]
	public Light2D BackgroundGlow;

	private string ExplodeTrigger = "Explode";  // animation

	private Bomb ParentBomb;
	private CircleCollider2D shockWave;			// expands on explosion
	private Animator animator;					// sprite animation
	private AudioSource audioSource;


	private void Awake()
	{
		ParentBomb = GetComponentInParent<Bomb>();
		shockWave = GetComponent<CircleCollider2D>();
		animator = GetComponent<Animator>();
		audioSource = GetComponent<AudioSource>();

		startRadius = shockWave.radius;
	}

	// explosion animation, particles, shockwave, audio
	public void Explode()
	{
		animator.SetTrigger(ExplodeTrigger);        // rapidly expand then shrink sprite
		Explosion.Play();                           // fireball particles
		Glow(false);

		// optionally reduce damage inflicted over duration of 'shockwave'
		decreasingDamage = DamageInflicted;

		if (!FixedDamage)
		{
			LeanTween.value(gameObject, 0f, DamageInflicted, ShockWaveTime)
						.setOnUpdate((float f) =>
						{
							decreasingDamage = DamageInflicted - f;
						})
						.setEaseOutCubic();
		}

		// expand circle trigger collider
		LeanTween.value(gameObject, shockWave.radius, ShockWaveRadius, ShockWaveTime)
					.setOnUpdate((float f) =>
					{
						shockWave.radius = f;
					})
					.setOnComplete(() =>
					{
						StartCoroutine(DelayBombEnd());
						//GameEvents.OnBombEnd?.Invoke(ParentBomb, true);       // camera stops following fuse (back to player)
						//ParentBomb.Reset();
					})	
					.setEaseOutCubic();

		if (ExplodeAudio != null)
		{
			audioSource.clip = ExplodeAudio;
			audioSource.Play();
		}
	}

	private IEnumerator DelayBombEnd()
	{
		yield return new WaitForSeconds(BombEndDelay);

		GameEvents.OnBombEnd?.Invoke(ParentBomb, true);       // camera stops following fuse (back to player)
		ParentBomb.Reset();
	}

	public void Reset()
	{
		shockWave.radius = startRadius;
		Glow(true);
	}

	// shockwave damages breakable obstacles
	private void OnTriggerEnter2D(Collider2D collision)
	{
		var obstacle = collision.gameObject.GetComponent<Obstacle>();
		if (obstacle == null)
			return;

		if (obstacle.IsBreakable)
			obstacle.TakeDamage(decreasingDamage);
	}

	private void Glow(bool on)
	{
		if (BackgroundGlow != null)
			BackgroundGlow.enabled = on;
	}
}
