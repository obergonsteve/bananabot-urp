﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// TODO: pool, move, scale, transparency fluctuations/animations

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(AreaEffector2D))]

public class Cloud : MonoBehaviour
{
	public enum WindDirection
	{
		None,
		Left,
		Right,
		Up,
		Down
	}

	[Header("Wind Force")]
	public WindDirection windDirection;

	[Range(0, 200)]
	public float windForce = 50f;

	public AreaEffector2D areaAffector;
	public SpriteRenderer Arrow;

	[Space]
	public SpriteRenderer cloudSprite;

	[Header("Floating")]
	public Transform FloatDestination;
	public float FloatTime = 6f;
	public float FloatDelay = 2f;			// at start of pulsating only
	//public float FloatMinPause = 0f;
	//public float FloatMaxPause = 1f;
	public float FloatPause = 1;
	//private float FloatPause => Random.Range(FloatMinPause, FloatMaxPause);
	private bool floating = false;

	public float PulsateScale = 1.25f;
	public float PulsateTime = 2f;
	public float PulsateDelay = 2f;         // at start of floating only
	//public float PulsateMinPause = 2f;
	//public float PulsateMaxPause = 6f;
	public float PulsatePause = 0.5f;
	//private float PulsatePause => Random.Range(PulsateMinPause, PulsateMaxPause);
	private bool pulsating = false;

	[Header("Optional Linked Portal")]
	public Portal CloudPortal;          // optional
	public ParticleSystem PortalSparkles;         // if CloudPortal
	public bool IsPortal => CloudPortal != null;

	private Vector3 startingScale;
	private Vector3 startingPosition;
	private Vector3 floatPosition;


	// init area affector (wind force) or link to portal
	private void Start()
	{
		cloudSprite = GetComponent<SpriteRenderer>();
		areaAffector = GetComponent<AreaEffector2D>();

		startingScale = transform.localScale;
		startingPosition = transform.position;
		floatPosition = FloatDestination.position;

		if (IsPortal)		// portal can't also be a wind force - floats to destination as stick figure teleports
		{
			Arrow.gameObject.SetActive(false);
			areaAffector.enabled = false;

			// make sure the portal entry and exit are aligned with the cloud and destination point
			CloudPortal.LinkToCloud(this, startingPosition, floatPosition, FloatTime);     // link both ways

			if (PortalSparkles != null)
				PortalSparkles.Play();
		}
		else		// set up wind force and start floating to/from destination
		{
			float affectorForce = windForce;
			float directionAngle = 0f;

			areaAffector.enabled = true;

			switch (windDirection)
			{
				case WindDirection.Left:
					Arrow.gameObject.SetActive(true);
					directionAngle = 180f;
					break;

				case WindDirection.Right:
					Arrow.gameObject.SetActive(true);
					directionAngle = 0f;
					break;

				case WindDirection.Up:
					Arrow.gameObject.SetActive(true);
					directionAngle = 90f;
					break;

				case WindDirection.Down:
					Arrow.gameObject.SetActive(true);
					directionAngle = -90f;
					break;

				case WindDirection.None:
					Arrow.gameObject.SetActive(false);
					affectorForce = 0f;
					break;
			}

			areaAffector.forceAngle = directionAngle;
			Arrow.transform.Rotate(0, 0, directionAngle, Space.Self);

			areaAffector.forceMagnitude = affectorForce;
			areaAffector.forceVariation = 0f;

			if (FloatTime > 0 && !floating && FloatDestination.localPosition != Vector3.zero)
				StartCoroutine(Float());
		}

		if (PulsateTime > 0 && !pulsating)
			StartCoroutine(Pulsate());
	}


	private IEnumerator Pulsate()
	{
		if (pulsating)
			yield break;

		pulsating = true;

		yield return new WaitForSeconds(PulsateDelay);

		while (pulsating)
		{
			LeanTween.scale(gameObject, startingScale * PulsateScale, PulsateTime)
						.setEaseInOutBack()
						.setOnComplete(() =>
						{
							LeanTween.scale(gameObject, startingScale, PulsateTime)
								.setEaseInOutBack();
						});

			yield return new WaitForSeconds((PulsateTime * 2f) + PulsatePause);
		}
	}

	private IEnumerator Float()
	{
		if (floating)
			yield break;

		if (FloatDestination.localPosition == Vector3.zero)     // same as cloud position!
			yield break;

		floating = true;

		yield return new WaitForSeconds(FloatDelay);

		while (floating)
		{
			LeanTween.move(gameObject, floatPosition, FloatTime)
						.setEaseInOutSine()
						.setOnComplete(() =>
						{
							LeanTween.move(gameObject, startingPosition, FloatTime)
									.setEaseInOutSine();
						});

			yield return new WaitForSeconds((FloatTime * 2f) + FloatPause);
		}
	}

	// effectively float in sync with portal teleport
	public void FloatTo(Vector3 targetPosition)
	{
		if (CloudPortal == null)
			return;

		if (floating)
			return;

		floating = true;

		LeanTween.move(gameObject, targetPosition, FloatTime)
						.setEaseInOutSine()
						.setOnComplete(() =>
						{
							floating = false;
						});
	}


	public void SetTransparency(float trans)
	{
		cloudSprite.color = new Color(1, 1, 1, trans);
	}

	public void SetColour(Color cloudColour)
	{
		cloudSprite.color = cloudColour;
	}


	private void OnDrawGizmos()
	{
		Gizmos.color = Color.cyan;

		if (FloatDestination.localPosition != Vector3.zero)
		{
			Gizmos.DrawLine(transform.position, FloatDestination.position);
			Gizmos.DrawWireSphere(FloatDestination.position, 2f);
		}
	}
}
