﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Handle screen touches
// Mouse clicks emulate touches
// Keeps track of beat ticks for touch timing
public class TouchInput : MonoBehaviour
{
	public SwipeStarPool SwipeStarPool;
	public float SwipeForce;            // increase to apply to more force on swipe
	//private float swipeFactor = 1f;	

	private Vector2 startPosition;
	private Vector2 moveDirection;
	private Vector2 endPosition;

	private long touchStartTicks;
	private long touchEndTicks;
	private float TickSpeedFactor = 100000f;		// to reduce swipe time

	private int tapCount = 0;

	private SwipeStars swipeStars;

	private GameManager.GameState gameState = GameManager.GameState.NotStarted;

	private bool GameInactive => (gameState == GameManager.GameState.NotStarted || gameState == GameManager.GameState.ReadyToStart
				|| gameState == GameManager.GameState.Paused || gameState == GameManager.GameState.GameOver);

	private bool OptionsShowing;


	public void OnEnable()
	{
		GameEvents.OnGameStateChanged += OnGameStateChanged;
		GameEvents.OnGameOptions += OnGameOptions;
	}

	public void OnDisable()
	{
		GameEvents.OnGameStateChanged -= OnGameStateChanged;
		GameEvents.OnGameOptions -= OnGameOptions;
	}

	private void OnGameStateChanged(GameManager.GameState oldState, GameManager.GameState newState)
	{
		if (gameState == newState)
			return;

		gameState = newState;

		if (gameState == GameManager.GameState.GameOver)
			tapCount = 0;       // reset
	}


	private void OnGameOptions(bool showing)
	{
		OptionsShowing = showing;
	}


	private void Update()
	{
		if (OptionsShowing)
		{
			return;
		}

		// track a single touch
		if (Input.touchCount > 0)
		{
			Touch touch = Input.GetTouch(0);

			// handle finger movements based on TouchPhase
			switch (touch.phase)
			{
				case TouchPhase.Began:
					StartTouch(touch.position, Input.touchCount);
					break;

				case TouchPhase.Moved:
					MoveTouch(touch.position, Input.touchCount);
					break;

				case TouchPhase.Ended:
					EndTouch(touch.position, Input.touchCount);
					break;

				case TouchPhase.Stationary:         // touching but hasn't moved
					break;

				case TouchPhase.Canceled:           // system cancelled touch
					break;
			}
		}

		// emulate touch with left mouse
		else if (Input.GetMouseButtonDown(0))
		{
			StartTouch(Input.mousePosition, 1);
		}
		else if (Input.GetMouseButton(0))
		{
			MoveTouch(Input.mousePosition, 1);
		}
		else if (Input.GetMouseButtonUp(0))
		{
			EndTouch(Input.mousePosition, 1);
		}

		// right mouse == 2 finger swipe
		else if (Input.GetMouseButtonDown(1))
		{
			StartTouch(Input.mousePosition, 2);
		}
		else if (Input.GetMouseButton(1))
		{
			MoveTouch(Input.mousePosition, 2);
		}
		else if (Input.GetMouseButtonUp(1))
		{
			EndTouch(Input.mousePosition, 2);
		}
	}

	private void StartTouch(Vector2 screenPosition, int touchCount)
	{
		if (touchCount == 1)			// one finger
		{
			tapCount++;
			GameEvents.OnPlayerTap?.Invoke(gameState, tapCount);        // capture all taps for GameManager
		}

		if (GameInactive)
			return;

		startPosition = screenPosition;
		touchStartTicks = DateTime.Now.Ticks;

		if (tapCount > 1)
		{
			swipeStars = SwipeStarPool.GetSwipeStars();
			swipeStars.SwipeStart(startPosition);
		}

		GameEvents.OnSwipeStart?.Invoke(startPosition, touchCount);
	}

	private void MoveTouch(Vector2 screenPosition, int touchCount)
	{
		if (GameInactive)
			return;

		Vector2 movePosition = screenPosition;

		if (movePosition != startPosition)
		{
			GameEvents.OnSwipeMove?.Invoke(movePosition, touchCount);

			if (swipeStars != null)
				swipeStars.SwipeMove(movePosition);
		}
	}

	private void EndTouch(Vector2 screenPosition, int touchCount)
	{
		if (GameInactive)
			return;

		endPosition = screenPosition;
		touchEndTicks = DateTime.Now.Ticks;
		moveDirection = (endPosition - startPosition).normalized;

		Vector2 worldStart = Camera.main.ScreenToWorldPoint(startPosition);
		Vector2 worldEnd = Camera.main.ScreenToWorldPoint(endPosition);

		//float moveDistance = Vector2.Distance(startPosition, endPosition);
		float moveDistance = Mathf.Abs(Vector2.Distance(worldStart, worldEnd));
		float moveSpeed = 0;     // distance / time

		if (touchEndTicks - touchStartTicks > 0)            // should be!!
		{
			float swipeTime = (touchEndTicks - touchStartTicks) / TickSpeedFactor;
			moveSpeed = moveDistance / swipeTime;     // speed = distance / time
			//Debug.Log($"EndTouch direction = {moveDirection} distance = {moveDistance} time = {swipeTime} speed = {moveSpeed} tapCount = {tapCount}");
		}

		if (moveSpeed > 0)
		{
			//Debug.Log($"OnSwipeEnd: touchStartBeatAccuracy = {touchStartBeatAccuracy} touchEndBeatAccuracy = {touchEndBeatAccuracy}");
			GameEvents.OnSwipeEnd?.Invoke(endPosition, moveDirection, moveSpeed * SwipeForce, touchCount, tapCount);
		}

		if (swipeStars != null)
		{
			swipeStars.SwipeEnd(endPosition);
			SwipeStarPool.PoolSwipeStars(swipeStars);       // after delay
			swipeStars = null;
		}
	}
}
