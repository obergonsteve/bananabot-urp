﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(AudioSource))]

public class FlashManager : MonoBehaviour
{
    public List<FlashContent> FlashImages;

    public bool FlashOnMazeEject = false;

    public float MinDisplayTime = 0.1f;
    public float MaxDisplayTime = 1f;
    public float FlashChance = 0.5f;      // chance of flashing a random image 

    private AudioSource audioSource;


	private void Start()
	{
        audioSource = GetComponent<AudioSource>();
        DisableAll();
	}

	private void OnEnable()
	{
        GameEvents.OnMazeZoneEject += OnMazeZoneEject;
	}

    private void OnDisable()
    {
        GameEvents.OnMazeZoneEject -= OnMazeZoneEject;
    }

	private void OnMazeZoneEject()
	{
        if (! FlashOnMazeEject)
            return;

        if (FlashImages.Count == 0)
            return;

       if (UnityEngine.Random.Range(0f, 1f) > FlashChance)
            return;

        StartCoroutine(RandomFlashImage());
    }

    private IEnumerator RandomFlashImage()
    {
        if (FlashImages.Count == 0)
            yield break;

        // flash background image for a random time
        FlashContent randomFlash = FlashImages[UnityEngine.Random.Range(0, FlashImages.Count)];
        float randomTime = UnityEngine.Random.Range(MinDisplayTime, MaxDisplayTime);

        randomFlash.FlashImage.enabled = true;

        if (randomFlash.FlashAudio != null)
        {
            audioSource.PlayOneShot(randomFlash.FlashAudio);
        }

        yield return new WaitForSeconds(randomTime);

        randomFlash.FlashImage.enabled = false;
    }

    private void DisableAll()
    {
        foreach (var flash in FlashImages)
        {
            flash.FlashImage.enabled = false;
        }
    }
}

[Serializable]
public class FlashContent
{
    public Image FlashImage;
    public AudioClip FlashAudio;
}
