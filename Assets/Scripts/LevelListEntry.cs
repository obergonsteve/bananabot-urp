﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LevelListEntry : MonoBehaviour
{
    public Button LevelButton;
    public Text LevelName;
    public Text LevelAuthor;

	private GameLevel GameLevel;

	private void OnEnable()
	{
		LevelButton.onClick.AddListener(LevelButtonClicked);
	}

	private void OnDisable()
	{
		LevelButton.onClick.RemoveListener(LevelButtonClicked);
	}

	private void LevelButtonClicked()
	{
		GameEvents.OnSceneSelected?.Invoke(GameLevel);
	}

	public void SetLevel(GameLevel level)
	{
		GameLevel = level;
		LevelName.text = level.LevelName;
		LevelAuthor.text = level.LevelAuthor;

		LevelButton.GetComponent<Image>().color = level.LevelColour;
		LevelName.color = level.TextColour;
		LevelAuthor.color = level.TextColour;
	}
}
