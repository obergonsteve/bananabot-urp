﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    // bitwise operation to check if the gameObject is in a given LayerMask
    public static bool IsInLayerMasks(this GameObject gameObject, int layerMasks)
    {
        return (layerMasks == (layerMasks | (1 << gameObject.layer)));
    }
}
