﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]

public class BossIntruder : MonoBehaviour
{
    private Boss parentBoss;
    private AudioSource audioSource;        // looping

    private void Start()
    {
        parentBoss = GetComponentInParent<Boss>();
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (parentBoss == null)
            return;

        if (collision.gameObject.CompareTag("Player"))
        {
            var figure = collision.gameObject.GetComponent<StickFigure>();

            if (figure != null)
            {
                parentBoss.IntruderEntry(figure);
                audioSource.Play();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (parentBoss == null)
            return;

        if (collision.gameObject.CompareTag("Player"))
        {
            var figure = collision.gameObject.GetComponent<StickFigure>();

            if (figure != null)
            {
                parentBoss.IntruderExit(figure);
                audioSource.Stop();
            }
        }
    }
}
