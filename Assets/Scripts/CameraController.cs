﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

// allows camera to follow other objects (temporarily)
public class CameraController : MonoBehaviour
{
	public CinemachineVirtualCamera Camera;
	public AudioSource SFXAudioSource;

	public float ZoomOutLensSize = 40f;
	public float ZoomOutTime = 2f;

	public float ZoomInLensSize = 20f;
	public float ZoomInTime = 1f;

	public AudioClip FollowAudio;

	private Transform followingPlayer;
	private float startLensSize;


	private void OnEnable()
	{
		GameEvents.OnBombStart += DetonatorZoom;
		GameEvents.OnFuseStart += FollowFuse;
		GameEvents.OnFuseEnd += ZoomFuse;
		GameEvents.OnBombEnd += FollowPlayer;
	}


	private void OnDisable()
	{
		GameEvents.OnBombStart -= DetonatorZoom;
		GameEvents.OnFuseStart -= FollowFuse;
		GameEvents.OnFuseEnd -= ZoomFuse;
		GameEvents.OnBombEnd -= FollowPlayer;
	}


	private void Start()
	{
		followingPlayer = Camera.Follow;
		startLensSize = Camera.m_Lens.OrthographicSize;
	}


	private void FollowFuse(ParticleSystem fuse, bool cameraFollow)
	{
		if (!cameraFollow)
			return;

		// follow fuse immediately, then zoom out
		Camera.Follow = fuse.transform;
		ZoomOut(null);
	}

	private void ZoomFuse(ParticleSystem fuse, bool cameraFollow)
	{
		if (!cameraFollow)
			return;

		ZoomIn(fuse.transform);
	}

	private void FollowPlayer(Bomb bomb, bool cameraFollow)
	{
		if (!cameraFollow)
			return;

		// zoom back to default, then return to following player
		ZoomBack(followingPlayer);
		FollowWhiff();
	}

	private void DetonatorZoom(Bomb bomb, bool cameraFollow)
	{
		if (!cameraFollow)
			return;

		ZoomIn(null);
	}


	private void ZoomOut(Transform follow)
	{
		LeanTween.value(gameObject, Camera.m_Lens.OrthographicSize, ZoomOutLensSize, ZoomOutTime)
						.setOnUpdate((float f) => Camera.m_Lens.OrthographicSize = f)
						.setEaseOutSine()
						.setOnComplete(() =>
						{
							if (follow != null)
								Camera.Follow = follow;
						});
	}

	private void ZoomIn(Transform follow)
	{
		LeanTween.value(gameObject, Camera.m_Lens.OrthographicSize, ZoomInLensSize, ZoomInTime)
						.setOnUpdate((float f) => Camera.m_Lens.OrthographicSize = f)
						.setEaseOutSine()
						.setOnComplete(() =>
						{
							if (follow != null)
								Camera.Follow = follow;
						});
	}

	private void ZoomBack(Transform follow)
	{
		LeanTween.value(gameObject, Camera.m_Lens.OrthographicSize, startLensSize, ZoomOutTime)
						.setOnUpdate((float f) => Camera.m_Lens.OrthographicSize = f)
						.setEaseOutSine()
						.setOnComplete(() =>
						{
							if (follow != null)
								Camera.Follow = follow;
						});
	}

	private void FollowWhiff()
	{
		if (FollowAudio != null)
		{
			SFXAudioSource.clip = FollowAudio;
			SFXAudioSource.Play();
		}
	}
}
