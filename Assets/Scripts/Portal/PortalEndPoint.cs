﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(AudioSource))]

public class PortalEndPoint : MonoBehaviour
{
	public Portal ParentPortal;

	public Transform TeleportPoint;		// ie. entry point
	public Transform ExitPoint;			// away from colliders

	public Light2D EndPointLight;    

	public enum PortalDirection
	{
		None,
		Left,
		Right,
		Up,
		Down
	}
	public PortalDirection ExitDirection;

	[Range(0, 100)]
	public float ExitForce = 25f;			// if !ParentPort.MaintainVelocity
	private Vector2 exitAngle;
	public Vector2 ExitVector => exitAngle * ExitForce;

	public float ShrinkTime = 0.5f;			// transportable object during teleport
	public float GrowTime = 0.5f;           // transportable object during teleport

	private AudioSource audioSource;

	public CircleCollider2D Zone;	// set in inspector

	public PortalEndPoint OtherEndPoint => (this == ParentPortal.Entry) ? ParentPortal.Exit : ParentPortal.Entry;
	public float TravelTime => ParentPortal.TravelTime;

	// gizmo circle
	public Vector2 ZonePosition => new Vector2(transform.position.x + Zone.offset.x, transform.position.y + Zone.offset.y);
	public float ZoneRadius => Zone.radius;


	private void Awake()
    {
		audioSource = GetComponent<AudioSource>();

		// exitAngle used if not maintaining entry velocity
		switch (ExitDirection)
		{
			case PortalDirection.Left:
				//exitAngle = 180f;
				exitAngle = Vector2.left; 
				break;

			case PortalDirection.Right:
				//exitAngle = 0f;
				exitAngle = Vector2.right;
				break;

			case PortalDirection.Up:
				//exitAngle = 90f;
				exitAngle = Vector2.up;
				break;

			case PortalDirection.Down:
				//exitAngle = -90f;
				exitAngle = Vector2.down;
				break;

			case PortalDirection.None:
				//exitAngle = 0f;
				exitAngle = Vector2.zero;
				break;
		}

		EndPointLight.color = ParentPortal.LightColour;
	}

	private void OnTriggerEnter2D(Collider2D collider)
	{
		if (ParentPortal == null)
			return;

		if (ParentPortal.OneWay && this == ParentPortal.Exit)
			return;

		var transportable = collider.gameObject.GetComponent<Transportable>();
		if (transportable == null)
			return;

		if (ParentPortal.LinkedToCloud != null)
		{
			// portals linked to clouds can only teleport stick figure! (ie. not fruit or other transportables)
			if (transportable.GetComponent<StickFigure>() == null)
				return;

			// can only enter a cloud portal if the cloud is at this end point
			if (transform.position != ParentPortal.LinkedToCloud.transform.position)
				return;
		}

		// if teleporting out of a maze zone, deactivate and exit that zone
		var mazeZoneVisitor = collider.gameObject.GetComponent<MazeZoneVisitor>();
		if (mazeZoneVisitor != null && mazeZoneVisitor.InMazeZone != null)
		{
			mazeZoneVisitor.InMazeZone.DeactivateZone(mazeZoneVisitor);		// visitor also exits zone
		}

		transportable.Teleport(this);

		// move cloud in sync with the teleport (to 'ride the cloud')
		if (ParentPortal.LinkedToCloud != null)
			ParentPortal.LinkedToCloud.FloatTo(OtherEndPoint.transform.position);
	}

	public void EntryAudio()
	{
		if (ParentPortal.EntryAudio != null)
		{
			audioSource.clip = ParentPortal.EntryAudio;
			audioSource.Play();
		}
	}

	public void TravelAudio()
	{
		if (ParentPortal.TravelAudio != null)
		{
			audioSource.clip = ParentPortal.TravelAudio;
			audioSource.Play();
		}
	}

	public void ExitAudio()
	{
		if (ParentPortal.ExitAudio != null)
		{
			audioSource.clip = ParentPortal.ExitAudio;
			audioSource.Play();
		}
	}


	public void EnableTriggerZone(bool enable)
	{
		Zone.enabled = enable;
	}
}
