﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    public PortalEndPoint Entry;
    public PortalEndPoint Exit;

    public float TravelTime = 0.5f;

    public Color LightColour;
    public bool MaintainVelocity = false;      // exit with same speed/direction/torque as entry

    public float ReactivateDelay = 0.5f;		// entry/exit colliders reactivated after delay

    public AudioClip EntryAudio;
    public AudioClip TravelAudio;
    public AudioClip ExitAudio;

    public bool OneWay = false;            // Entry -> Exit only

	public Cloud LinkedToCloud { get; private set; }       // if linked to a cloud, cloud must 'float' during teleport


    public void LinkToCloud(Cloud cloud, Vector2 startingPosition, Vector2 floatPosition, float floatTime)
    {
        LinkedToCloud = cloud;

        // make sure the portal entry and exit are aligned with the cloud and destination point
        Entry.transform.position = startingPosition;
        Exit.transform.position = floatPosition;
        OneWay = false;

        TravelTime = floatTime;

        cloud.SetColour(LightColour);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = LightColour;

        if (Entry.Zone != null && Exit.Zone != null)
        {
            //Gizmos.color = LightColour;
            Gizmos.DrawLine(Entry.ZonePosition, Exit.ZonePosition);
        }

        if (Entry.Zone != null)
        {
            //Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(Entry.ZonePosition, Entry.ZoneRadius);
        }

        if (Exit.Zone != null)
        {
            //Gizmos.color = Color.red;
            if (OneWay)
                Gizmos.DrawWireCube(Exit.ZonePosition, Vector2.one * Exit.ZoneRadius * 3f);
            else
                Gizmos.DrawWireSphere(Exit.ZonePosition, Exit.ZoneRadius);
        }
    }
}
