﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSettingsSO", menuName = "CustomSO", order = 1)]
public class GameSettings : ScriptableObject
{
	public float MusicVolume;
	public float SFXVolume;

	public bool LogoTreeAnimationPlayed = false;
}
