﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpawner : MonoBehaviour
{
    public Transform BossParent;             // set in inspector
    public Boss BossPrefab;

    public List<BossSpawnData> MinionPositions;     // set in inspector
    public float SpawnTime = 2f;

    private List<Boss> Minions = new List<Boss>();


    public void SpawnMinions()
    {
        foreach (var minionPosition in MinionPositions)
        {
            SpawnMinion(minionPosition);
        }
    }

    private Boss SpawnMinion(BossSpawnData spawnData)
    {
        var minion = Instantiate(BossPrefab, BossParent);
        Minions.Add(minion);

        // save default scale before init
        var minionScale = minion.transform.localScale;
  
        minion.InitMinionBoss(spawnData, this);

        LeanTween.scale(minion.gameObject, minionScale, SpawnTime)
                    .setEaseOutBack();

        LeanTween.move(minion.gameObject, spawnData.SpawnPosition.position, SpawnTime)
                    .setEaseOutBack()
                    .setOnComplete(() =>
                    {
                        minion.Activate();
                    });

        return minion;
    }

    public int MinionCount(bool aliveOnly)
    {
        if (!aliveOnly)
            return Minions.Count;
        else
        {
            int aliveCount = 0;

            foreach (Boss minion in Minions)
            {
                if (minion.State != Boss.BossState.Dead)
                    aliveCount++;
            }

            return aliveCount;
        }
    }
}


[Serializable]
public class BossSpawnData
{
    public Transform SpawnPosition;
    public Transform PatrolDestination;
    public bool IsFlipped;
}
