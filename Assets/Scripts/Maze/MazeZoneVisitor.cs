﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]

// component to allow entry into a MazeZone, reset on collision with ResetCollisionLayers
public class MazeZoneVisitor : MonoBehaviour
{
	public LayerMask ResetCollisionLayers;       // while InMazeZone

	public AudioClip MazeEjectAudio;

	public float MazeEjectHealthCost = 10f;
	public float MazeEjectFuelCost = 10f;

	private Rigidbody2D visitorRB;
	private Collider2D visitorCollider;
	private float visitorGravityScale = 1;		// saved on entry, restored on exit zone
	private float visitorLinearDrag = 0.5f;      // saved on entry, restored on exit zone

	[HideInInspector]
	public bool InOuterTrigger;
	[HideInInspector]
	public bool InInnerTrigger;

	public MazeZone InMazeZone { get; private set; }
	public bool EjectingFromMaze { get; private set; }


	private void Start()
	{
		visitorRB = GetComponent<Rigidbody2D>();			// required	
		visitorCollider = GetComponent<Collider2D>();            // required	
	}

	public void EnterZone(MazeZone zone)
	{
		if (visitorRB != null)
		{
			visitorGravityScale = visitorRB.gravityScale;
			visitorLinearDrag = visitorRB.drag;
			visitorRB.gravityScale = zone.GravityScale;		// restored on exit
			visitorRB.drag = zone.LinearDrag;				// restored on exit
		}

		InMazeZone = zone;
	}

	public void ExitZone(MazeZone zone)
	{
		if (visitorRB != null)
		{
			visitorRB.gravityScale = visitorGravityScale;
			visitorRB.drag = visitorLinearDrag;
		}

		visitorGravityScale = 1;        // default value
		visitorLinearDrag = 0.5f;		// default value
		InMazeZone = null;
	}

	public void Ejecting(bool ejecting)
	{
		EjectingFromMaze = ejecting;
		visitorCollider.enabled = !ejecting;
	}
}
