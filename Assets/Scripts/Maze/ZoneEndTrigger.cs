﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]       // trigger zone

public class ZoneEndTrigger : MonoBehaviour
{
	public BoxCollider2D ZoneCollider;     // set in inspector (for Gizmo) - 'Player' collider mask only

	public bool IsInner;

	private ZoneEndPoint parentEndPoint;

	public bool WasEntered { get; private set; }		// TODO: not currently used
	public bool WasExited { get; private set; }			// TODO: not currently used


	private void Start()
	{
		parentEndPoint = GetComponentInParent<ZoneEndPoint>();
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (parentEndPoint == null)
			return;

		if (parentEndPoint.IsLocked)
			return;

		var zoneVisitor = collision.gameObject.GetComponent<MazeZoneVisitor>();
		if (zoneVisitor == null)
			return;

		WasEntered = true;

		if (IsInner)
			zoneVisitor.InInnerTrigger = true;
		else
			zoneVisitor.InOuterTrigger = true;
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		if (parentEndPoint == null)
			return;

		if (parentEndPoint.IsLocked)
			return;

		var zoneVisitor = collision.gameObject.GetComponent<MazeZoneVisitor>();
		if (zoneVisitor == null)
			return;

		WasExited = true;

		if (IsInner)
		{
			zoneVisitor.InInnerTrigger = false;

			if (!zoneVisitor.InOuterTrigger)        // ie. not leaving inner trigger to exit zone
				parentEndPoint.EnterZone(zoneVisitor);
		}
		else
		{
			zoneVisitor.InOuterTrigger = false;

			if (!zoneVisitor.InInnerTrigger)        // ie. not leaving outer trigger to enter zone
				parentEndPoint.ExitZone(zoneVisitor);
		}
	}

	public void Reset()
	{
		WasEntered = false;
		WasExited = false;
	}


	//private void OnDrawGizmos()
	//{
	//	Gizmos.color = Color.white;

	//	if (ZoneCollider != null)
	//		Gizmos.DrawWireCube(transform.position, ZoneCollider.bounds.size);		// NOTE: assumes zero offset for collider
	//}
}
