﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

[RequireComponent(typeof(AudioSource))]

public class MazeZone : MonoBehaviour
{
	public ZoneEndPoint EndPoint1;
	public ZoneEndPoint EndPoint2;
	public ZoneUnlockKey UnlockKey;

	[Header("Eject Contact Sprite")]
	public SpriteRenderer EjectSprite;
	private Vector2 ejectSpriteScale;

	public Light2D ZoneLight;

	public float VisitorResetTime = 0.5f;
	public float VisitorEjectScaleTime = 0.15f;
	public float VisitorEjectScaleFactor = 2f;

	public float GravityScale;
	public float LinearDrag = 0.5f;

	public bool LockEntryOnEntry = true;            // locked on entry into MazeZone
	public bool LockExitOnEntry = false;            // locked on entry into MazeZone - needs unlocking

	private ZoneEndPoint enteredVia;
	private MazeZoneVisitor visitorInZone;
	private Animator zoneLightAnimator;

	private AudioSource audioSource;

	public bool ZoneIsActive => visitorInZone != null;


	private void Start()
	{
		zoneLightAnimator = ZoneLight.GetComponent<Animator>();
		audioSource = GetComponent<AudioSource>();

		EjectSprite.enabled = false;
		ejectSpriteScale = EjectSprite.transform.localScale;

		LightZone(false);
	}

	// currently only one visitor at a time!
	public void ActivateZone(MazeZoneVisitor visitor, ZoneEndPoint entryPoint)
	{
		if (ZoneIsActive)
			return;

		GameEvents.OnMazeZoneEnter?.Invoke(visitor, this);

		LightZone(true);
		audioSource.Play();         // looping

		visitorInZone = visitor;
		visitorInZone.EnterZone(this);

		enteredVia = entryPoint;

		// lock entry point & set other point to indicate exit
		if (EndPoint1 == entryPoint && LockEntryOnEntry)
			EndPoint1.Lock(true);
		else if (EndPoint1 != entryPoint && LockExitOnEntry)
			EndPoint1.Lock(true);
		else        // set to exit indicator
			EndPoint1.SetEntryExitSprite(false);

		if (EndPoint2 == entryPoint && LockEntryOnEntry)
			EndPoint2.Lock(true);
		else if (EndPoint2 != entryPoint && LockExitOnEntry)
			EndPoint1.Lock(true);
		else        // set to exit indicator
			EndPoint2.SetEntryExitSprite(false);
	}

	public void DeactivateZone(MazeZoneVisitor visitor)
	{
		if (visitorInZone != visitor)
			return;

		visitorInZone.ExitZone(this);
		Reset();

		GameEvents.OnMazeZoneExit?.Invoke(visitor, this);
	}

	public void LockZone(bool locked)
	{
		EndPoint1.Lock(locked);
		EndPoint2.Lock(locked);
	}

	public void ToggleLock()        // via UnlockKey trigger
	{
		EndPoint1.Lock(!EndPoint1.IsLocked);

		if (!EndPoint1.IsLocked && ZoneIsActive)
			EndPoint1.SetEntryExitSprite(false);

		EndPoint2.Lock(!EndPoint2.IsLocked);

		if (!EndPoint2.IsLocked && ZoneIsActive)
			EndPoint2.SetEntryExitSprite(false);
	}

	private void LightZone(bool on)
	{
		if (ZoneLight != null)
			ZoneLight.enabled = on;

		if (zoneLightAnimator != null)
			zoneLightAnimator.enabled = on;
	}

	private void Reset()
	{
		visitorInZone = null;
		enteredVia = null;

		EndPoint1.Reset();
		EndPoint2.Reset();

		LightZone(false);
		audioSource.Stop();

		EjectSprite.enabled = false;
	}


	// scale contact sprite at contact point, then scale stick figure before moving to reset point
	public void EjectVisitor(MazeZoneVisitor visitor, Vector2 contactPoint)
	{
		if (!ZoneIsActive)
			return;

		if (visitor != visitorInZone)       // should never happen!
			return;

		if (visitorInZone.EjectingFromMaze)
			return;

		visitorInZone.Ejecting(true);       // disable collider

		GameEvents.OnMazeZoneEject?.Invoke();

		if (EjectSprite == null)		// eject visitor immediately
		{
			AnimateEjectVisitor();      // scale and move to reset point
			return;
		}

		// animate contact sprite and then eject visitor
		EjectSprite.transform.localScale = Vector2.zero;
		EjectSprite.enabled = true;

		EjectSprite.transform.position = contactPoint;

		// scale contact sprite, then scale stick figure before moving to reset point
		LeanTween.scale(EjectSprite.gameObject, ejectSpriteScale, VisitorEjectScaleTime * 0.5f)
					.setEaseOutBack()
					.setOnComplete(() =>
					{
						AnimateEjectVisitor();		// scale and move to reset point

						LeanTween.scale(EjectSprite.gameObject, Vector2.zero, VisitorEjectScaleTime * 0.5f)
								.setEaseInBack()
								.setOnComplete(() =>
								{
									EjectSprite.enabled = false;
								});
					});
	}

	// scale and move to reset point
	private void AnimateEjectVisitor()
	{
		Vector2 visitorScale = visitorInZone.transform.localScale;

		LeanTween.scale(visitorInZone.gameObject, visitorScale * VisitorEjectScaleFactor, VisitorEjectScaleTime)
					.setEaseOutBack()
					.setLoopPingPong(1)
					.setOnComplete(() =>
					{
						LeanTween.move(visitorInZone.gameObject, enteredVia.ResetPoint, VisitorResetTime)
									.setEaseInQuad()
									.setOnComplete(() =>
									{
										visitorInZone.Ejecting(false);      // re-enable collider
										DeactivateZone(visitorInZone);
									});
		});
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.white;

		if (EndPoint1 != null)
			Gizmos.DrawWireSphere(EndPoint1.transform.position, 2f);
		if (EndPoint2 != null)
			Gizmos.DrawWireSphere(EndPoint2.transform.position, 2f);

		if (EndPoint1 != null && EndPoint2 != null)
			Gizmos.DrawLine(EndPoint1.transform.position, EndPoint2.transform.position);
	}
}
