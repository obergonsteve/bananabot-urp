﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]

public class ZoneEndPoint : MonoBehaviour
{
	public ZoneEndTrigger OuterTrigger;
	public ZoneEndTrigger InnerTrigger;

	public SpriteRenderer EntrySprite;
	public SpriteRenderer ExitSprite;
	public SpriteRenderer LockSprite;

	public Transform ResetPoint;

	public bool LockedAtStart = false;
	public AudioClip LockAudio;

	private AudioSource audioSource;
	private MazeZone parentMazeZone;
	private Collider2D lockCollider;		// enabled / disabled when locked / unlocked

	public bool IsLocked { get; private set; }


	private void Start()
	{
		parentMazeZone = GetComponentInParent<MazeZone>();
		lockCollider = GetComponentInParent<Collider2D>();

		audioSource = GetComponent<AudioSource>();

		SetEntryExitSprite(true);
		Lock(LockedAtStart);
	}


	public void EnterZone(MazeZoneVisitor visitor)
	{
		parentMazeZone.ActivateZone(visitor, this);
	}

	public void ExitZone(MazeZoneVisitor visitor)
	{
		parentMazeZone.DeactivateZone(visitor);
	}

	public void Reset()
	{
		OuterTrigger.Reset();
		InnerTrigger.Reset();

		Lock(LockedAtStart);
	}

	public void Lock(bool locked)
	{
		bool changed = (IsLocked != locked);

		IsLocked = locked;
		LockSprite.enabled = locked;
		lockCollider.enabled = locked;

		if (locked)
		{
			EntrySprite.enabled = false;
			ExitSprite.enabled = false;
		}
		else		// unlocked = entry
			SetEntryExitSprite(true);

		if (changed && LockAudio != null)
		{
			audioSource.clip = LockAudio;
			audioSource.Play();
		}
	}

	public void SetEntryExitSprite(bool entry)
	{
		EntrySprite.enabled = entry;
		ExitSprite.enabled = !entry;
	}


	private void OnDrawGizmos()
	{
		if (ResetPoint != null)
		{
			Gizmos.color = Color.white;
			Gizmos.DrawWireSphere(ResetPoint.transform.position, 1f);
		}

		if (OuterTrigger != null)
		{
			Gizmos.color = Color.green;
			Gizmos.DrawWireCube(OuterTrigger.transform.position, OuterTrigger.ZoneCollider.bounds.size);      // NOTE: assumes zero offset for collider
		}

		if (InnerTrigger != null)
		{
			Gizmos.color = Color.red;
			Gizmos.DrawWireCube(InnerTrigger.transform.position, InnerTrigger.ZoneCollider.bounds.size);      // NOTE: assumes zero offset for collider
		}
	}
}
