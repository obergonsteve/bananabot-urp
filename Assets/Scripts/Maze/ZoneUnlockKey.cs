﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneUnlockKey : MonoBehaviour
{
	private MazeZone parentMazeZone;

	private void Start()
	{
		parentMazeZone = GetComponentInParent<MazeZone>();
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		var zoneVisitor = collision.gameObject.GetComponent<MazeZoneVisitor>();
		if (zoneVisitor == null)
			return;

		parentMazeZone.ToggleLock();
		//parentMazeZone.LockZone(false);		// unlock all
	}
}
