﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.UI;

//public class LifeHeart : MonoBehaviour
//{
//    public Image HeartFull;
//    public Image HeartEmpty;

//    public AudioClip PulseAudio;

//    private const int pulseBeats = 3;
//    private int pulseBeatsRemaining = 0;
//    private float PulseScale = 2f;      // 1.75f;

//    public bool IsFull => HeartFull.enabled;      // empty heart on top of full heart


//    public void OnEnable()
//    {
//        BeatEvents.OnBeat += OnBeat;
//    }

//    public void OnDisable()
//    {
//        BeatEvents.OnBeat -= OnBeat;
//    }

//	private void OnBeat(float BPM, long beatTicks, long lastBeatTicks)
//	{
//        if (pulseBeatsRemaining > 0)
//        {
//            Pulse(BPM);
//            pulseBeatsRemaining--;
//        }
//	}

//	public void SetHeartState(bool full, bool init, bool pulseIfChanged = false)
//    {
//        if (pulseIfChanged)
//        {
//            bool changed = !init && HeartFull.enabled != full;
//            if (changed)
//                SetPulseCount(pulseBeats);
//        }

//        HeartFull.enabled = full;
//        HeartEmpty.enabled = !full;
//    }

//    public void SetPulseCount(int pulses)
//    {
//        pulseBeatsRemaining = pulses;
//    }

//    private void Pulse(float BPM) 
//    {
//        var outScale = Vector3.one * PulseScale;
//        var inScale = transform.localScale;

//        LeanTween.scale(gameObject, outScale, 60f / BPM * BeatKeeper.PumpInTime)
//                    .setEaseInBack()
//                    .setOnComplete(() =>
//                    {
//                        if (PulseAudio != null)
//                            AudioSource.PlayClipAtPoint(PulseAudio, Vector3.zero, AudioManager.SFXVolume);

//                        LeanTween.scale(gameObject, inScale, 60f / BPM * BeatKeeper.PumpOutTime)
//                                    .setEaseOutBack();
//                    });
//    }
//}       
