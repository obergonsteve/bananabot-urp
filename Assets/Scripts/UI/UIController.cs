﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
	public LevelList LevelList;     // scenes
	public GameSettings GameSettings;

	public Text LevelName;				// persists during play
	public Text LevelAuthor;			// persists during play

	public Image StoryPanel;			// at new game only
	//public Text StoryName;
	//public Text StoryAuthor;
	public Text LevelStory;

	public Text TapToPlay;
	public Text SwipeToMove;

	public Image GameLogo;
	public Image CreditsPanel;

	[Header("Options")]
	public Button OptionsButton;
	public CanvasGroup OptionsPanel;

	public Slider MusicVolumeSlider;
	public Slider SFXVolumeSlider;

	public Button LeaderboardButton;

	public Button RestartButton;
	public Button ConfirmRestartButton;
	public Button ResetButton;					// reset data (scores, player profile)
	public Button ConfirmResetButton;					// reset data (scores, player profile)
	public Button QuitButton;
	public Button ConfirmQuitButton;

	public float ConfirmTimeout = 3f;

	private float optionsScaleTime = 0.75f;
	private bool optionsPanelShowing = false;

	[Header("Scores")]
	public Image RunningScorePanel;
	public Text RunningScore;
	public Text PBScore;

	[Header("Game Over")]
	public Image GameOverPanel;
	public Text GameScores;
	public Text PBScores;
	public Text NewPersonalBest;
	public Button PlayAgainButton;
	public Button ExitLevelButton;
	public Button StatsButton;

	public Text ZeroFuel;
	public Text ZeroHealth;
	public Text ZeroTime;

	private Coroutine flashGameOverCoroutine;
	private bool gameOverFlashing = false;
	private float gameOverFlashTime = 0.5f; 

	public Text FinalScore;
	public Text StatsButtonText;
	public CanvasGroup StatsPanel;

	private Coroutine flashNewPBCoroutine;
	private bool newPBFlashing = false;
	private float newPBFlashTime = 0.6f;

	public BonusUI BonusUIPrefab;
	public Transform BonusScores;

	private float scoreScaleTime = 1f;
	private bool hilightScore = false;			// extra pulse (eg. on bonus)
	private float scoreHilightScaleFactor = 1.75f;

	[Header("Leaderboard")]
	public LeaderboardUI LeaderboardUI;
	private bool leaderboardPanelShowing = false;
	private bool leaderboardAnimating = false;

	[Header("Meters")]
	public Image MetersPanel;

	public Slider HealthMeter;
	public Text HealthLabel;
	public Image HealthMeterFill;

	public Slider FuelMeter;
	public Text FuelLabel;
	public Image FuelMeterFill;

	public Slider TimeRemainingMeter;
	public Text TimeLabel;
	public Image TimeMeterFill;

	public float MeterUpdateTime = 0.5f;

	public string HealthName = "HEALTH";
	public string FuelName = "FUEL";
	public string TimeName = "TIME";
	public Color HealthColour;
	public Color FuelColour;
	public Color TimeColour;

	public Slider SwipeMeter;

	public Slider ProximityMeter;
	public Image ProximityMeterFill;

	public Image ProximityPanel;
	public ProximityGauge ProximityGauge;
	private ProximityTrigger currentProximityTrigger;			// meter for only one 'scanner' at a time!

	[Header("Fade to Black")]
	public Image Blackout;
	public Color BlackOutColour;
	public Color WhiteOutColour;

	private float fadeToBlackTime = 0.5f;
	private float fadeToWhiteTime = 0.5f;

	public bool FlashOnHit = false;
	public Color FlashColour = Color.white;
	private float flashTime = 0.01f;

	private float PulseScale = 1.15f;            // eg. slider, score
	private float PulseTime = 0.3f;				// eg. slider

	private const float NewPBPulseScale = 1.5f;    
	private const float NewPBPulseTime = 0.5f;   

	private int playCount;
	private int SwipeToBeatPlayCount = 3;           // 'swipe to beat' message for first 3 games

	private GameManager.GameState gameState;
	private bool GameOver => gameState == GameManager.GameState.GameOver;

	public string CurrentLevelName => (LevelList.CurrentLevel != null && !string.IsNullOrEmpty(LevelList.CurrentLevel.LevelName))
						? LevelList.CurrentLevel.LevelName : "Untitled Story...";
	public string CurrentLevelAuthor => (LevelList.CurrentLevel != null && !string.IsNullOrEmpty(LevelList.CurrentLevel.LevelAuthor))
						? LevelList.CurrentLevel.LevelAuthor : "Anonymous";
	public string CurrentLevelStory => (LevelList.CurrentLevel != null && ! string.IsNullOrEmpty(LevelList.CurrentLevel.LevelStory))
						? LevelList.CurrentLevel.LevelStory : "A Story Untold...";

	public Color CurrentLevelTextColour => LevelList.CurrentLevel != null ? LevelList.CurrentLevel.TextColour : Color.grey;
	public GameScores LevelScores => LevelList.CurrentLevel != null ? LevelList.CurrentLevel.LevelScores : null;


	public void OnEnable()
	{
		GameEvents.OnTapToPlay += OnTapToPlay;
		GameEvents.OnNewGameStarted += OnRestartGame;
		GameEvents.OnStartPlay += OnStartPlay;
		GameEvents.OnPlayerTap += OnPlayerTap;

		GameEvents.OnSwipeEnd += OnSwipeEnd;

		GameEvents.OnGameScoreUpdated += OnGameScoreUpdated;
		GameEvents.OnScoreReset += OnScoreReset;
		GameEvents.OnFinalGameScore += OnFinalGameScore;
		GameEvents.OnNewPersonalBest += OnNewPersonalBest;
		GameEvents.OnBonusScoreFeedback += OnBonusScoreFeedback;
		GameEvents.OnTimeIncreaseFeedback += OnTimeIncreaseFeedback;
		GameEvents.OnToggleLeaderboard += ToggleLeaderboardsPanel;
		GameEvents.OnGameStateChanged += OnGameStateChanged;

		GameEvents.OnPlayerTakeFruit += OnPlayerTakeFruit;
		GameEvents.OnPlayerHealthFuelChanged += OnPlayerHealthFuelChanged;
		GameEvents.OnTimeRemainingChanged += OnTimeRemainingChanged;

		GameEvents.OnFigureProximityEnter += OnFigureProximityEnter;
		GameEvents.OnFigureProximityStay += OnFigureProximityStay;
		GameEvents.OnFigureProximityLeave += OnFigureProximityLeave;

		OptionsButton.onClick.AddListener(ToggleOptionsPanel);
		LeaderboardButton.onClick.AddListener(OnLeaderboardButton);
		RestartButton.onClick.AddListener(OnRestartButton);
		ResetButton.onClick.AddListener(OnResetButton);
		QuitButton.onClick.AddListener(OnQuitButton);

		PlayAgainButton.onClick.AddListener(OnPlayAgain);
		ExitLevelButton.onClick.AddListener(OnExitLevel);
		StatsButton.onClick.AddListener(OnToggleStats);

		ConfirmResetButton.onClick.AddListener(ConfirmResetClicked);
		ConfirmRestartButton.onClick.AddListener(ConfirmRestartClicked);
		ConfirmQuitButton.onClick.AddListener(ConfirmQuitClicked);

		MusicVolumeSlider.onValueChanged.AddListener(MusicVolumeChanged);
		SFXVolumeSlider.onValueChanged.AddListener(SFXVolumeChanged);

		InitMeters();
		InitAudioVolumes();     // from GameSettings
	}

	public void OnDisable()
	{
		GameEvents.OnTapToPlay -= OnTapToPlay;
		GameEvents.OnNewGameStarted -= OnRestartGame;
		GameEvents.OnStartPlay -= OnStartPlay;
		GameEvents.OnPlayerTap -= OnPlayerTap;

		GameEvents.OnSwipeEnd -= OnSwipeEnd;

		GameEvents.OnGameScoreUpdated -= OnGameScoreUpdated;
		GameEvents.OnScoreReset -= OnScoreReset;
		GameEvents.OnFinalGameScore -= OnFinalGameScore;
		GameEvents.OnNewPersonalBest -= OnNewPersonalBest;
		GameEvents.OnBonusScoreFeedback -= OnBonusScoreFeedback;
		GameEvents.OnTimeIncreaseFeedback -= OnTimeIncreaseFeedback;
		GameEvents.OnToggleLeaderboard -= ToggleLeaderboardsPanel;
		GameEvents.OnGameStateChanged -= OnGameStateChanged;

		GameEvents.OnPlayerTakeFruit -= OnPlayerTakeFruit;
		GameEvents.OnPlayerHealthFuelChanged -= OnPlayerHealthFuelChanged;
		GameEvents.OnTimeRemainingChanged -= OnTimeRemainingChanged;

		GameEvents.OnFigureProximityEnter -= OnFigureProximityEnter;
		GameEvents.OnFigureProximityStay -= OnFigureProximityStay;
		GameEvents.OnFigureProximityLeave -= OnFigureProximityLeave;

		OptionsButton.onClick.RemoveListener(ToggleOptionsPanel);
		LeaderboardButton.onClick.RemoveListener(OnLeaderboardButton);
		RestartButton.onClick.RemoveListener(OnRestartButton);
		ResetButton.onClick.RemoveListener(OnResetButton);
		QuitButton.onClick.RemoveListener(OnQuitButton);

		PlayAgainButton.onClick.RemoveListener(OnPlayAgain);
		ExitLevelButton.onClick.RemoveListener(OnExitLevel);
		StatsButton.onClick.RemoveListener(OnToggleStats);

		ConfirmResetButton.onClick.RemoveListener(ConfirmResetClicked);
		ConfirmRestartButton.onClick.RemoveListener(ConfirmRestartClicked);
		ConfirmQuitButton.onClick.RemoveListener(ConfirmQuitClicked);

		MusicVolumeSlider.onValueChanged.RemoveListener(MusicVolumeChanged);
		SFXVolumeSlider.onValueChanged.RemoveListener(SFXVolumeChanged);
	}


	private void Start()
	{
		TapToPlay.gameObject.SetActive(true);

		StoryPanel.gameObject.SetActive(true);

		//GameLogo.gameObject.SetActive(true);
		MetersPanel.gameObject.SetActive(false);

		OptionsButton.gameObject.SetActive(false);

		//CreditsPanel.gameObject.SetActive(true);
		SwipeToMove.gameObject.SetActive(false);

		RunningScorePanel.gameObject.SetActive(false);
		GameOverPanel.gameObject.SetActive(false);
		GameOverPanel.transform.localScale = Vector3.zero;

		//StoryName.text = CurrentLevelName;
		//StoryAuthor.text = "by " + CurrentLevelAuthor;
		LevelStory.text = CurrentLevelStory;

		LevelName.text = CurrentLevelName;
		LevelAuthor.text = "by " + CurrentLevelAuthor;

		LevelName.color = CurrentLevelTextColour;
		LevelAuthor.color = CurrentLevelTextColour;
	}


	private void OnTapToPlay(GameScores scores)
	{
		if (scores != null)
			playCount = scores.PlayCount;

		CreditsPanel.gameObject.SetActive(false);
		ActivateLeaderboardPanel(false); //, true);

		ProximityMeter.gameObject.SetActive(false);
		ProximityPanel.gameObject.SetActive(false);
		currentProximityTrigger = null;

		ShowGameOverPanel(false);
		FadeToNewGame(scores);
	}


	private void OnRestartGame(Action restart)
	{
		FadeToWhite(restart);
	}


	private void OnGameStateChanged(GameManager.GameState oldState, GameManager.GameState newState)
	{
		//Debug.Log($"OnGameStateChanged from {oldState} to {newState}");

		if (gameState == newState)
			return;

		gameState = newState;

		if (GameOver)
		{
			GameLogo.gameObject.SetActive(true);
			MetersPanel.gameObject.SetActive(false);
			SwipeToMove.gameObject.SetActive(false);
			ShowGameOverPanel(true);
			ActivateLeaderboardPanel(true);
			ShowOptionsButton(false);
		}
	}


	private void OnStartPlay()
	{
		GameLogo.gameObject.SetActive(false);
		TapToPlay.gameObject.SetActive(false);

		StoryPanel.gameObject.SetActive(false);

		// 'swipe to beat' prompt for first 3 games?
		SwipeToMove.gameObject.SetActive(playCount <= SwipeToBeatPlayCount);

		ShowOptionsButton(true);

		MetersPanel.gameObject.SetActive(true);

		RunningScorePanel.gameObject.SetActive(true);
		CreditsPanel.gameObject.SetActive(false);
	}

	private void OnPlayerTap(GameManager.GameState gameState, int tapCount)
	{
		if (tapCount > 1)
			SwipeToMove.gameObject.SetActive(false);        // first tap = 'Tap to Play!'
	}


	private void OnSwipeEnd(Vector2 endPosition, Vector2 direction, float speed, int touchCount, int tapCount)
	{
		LeanTween.value(SwipeMeter.value, speed, MeterUpdateTime * 0.5f)
			.setOnUpdate((float f) => { SwipeMeter.value = f; })
			.setEaseOutQuart()
			.setOnComplete(() =>
			{
				LeanTween.value(speed, 0f, MeterUpdateTime * 2f)
							.setOnUpdate((float f) => { SwipeMeter.value = f; })
							.setEaseInQuart();	
			});
	}


	private void OnPlayerTakeFruit(float fuel, float health, bool isRotten, float toxicity, float timeIncrease, bool isToxic,
							CardManager.CardBackType cardBackType, Color scoreColour, Vector3 fruitPosition, string scoreMessage)
	{
		if (FlashOnHit && ! GameOver)
			StartCoroutine(WhiteFlash());
	}

	private void OnPlayerHealthFuelChanged(float healthChange, float healthLevel, float maxHealth, float fuelChange, float fuelLevel, float maxFuel, bool animate)
	{
		bool noHealthChange = (HealthMeter.value == healthLevel && HealthMeter.maxValue == maxHealth);      // no change!
		bool noFuelChange = (FuelMeter.value == fuelLevel && FuelMeter.maxValue == maxFuel);		// no change!

		if (animate)              // only pulse if not already full
		{
			bool healthFull = HealthMeter.value == maxHealth && healthChange >= 0f;

			if (! healthFull && ! noHealthChange)		// not already full and value has changed
				Pulse(HealthMeter.gameObject);

			bool fuelFull = FuelMeter.value == maxFuel && fuelChange >= 0f;

			if (! fuelFull && ! noFuelChange)       // not already full and value has changed
				Pulse(FuelMeter.gameObject);
		}

		if (!noHealthChange)
		{
			HealthMeter.maxValue = maxHealth;

			if (animate)
			{
				LeanTween.value(HealthMeter.value, healthLevel, MeterUpdateTime)
							.setOnUpdate((float f) => { HealthMeter.value = f; })
							.setEaseOutQuart();
			}
			else
				HealthMeter.value = healthLevel;
		}

		if (!noFuelChange)
		{
			FuelMeter.maxValue = maxFuel;

			if (animate)
			{
				LeanTween.value(FuelMeter.value, fuelLevel, MeterUpdateTime)
						.setOnUpdate((float f) => { FuelMeter.value = f; })
						.setEaseOutQuart();
			}
			else
				FuelMeter.value = fuelLevel;
		}
	}


	private void OnTimeRemainingChanged(float timeChange, float timeRemaining, float timeLimit, bool animate)
	{
		if (TimeRemainingMeter.value == timeRemaining && TimeRemainingMeter.maxValue == timeLimit)      // no change!
			return;

		// only pulse if not already full
		bool timeFull = TimeRemainingMeter.value == timeLimit && timeChange >= 0f;

		if (animate && ! timeFull)
			Pulse(TimeRemainingMeter.gameObject);

		TimeRemainingMeter.maxValue = timeLimit;
		if (animate)
		{
			LeanTween.value(TimeRemainingMeter.value, timeRemaining, MeterUpdateTime)
						.setOnUpdate((float f) => { TimeRemainingMeter.value = f; })
						.setEaseOutQuart();
		}
		else
			TimeRemainingMeter.value = timeRemaining;
	}


	private void OnFigureProximityEnter(ProximityTrigger scannerTrigger, ProximityTrigger intruderTrigger)
	{
		if (currentProximityTrigger != null)
			return;

		currentProximityTrigger = scannerTrigger;

		//ProximityMeter.gameObject.SetActive(true);
		//ProximityMeter.value = 0f;

		ProximityPanel.gameObject.SetActive(true);
		StartCoroutine(ProximityGauge.SetValue(0f, 1f, true));		// init blocks
	}

	private void OnFigureProximityStay(ProximityTrigger scannerTrigger, ProximityTrigger intruderTrigger, float proximityPercent) //, Color proximityColour)
	{
		//ProximityMeter.value = proximityPercent;
		//ProximityMeterFill.color = proximityColour;

		StartCoroutine(ProximityGauge.SetValue(proximityPercent, 1f, false));
	}

	private void OnFigureProximityLeave(ProximityTrigger scannerTrigger, ProximityTrigger intruderTrigger)
	{
		//ProximityMeter.gameObject.SetActive(false);
		//ProximityMeter.value = 0f;

		ProximityPanel.gameObject.SetActive(false);
		StartCoroutine(ProximityGauge.SetValue(0f, 1f, false));

		currentProximityTrigger = null;
	}


	private void Pulse(GameObject UIObject, Action onComplete = null)
	{
		var largeScale = Vector2.one * PulseScale;

		LeanTween.scale(UIObject.gameObject, largeScale, PulseTime)
			.setEaseInBack()
			.setOnComplete(() =>
			{
				LeanTween.scale(UIObject.gameObject, Vector2.one, PulseTime * 0.5f)
					.setEaseOutBack()
					.setOnComplete(() =>
					{
						onComplete?.Invoke();
					});
			});
	}


	private void OnGameScoreUpdated(long runningScore, int scoreChange, Color scoreColour, Vector3 fruitPosition, string scoreMessage)
	{
		UpdateRunningScore(runningScore);

		//Debug.Log($"OnGameScoreUpdated scoreChange {scoreChange}, scoreMessage {scoreMessage}");

		// feedback (BonusUI)
		if (!string.IsNullOrEmpty(scoreMessage) || scoreChange > 0)
			GameEvents.OnBonusScoreFeedback?.Invoke(scoreColour, fruitPosition, scoreMessage, scoreChange);

		//// empty message and zero score ... time increase?
		//else if (!string.IsNullOrEmpty(TimeMessage) || (int)(secondsRemaining * timeFactor) > 0)
		//	GameEvents.OnTimeIncreaseFeedback?.Invoke(TimeColour, position, TimeMessage, (int)(secondsRemaining * timeFactor), HitScore);
	}

	private void OnScoreReset()
	{
		SetScoreText(0);
	}

	private void OnFinalGameScore(GameScores scores, bool newPB, GameManager.GameOverState state)
	{
		ShowGameOverReason(state);		// flash text

		//ZeroFuel.gameObject.SetActive(state == GameManager.GameOverState.ZeroFuel);
		//ZeroHealth.gameObject.SetActive(state == GameManager.GameOverState.ZeroHealth);
		//ZeroTime.gameObject.SetActive(state == GameManager.GameOverState.ZeroTime);

		if (scores != null)
		{
			// stats
			//string gamePlayTime = FormatPlayTime(scores.GamePlayTime);
			GameScores.text = String.Format("{0}\n{1}\n{2}\n{3}\n{4}",
											"GAME", (int)scores.GameHealthGained, (int)scores.GameFuelGained, (int)scores.GameTimeGained, (int)scores.GameBonusScore);

			//string PBPlayTime = FormatPlayTime(scores.PBPlayTime); 
			PBScores.text = String.Format("{0}\n{1}\n{2}\n{3}\n{4}",
											"BEST", (int)scores.PBHealthGained, (int)scores.GameFuelGained, (int)scores.PBTimeGained, (int)scores.PBBonusScore);

			// final score
			FinalScore.text = String.Format("{0}\n{1:N0}", "FINAL SCORE", scores.RunningScore);
		}

		FinalScore.gameObject.SetActive(true);

		ShowNewPB(newPB);
	}

	private void ShowGameOverReason(GameManager.GameOverState state)
	{
		ZeroFuel.gameObject.SetActive(state == GameManager.GameOverState.ZeroFuel);
		ZeroHealth.gameObject.SetActive(state == GameManager.GameOverState.ZeroHealth);
		ZeroTime.gameObject.SetActive(state == GameManager.GameOverState.ZeroTime);

		// flash game over reason
		gameOverFlashing = true;

		switch (state)
		{
			case GameManager.GameOverState.None:
				break;
			case GameManager.GameOverState.ZeroFuel:
				flashGameOverCoroutine = StartCoroutine(FlashGameOverReason(ZeroFuel));
				break;
			case GameManager.GameOverState.ZeroHealth:
				flashGameOverCoroutine = StartCoroutine(FlashGameOverReason(ZeroHealth));
				break;
			case GameManager.GameOverState.ZeroTime:
				flashGameOverCoroutine = StartCoroutine(FlashGameOverReason(ZeroTime));
				break;
		}
	}

	private IEnumerator FlashGameOverReason(Text reason)
	{
		while (gameOverFlashing)
		{
			reason.gameObject.SetActive(false);
			yield return new WaitForSeconds(gameOverFlashTime);
			reason.gameObject.SetActive(true);
			yield return new WaitForSeconds(gameOverFlashTime);
		}

		yield return null;
	}

	private void StopGameOverFlashing()
	{
		gameOverFlashing = false;

		if (flashGameOverCoroutine != null)
			StopCoroutine(flashGameOverCoroutine);
	}

		private void ShowNewPB(bool show)
	{
		if (show == newPBFlashing)		// no change!
			return;

		newPBFlashing = show;

		if (newPBFlashing)
		{
			flashNewPBCoroutine = StartCoroutine(FlashNewPB());
		}
		else
		{
			if (flashNewPBCoroutine != null)
				StopCoroutine(flashNewPBCoroutine);

			NewPersonalBest.gameObject.SetActive(false);
		}
	}

	private IEnumerator FlashNewPB()
	{
		while (newPBFlashing)
		{
			FinalScore.gameObject.SetActive(false);
			NewPersonalBest.gameObject.SetActive(true);
			yield return new WaitForSeconds(newPBFlashTime);
			FinalScore.gameObject.SetActive(true);
			NewPersonalBest.gameObject.SetActive(false);
			yield return new WaitForSeconds(newPBFlashTime);
		}

		yield return null;
	}

	private void ShowGameOverPanel(bool show)
	{
		if (show)
		{
			GameOverPanel.gameObject.SetActive(true);
			StatsPanel.gameObject.SetActive(false);

			LeanTween.scale(GameOverPanel.gameObject, Vector3.one, scoreScaleTime)
							.setEaseOutElastic();
		}
		else
		{
			StopGameOverFlashing();

			LeanTween.scale(GameOverPanel.gameObject, Vector3.zero, scoreScaleTime * 0.5f)
							.setEaseInCubic()
							.setOnComplete(() => { GameOverPanel.gameObject.SetActive(false); });

			ShowNewPB(false);
		}

		MetersPanel.gameObject.SetActive(!show);
	}

	private void OnBonusScoreFeedback(Color colour, Vector3 worldPosition, string message, int bonusScore)
	{
		if (GameOver)
			return;

		if (string.IsNullOrEmpty(message) && bonusScore == 0)
			return;

		var bonusUI = Instantiate(BonusUIPrefab, BonusScores);
		bonusUI.transform.position = Camera.main.WorldToScreenPoint(worldPosition);
		bonusUI.BonusFeedback(colour, message, bonusScore, RunningScore.transform.position);
	}

	private void OnTimeIncreaseFeedback(Color colour, Vector3 worldPosition, string message, int timeIncrease, int bonusScore)
	{
		if (GameOver)
			return;

		// time increase feedback
		if (!string.IsNullOrEmpty(message) || timeIncrease > 0)
		{
			var timeBonusUI = Instantiate(BonusUIPrefab, BonusScores);
			timeBonusUI.transform.position = Camera.main.WorldToScreenPoint(worldPosition);
			timeBonusUI.BonusFeedback(colour, message, timeIncrease, TimeRemainingMeter.transform.position);
		}

		// bonus score feedback
		if (bonusScore > 0)
		{
			var bonusUI = Instantiate(BonusUIPrefab, BonusScores);
			bonusUI.transform.position = Camera.main.WorldToScreenPoint(worldPosition);
			bonusUI.BonusFeedback(colour, "", bonusScore, RunningScore.transform.position);
		}
	}


	private void OnNewPersonalBest(GameScores scores, bool reloadScores)
	{
		UpdatePersonalBest(scores.PersonalBest);
		GameEvents.OnToggleLeaderboard?.Invoke();
	}


	private string FormatPlayTime(int playSeconds)
	{
		//Debug.Log("FormatPlayTime: playSeconds = " + playSeconds);

		int minutes = Math.Abs(playSeconds / 60);
		int hours = Math.Abs(minutes / 60);
		int remainderSecs = Math.Abs(playSeconds % 60);

		string seconds = (remainderSecs < 10) ? ("0" + remainderSecs.ToString()) : remainderSecs.ToString();
		return (hours > 0 ? (hours + ":") : "") + minutes + ":" + seconds;
	}

	private void UpdateRunningScore(long score)
	{
		SetScoreText(score);
	}

	private void UpdatePersonalBest(long newPB)
	{
		PBScore.transform.localScale = Vector3.one;

		var outScale = Vector3.one * NewPBPulseScale;
		var inScale = PBScore.transform.localScale;

		LeanTween.scale(PBScore.gameObject, outScale, NewPBPulseTime)
					.setEaseInBack()
					.setOnComplete(() =>
					{
						SetPBText(newPB);

						LeanTween.scale(PBScore.gameObject, inScale, NewPBPulseTime)
									.setEaseOutBack();
					});
	}


	private void SetScoreText(long score)
	{
		RunningScore.text = String.Format("SCORE: {0:N0}", score);
		Pulse(RunningScore.gameObject);
	}

	private void SetPBText(long PB)
	{
		PBScore.text = String.Format("BEST: {0:N0}", PB);
	}


	private void FadeToNewGame(GameScores scores, Action onBlackOut = null)
	{
		Blackout.color = Color.clear;
		Blackout.gameObject.SetActive(true);

		LeanTween.color(Blackout.gameObject, BlackOutColour, fadeToBlackTime)
				.setOnUpdate((Color c) => { Blackout.color = c; })
				.setEaseOutSine()
				.setOnComplete(() =>
				{
					onBlackOut?.Invoke();

					if (scores != null)
						SetPBText(scores.PersonalBest);

					SetScoreText(0);

					GameEvents.OnStartPlay?.Invoke();

					LeanTween.color(Blackout.gameObject, Color.clear, fadeToBlackTime * 1.5f)
							.setOnUpdate((Color c) => { Blackout.color = c; })
							.setEaseInSine()
							.setOnComplete(() =>
							{
								Blackout.gameObject.SetActive(false);
							});
				});
	}


	private void FadeToWhite(Action onWhiteOut = null)
	{
		Blackout.color = Color.clear;
		Blackout.gameObject.SetActive(true);

		LeanTween.color(Blackout.gameObject, WhiteOutColour, fadeToWhiteTime)
				.setOnUpdate((Color c) => { Blackout.color = c; })
				.setEaseOutCubic()
				.setOnComplete(() =>
				{
					onWhiteOut?.Invoke();

					LeanTween.color(Blackout.gameObject, Color.clear, fadeToWhiteTime * 1.5f)
							.setOnUpdate((Color c) => { Blackout.color = c; })
							.setEaseInCubic()
							.setOnComplete(() =>
							{
								Blackout.gameObject.SetActive(false);
							});
				});
	}

	private IEnumerator WhiteFlash()
	{
		Blackout.gameObject.SetActive(false);
		Blackout.color = FlashColour;
		Blackout.gameObject.SetActive(true);
		yield return new WaitForSeconds(flashTime);
		Blackout.gameObject.SetActive(false);
		yield return null;
	}

	/// <summary>
	/// Converts the altitude into a degree rotation between -90 and 90 for use with the altimeter
	/// </summary>
	/// <returns>Returns the value of the rotation</returns>
	private float ConvertAltToRotation(float currentAltitude, float maxAltitude)
    {
		return Mathf.Clamp(((currentAltitude / maxAltitude * 180) - 90) * -1, -90f, 90f);
    }


	private void ToggleOptionsPanel()
	{
		optionsPanelShowing = !optionsPanelShowing;
		GameEvents.OnGameOptions?.Invoke(optionsPanelShowing);

		ScaleOptionsPanel(optionsPanelShowing);

		if (!optionsPanelShowing)
			ActivateLeaderboardPanel(false);

		MetersPanel.gameObject.SetActive(!optionsPanelShowing);
	}

	private void ScaleOptionsPanel(bool showing)
	{
		if (showing)
		{
			OptionsPanel.alpha = 0f;
			OptionsPanel.transform.localScale = Vector3.zero;
			OptionsPanel.gameObject.SetActive(true);

			ResetButtons();

			LeanTween.scale(OptionsPanel.gameObject, Vector3.one, optionsScaleTime)
							.setEaseOutElastic();

			LeanTween.value(OptionsPanel.gameObject, 0f, 1f, optionsScaleTime)
							.setEaseOutElastic()
							.setOnUpdate((float f) => { OptionsPanel.alpha = f; });
		}
		else
		{
			LeanTween.scale(OptionsPanel.gameObject, Vector3.zero, optionsScaleTime * 0.5f)
							.setEaseInCubic();

			LeanTween.value(OptionsPanel.gameObject, 1f, 0f, optionsScaleTime * 0.5f)
							.setEaseInCubic()
							.setOnUpdate((float f) => { OptionsPanel.alpha = f; })
							.setOnComplete(() =>
							{
								OptionsPanel.gameObject.SetActive(false);
							});
		}
	}

	private void ResetButtons()
	{
		RestartButton.gameObject.SetActive(true);
		RestartButton.interactable = true;
		ConfirmRestartButton.gameObject.SetActive(false);

		ResetButton.gameObject.SetActive(true);
		ResetButton.interactable = true;
		ConfirmResetButton.gameObject.SetActive(false);

		QuitButton.gameObject.SetActive(true);
		QuitButton.interactable = true;
		ConfirmQuitButton.gameObject.SetActive(false);
	}

	private void ToggleLeaderboardsPanel()
	{
		ActivateLeaderboardPanel(!leaderboardPanelShowing);
	}

	private void ActivateLeaderboardPanel(bool show, bool force = false)
	{
		//Debug.Log($"ActivateLeaderboardPanel showing = {showing} leaderboardPanelShowing = {leaderboardPanelShowing}");

		if (!force && show == leaderboardPanelShowing)
			return;

		if (leaderboardAnimating)
			return;

		leaderboardAnimating = true;

		CanvasGroup LeaderboardPanel = LeaderboardUI.GetComponent<CanvasGroup>();

		if (show)
		{
			LeaderboardUI.Populate();		// player name/school, schools, top scores

			LeaderboardPanel.alpha = 0f;
			LeaderboardPanel.transform.localScale = Vector3.zero;
			LeaderboardPanel.gameObject.SetActive(true);

			LeanTween.scale(LeaderboardPanel.gameObject, Vector3.one, optionsScaleTime)
							.setEaseOutElastic();

			LeanTween.value(LeaderboardPanel.gameObject, 0f, 1f, optionsScaleTime)
							.setEaseOutElastic()
							.setOnUpdate((float f) => { LeaderboardPanel.alpha = f; })
							.setOnComplete(() =>
							{
								leaderboardAnimating = false;
								leaderboardPanelShowing = true;
							});
		}
		else
		{
			LeaderboardUI.CancelPopulate();       // cancel score retrieval

			LeanTween.scale(LeaderboardPanel.gameObject, Vector3.zero, optionsScaleTime * 0.5f)
							.setEaseInCubic();

			LeanTween.value(LeaderboardPanel.gameObject, 1f, 0f, optionsScaleTime * 0.5f)
							.setEaseInCubic()
							.setOnUpdate((float f) => { LeaderboardPanel.alpha = f; })
							.setOnComplete(() =>
							{
								leaderboardAnimating = false;
								LeaderboardPanel.gameObject.SetActive(false);
								leaderboardPanelShowing = false;
							});
		}
	}

	private void ShowOptionsButton(bool showing)
	{
		if (showing)
		{
			OptionsButton.transform.localScale = Vector3.zero;
			OptionsButton.gameObject.SetActive(true);

			LeanTween.scale(OptionsButton.gameObject, Vector3.one, optionsScaleTime)
							.setEaseOutElastic();
		}
		else
		{
			LeanTween.scale(OptionsButton.gameObject, Vector3.zero, optionsScaleTime * 0.5f)
							.setEaseInCubic()
							.setOnComplete(() =>
							{
								OptionsButton.gameObject.SetActive(false);
							});
		}
	}

	private void InitMeters()
	{
		HealthLabel.text = HealthName;
		HealthLabel.color = HealthColour;
		HealthMeterFill.color = HealthColour;

		FuelLabel.text = FuelName;
		FuelLabel.color = FuelColour;
		FuelMeterFill.color = FuelColour;

		TimeLabel.text = TimeName;
		TimeLabel.color = TimeColour;
		TimeMeterFill.color = TimeColour;
	}

	private void InitAudioVolumes()
	{
		MusicVolumeSlider.value = GameSettings.MusicVolume;
		SFXVolumeSlider.value = GameSettings.SFXVolume;

		GameEvents.OnMusicVolumeChanged?.Invoke(GameSettings.MusicVolume);
		GameEvents.OnSFXVolumeChanged?.Invoke(GameSettings.SFXVolume);
	}

	private void MusicVolumeChanged(float vol)
	{
		GameSettings.MusicVolume = vol;
		GameEvents.OnMusicVolumeChanged?.Invoke(vol);
	}

	private void SFXVolumeChanged(float vol)
	{
		GameSettings.SFXVolume = vol;
		GameEvents.OnSFXVolumeChanged?.Invoke(vol);
	}


	private void OnLeaderboardButton()
	{
		GameEvents.OnToggleLeaderboard?.Invoke();
	}


	private void OnPlayAgain()
	{
		ShowGameOverPanel(false);
		ActivateLeaderboardPanel(false); //, true);

		GameEvents.OnStartNewGame?.Invoke();
	}


	private void OnExitLevel()
	{
		GameEvents.OnSceneQuit?.Invoke();
	}


	private void OnToggleStats()
	{
		bool show = ! StatsPanel.gameObject.activeSelf;

		//StatsButtonText.text = show ? "HIDE STATS" : "SHOW STATS";

		if (show)
		{
			StatsPanel.alpha = 0f;
			StatsPanel.transform.localScale = Vector3.zero;
			StatsPanel.gameObject.SetActive(true);

			LeanTween.scale(StatsPanel.gameObject, Vector3.one, scoreScaleTime)
							.setEaseOutElastic();

			LeanTween.value(StatsPanel.gameObject, 0f, 1f, scoreScaleTime)
							.setEaseOutElastic()
							.setOnUpdate((float f) => { StatsPanel.alpha = f; });
		}
		else
		{
			LeanTween.scale(StatsPanel.gameObject, Vector3.zero, scoreScaleTime * 0.5f)
							.setEaseInCubic();

			LeanTween.value(StatsPanel.gameObject, 1f, 0f, scoreScaleTime * 0.5f)
							.setEaseInCubic()
							.setOnUpdate((float f) => { StatsPanel.alpha = f; })
							.setOnComplete(() => StatsPanel.gameObject.SetActive(false));
		}
	}


	private void OnRestartButton()
	{
		StartCoroutine(ConfirmRestartTimer());
	}

	private void ConfirmRestartClicked()
	{
		ToggleOptionsPanel();       // hide options
		GameEvents.OnStartNewGame?.Invoke();

		RestartButton.gameObject.SetActive(true);
		ConfirmRestartButton.gameObject.SetActive(false);

		ResetButton.interactable = true;
		QuitButton.interactable = true;
	}

	private IEnumerator ConfirmRestartTimer()
	{
		RestartButton.gameObject.SetActive(false);
		ConfirmRestartButton.gameObject.SetActive(true);

		ResetButton.interactable = false;
		QuitButton.interactable = false;

		yield return new WaitForSeconds(ConfirmTimeout);

		RestartButton.gameObject.SetActive(true);
		ConfirmRestartButton.gameObject.SetActive(false);

		ResetButton.interactable = true;
		QuitButton.interactable = true;

		yield return null;
	}

	private void OnResetButton()
	{
		StartCoroutine(ConfirmResetTimer());
	}

	private void ConfirmResetClicked()
	{
		ToggleOptionsPanel();       // hide options

		if (LevelScores != null)
			LevelScores.ResetData();

		//GameEvents.OnResetData?.Invoke();

		// restart after reset
		GameEvents.OnStartNewGame?.Invoke();

		ResetButton.gameObject.SetActive(true);
		ConfirmResetButton.gameObject.SetActive(false);

		RestartButton.interactable = true;
		QuitButton.interactable = true;
	}

	private IEnumerator ConfirmResetTimer()
	{
		ResetButton.gameObject.SetActive(false);
		ConfirmResetButton.gameObject.SetActive(true);

		RestartButton.interactable = false;
		QuitButton.interactable = false;

		yield return new WaitForSeconds(ConfirmTimeout);

		ResetButton.gameObject.SetActive(true);
		ConfirmResetButton.gameObject.SetActive(false);

		RestartButton.interactable = true;
		QuitButton.interactable = true;

		yield return null;
	}

	private void OnQuitButton()
	{
		StartCoroutine(ConfirmQuitTimer());
	}

	private void ConfirmQuitClicked()
	{
		ToggleOptionsPanel();      // hide options
		GameEvents.OnSceneQuit?.Invoke();

		QuitButton.gameObject.SetActive(true);
		ConfirmQuitButton.gameObject.SetActive(false);

		RestartButton.interactable = true;
		ResetButton.interactable = true;
	}

	private IEnumerator ConfirmQuitTimer()
	{
		QuitButton.gameObject.SetActive(false);
		ConfirmQuitButton.gameObject.SetActive(true);

		RestartButton.interactable = false;
		ResetButton.interactable = false;

		yield return new WaitForSeconds(ConfirmTimeout);

		QuitButton.gameObject.SetActive(true);
		ConfirmQuitButton.gameObject.SetActive(false);

		RestartButton.interactable = true;
		ResetButton.interactable = true;

		yield return null;
	}

	private void OnApplicationQuit()
	{
		GameSettings.LogoTreeAnimationPlayed = false;		// reset for next play
	}
}
