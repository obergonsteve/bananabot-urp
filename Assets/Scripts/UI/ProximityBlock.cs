﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProximityBlock : MonoBehaviour
{
    public Image EmptyBlock;        // semi-trans colour of full block
    public Image FullBlock;

    private float PulseTime = 0.1f;
    private float PulseScale = 1.25f;

    [Range(0, 1)]
    public float EmptyTransparency;

    private bool pulsing = false;
    public bool IsFull => FullBlock.enabled;      // empty block on top of full block



    public void SetState(bool full, bool pulse)
    {
        if (pulse)
            Pulse();

        FullBlock.enabled = full;
        EmptyBlock.enabled = !full;
    }


    private void Pulse()
    {
        if (pulsing)
            return;

        var outScale = Vector3.one * PulseScale;
        var inScale = Vector3.one;

        pulsing = true;

        LeanTween.scale(gameObject, outScale, PulseTime)
                    .setEaseInBack()
                    .setOnComplete(() =>
                    {
                        LeanTween.scale(gameObject, inScale, PulseTime)
                                    .setEaseOutBack()
                                    .setOnComplete(() => { pulsing = false; });
                    });
    }

    public void SetColour(Color colour)
    {
        FullBlock.color = colour;
        EmptyBlock.color = new Color(colour.r, colour.g, colour.b, EmptyTransparency);
    }
}
