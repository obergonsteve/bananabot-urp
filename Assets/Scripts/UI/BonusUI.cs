﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BonusUI : MonoBehaviour
{
	public Text Message;
	public Text Score;

	private const float scorePulseScale = 2f;
	private const float messagePulseScale = 1.75f;
	private const float textPulseTime = 0.25f;
	private const float messageDelayTime = 0.2f;
	private const float scoreMoveTime = 0.75f;

	private const float destroyDelayTime = 2f;
	private const float transparency = 0.8f;

	private Vector3 gameScorePosition;



	public void BonusFeedback(Color colour, string message, int bonusScore, Vector3 gameScorePosition)
	{
		if (string.IsNullOrEmpty(message) && bonusScore == 0)        // nothing to show!
		{
			Destroy(gameObject, destroyDelayTime);
			return;
		}

		var transColour = TransparentColour(colour);
		Message.color = transColour;
		Score.color = transColour;
		Message.transform.localScale = Vector3.zero;
		Score.transform.localScale = Vector3.zero;

		this.gameScorePosition = gameScorePosition;

		Message.text = string.IsNullOrEmpty(message) ? "" : message;
		// multiply score (for effect)
		Score.text = bonusScore > 0 ? String.Format("+{0:N0}", bonusScore * GameScores.ScoreFactor) : "";

		// if there is a score, pulse it, update the game score text, pulse the message if present and then destroy object
		if (Score.text != "")
			PulseScore(bonusScore);
		// else just pulse the message and then destroy the object
		else if (Message.text != "")
			PulseMessage(true);
	}

	private Color TransparentColour(Color colour)
	{
		return new Color(colour.r, colour.g, colour.b, transparency);
	}

    private void PulseScore(int bonusScore)
	{
		if (Score.text == "")
			return;

		LeanTween.scale(Score.gameObject, Vector3.one * scorePulseScale, textPulseTime)
								.setEase(LeanTweenType.easeOutQuart)
								//.setDelay(textDelayTime)
								.setOnComplete(() =>
								{
									AnimateGameScore(bonusScore);      // moves to game score + destroys object on completion
									PulseMessage(false);
								});
    }


	// move to game score + destroy object on completion
	private void AnimateGameScore(int bonusScore)
	{
		//Debug.Log($"UpdateGameScore {bonusScore}");

		// shrink score
		LeanTween.scale(Score.gameObject, Vector3.zero, scoreMoveTime)
					            .setEase(LeanTweenType.easeInQuart);

		if (gameScorePosition == null)
			return;

		// move score to game score
		LeanTween.move(Score.gameObject, gameScorePosition, scoreMoveTime)
							    .setEaseInQuart()
							    .setOnComplete(() =>
							    {
									// update game score
									GameEvents.OnBonusScoreIncrement?.Invoke(bonusScore);
									Destroy(gameObject, destroyDelayTime);
								});
	}

	private void PulseMessage(bool destroyOnComplete)
    {
		if (Message.text == "")
			return;

		LeanTween.scale(Message.gameObject, Vector3.one * messagePulseScale, textPulseTime)
						        .setEase(LeanTweenType.easeOutQuart)
								.setDelay(messageDelayTime)
								.setLoopPingPong(1)
						        .setOnComplete(() =>
						        {
									//if (destroyOnComplete)      // if score present, object destroyed on game score update
									//	Destroy(gameObject, destroyDelayTime);
						        });
    }
}
