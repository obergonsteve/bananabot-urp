﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ProximityGauge : MonoBehaviour
{
    public ProximityBlock ProximityBlockPrefab;
    public Gradient ColourGradient;

    public int NumBlocks;                // max value divided into blocks

    private List<ProximityBlock> proximityBlocks = new List<ProximityBlock>();
    private int fullBlockCount = -1;        // to detect change in blocks

    private float updateInterval = 0.02f;   // blocks


    private void InitBlocks()
    {
        proximityBlocks.Clear();

        foreach (Transform block in transform)
        {
            Destroy(block.gameObject);
        }

        for (int i = 0; i < NumBlocks; i++)
        {
            var newBlock = Instantiate(ProximityBlockPrefab, transform);
            //newBlock.gameObject.SetActive(false);
            proximityBlocks.Add(newBlock);

            newBlock.SetState(false, false);
            //newBlock.gameObject.SetActive(true);
        }

        fullBlockCount = -1;
    }

    // returns true if value increased
    public IEnumerator SetValue(float newValue, float maxValue, bool init)
    {
        float valuePerBlock = maxValue / NumBlocks;
        int filledBlocks = Mathf.CeilToInt(newValue / valuePerBlock);

        // first block should always be filled (ie. if at very edge of proximity trigger)
        if (filledBlocks == 0)
            filledBlocks = 1;

        //Debug.Log($"SetValue: newValue {newValue} valuePerBlock {valuePerBlock} filledBlocks {filledBlocks}");

        if (init) //newValue == 0)
            InitBlocks();

        if (filledBlocks == fullBlockCount)         // no change in blocks
            yield break;

        //bool increased = filledBlocks > fullBlockCount;
        fullBlockCount = filledBlocks;

        GameEvents.OnProximityBlocksChanged?.Invoke(newValue, filledBlocks);

        EmptyAll();

		for (int i = 0; i < proximityBlocks.Count; i++)
        {
            bool full = i < filledBlocks;

            // initialise block colours according to charge value
            if (init) //newValue == 0)
            {
				proximityBlocks[i].SetColour(ColourGradient.Evaluate(valuePerBlock * (i + 1) / maxValue));
                fullBlockCount = 0;
            }

            proximityBlocks[i].SetState(full, full);

            if (full)
				yield return new WaitForSeconds(updateInterval);
			else
				yield return null;
		}
    }

    //public IEnumerator SetCharge(float newCharge, float maxCharge)
    //{
    //    float chargePerBlock = maxCharge / NumBlocks;
    //    int fullChargeBlocks = (int)(newCharge / chargePerBlock);

    //    if (newCharge == 0)
    //        InitBlocks();

    //    if (fullChargeBlocks == fullBlockCount)         // no change in blocks
    //        yield break;

    //    fullBlockCount = fullChargeBlocks;

    //    EmptyAll();

    //    //if (newCharge > 0)
    //    //{
    //    //    if (ChargeUpAudio != null)
    //    //        AudioSource.PlayClipAtPoint(ChargeUpAudio, Vector3.zero, AudioManager.SFXVolume);

    //    //    BeatEvents.OnLifeChargeIncrement?.Invoke(newCharge, fullChargeBlocks);      // electricity particles
    //    //}

    //    for (int i = 0; i < chargeBlocks.Count; i++)
    //    {
    //        bool full = i < fullChargeBlocks;

    //        // initialise block colours according to charge value
    //        if (newCharge == 0)
    //        {
    //            chargeBlocks[i].SetColour(ColourGradient.Evaluate(chargePerBlock * (i + 1) / maxCharge));
    //            fullBlockCount = 0;
    //        }

    //        chargeBlocks[i].SetChargeState(full, full);

    //        if (full)
    //            yield return new WaitForSeconds(updateInterval);
    //        else
    //            yield return null;
    //    }
    //}

    private void EmptyAll()
    {
        foreach (var block in proximityBlocks)
        {
            block.SetState(false, false);
        }
    }
}
