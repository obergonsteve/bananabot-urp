﻿	using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

[RequireComponent(typeof(Transportable))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Collider2D))]

public class StickFigure : MonoBehaviour
{
	public Animator animator;
	public Rigidbody2D rigidBody;

	public Transform Figure;
	public ParticleSystem Jetpack;
	public ParticleSystem JetpackLeft;
	public ParticleSystem JetpackRight;

	public AudioClip JetpackAudio;

	public CardManager CardManager;                 // to get score factor from showing cards of a given type
	public GameSettings gameSettings;

	[Header("Health / Fuel")]
	public float HealthLevel;       // 0-MaxHealth
	public float FuelLevel;         // 0-MaxFuel

	public float MaxHealth = 100f;
	public float MaxFuel = 100f;

	private float ThrusterFuelRate = 0.05f;
	private float ThrowFuelRate = 0.025f;

	private float flipTime = 0.5f;                    // face left / right
	private bool flipping = false;                  // to face opposite direction

	[Header("Looking Down")]
	public float PlatformReach;                     // raycast to platform (eg. for flapping animation)
	public Transform RayCastOrigin;                     // raycast to platform (eg. for flapping animation)
	public LayerMask platformLayer;             // max raycast to platform
	public LayerMask groundLayer;                   // raycast to ground
	public LayerMask obstacleLayer;                   // raycast to obstacle
	public LayerMask bombLayer;                   // raycast to bomb

	private string[] landingLayerNames = { "Ground", "Platform", "Obstacle", "Bomb" };
	private LayerMask landingLayers;

	private float sphereCastRadius = 1f;          // for raycast downwards

	//private Platform platformBelow = null;
	//private Obstacle obstacleBelow = null;
	//private Bomb bombBelow = null;
	//private Ground groundBelow = null;
	//private bool lookingDown = false;
	private float lookDownInterval = 0.05f;      // seconds
	private GameObject currentLanding = null;       // according to raycast down

	public float AirDrag = 0.5f;

	private float landingDownForce = 2f;        // to help ensure collision for landing

	[Header("Throwing")]
	public Fruit ThrowFruitPrefab;
	public Transform FruitSpawner;          // parent for instantiated fruit
	public Transform ThrowOrigin;

	private float ThrowDelay = 0.25f;               // if player not flipping before throwing
	private float ThrowFactor = 1f;               // reduce swipe force when throwing fruit
	private float ThrowJetpackLevel = 0.1f;         // small amount of jetpack thrust after throwing

	[Header("Boss")]
	public float BossDamage = 100f;                 // damage inflicted on boss when 'hit'
	public string BossDefeatMessage = "VICTORY!";
	public Color BossDefeatColour = Color.yellow;

	[Header("Take Damage")]
	public SpriteRenderer BossDamageSprite;
	private Vector2 takeDamageSpriteScale;				// set in Start
	private float takeDamageSpriteScaleTime = 0.5f;
	//private float takeDamageScaleFactor = 1.5f;			// figure enlarges briefly

	[Header("Boss Targeting")]
	public SpriteRenderer CrossHair;			// when boss searching

	// direction
	public bool MovingHoriz => Mathf.Abs(rigidBody.velocity.x) > 0f;
	public bool MovingLeft => rigidBody.velocity.x < 0f; //.05f;

	public bool MovingVertically => Mathf.Abs(rigidBody.velocity.y) > 0f;
	public bool MovingUp => rigidBody.velocity.y > 0.05f;

	private bool justLanded = false;        // reset to false on AddForce

	// animation
	private const string FloatTrigger = "Float";
	private const string FallTrigger = "Fall";
	private const string FlapTrigger = "Flap";
	private const string LandingTrigger = "Land";
	private const string IdleTrigger = "Idle";
	private const string ThrowTrigger = "Throw";

	private bool animationChecking = false;
	private float animationCheckInterval = 0.05f;      // seconds

	private GameManager.GameState gameState;
	private bool GameOver => gameState == GameManager.GameState.GameOver;

	private Transportable transportable;        // required component
	private AudioSource audioSource;			// required component
	private Collider2D figureCollider;			// required component

	private MazeZoneVisitor mazeZoneVisitor;

	private MazeZone InMazeZone => (mazeZoneVisitor != null) ? mazeZoneVisitor.InMazeZone : null;


	public enum AnimationState
	{
		None,     
		Idle,			// idle - after land animation
		Floating,		// upwards movement
		Flapping,       // from floating / idle
		Falling,        // 'seen' platform / water below (clamber)
		Landing,		// on collision with platform (if falling)
	}
	[Space]
	public AnimationState CurrentAnimationState;

	public enum Facing
	{
		Left,
		Right
	}
	private Facing currentFacing;


	private void OnEnable()
	{
		GameEvents.OnTapToPlay += OnNewGame;	
		GameEvents.OnStartPlay += OnStartPlay;
		GameEvents.OnGameStateChanged += OnGameStateChanged;
		GameEvents.OnSwipeEnd += OnSwipeEnd;			// add force
		//GameEvents.OnFigureLanded += OnFigureLanded;	
		GameEvents.OnPlayerTakeFruit += OnPlayerTakeFruit;
		GameEvents.OnPlatformExpired += OnPlatformExpired;

		GameEvents.OnBossDefeatTeleport += ResetTeleport;
		GameEvents.OnFigureInCrossHair += OnFigureInCrossHair;
	}

	private void OnDisable()
	{
		GameEvents.OnTapToPlay -= OnNewGame;
		GameEvents.OnStartPlay -= OnStartPlay;
		GameEvents.OnGameStateChanged -= OnGameStateChanged;
		GameEvents.OnSwipeEnd -= OnSwipeEnd;
		//GameEvents.OnFigureLanded -= OnFigureLanded;
		GameEvents.OnPlayerTakeFruit -= OnPlayerTakeFruit;
		GameEvents.OnPlatformExpired -= OnPlatformExpired;

		GameEvents.OnBossDefeatTeleport -= ResetTeleport;
		GameEvents.OnFigureInCrossHair -= OnFigureInCrossHair;
	}


    private void Start()
	{
		transportable = GetComponent<Transportable>();
		audioSource = GetComponent<AudioSource>();      // required component

		mazeZoneVisitor = GetComponent<MazeZoneVisitor>(); 

		SetAnimationState(AnimationState.Falling);
		Stop();
		rigidBody.drag = AirDrag;

		landingLayers = LayerMask.GetMask(landingLayerNames);

		BossDamageSprite.enabled = false;
		takeDamageSpriteScale = BossDamageSprite.transform.localScale;

		if (CrossHair != null)
			CrossHair.enabled = false;
	}

	private void OnNewGame(GameScores scores)
	{
		Stop();
		justLanded = false;
		SetAnimationState(AnimationState.Flapping);

		HealthLevel = MaxHealth;
		FuelLevel = MaxFuel;

		GameEvents.OnPlayerHealthFuelChanged?.Invoke(0, HealthLevel, MaxHealth, 0, FuelLevel, MaxFuel, false);
	}

	private void OnStartPlay()
	{
		StartCoroutine(CheckAnimationState());
	}

	private void OnGameStateChanged(GameManager.GameState currentState, GameManager.GameState newState)
	{
		gameState = newState;
	}

	// land on platform
	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (GameOver)
			return;

		if (transportable.InTransit)
			return;
		
		if (collision.gameObject.CompareTag("Fruit"))
		{
			//Debug.Log($"Platform Collision ENTER: CurrentState = {CurrentState} platformBelow = {platformBelow}");
			var fruit = collision.gameObject.GetComponent<Fruit>();
			GameEvents.OnFigureHitFruit?.Invoke(fruit);         // TODO: not currently used
		}
		else if (collision.gameObject.CompareTag("Ground"))
		{
			//Debug.Log($"Platform Collision ENTER: CurrentState = {CurrentState} platformBelow = {platformBelow}");
			var ground = collision.gameObject.GetComponent<Ground>();

			GameEvents.OnFigureHitGround?.Invoke(ground);
		}
		else if (collision.gameObject.CompareTag("Platform"))
		{
			//Debug.Log($"Platform Collision ENTER: CurrentState = {CurrentState} platformBelow = {platformBelow}");
			var platform = collision.gameObject.GetComponent<Platform>();

			GameEvents.OnFigureHitPlatform?.Invoke(platform);
		}
		else if (collision.gameObject.CompareTag("Obstacle"))
		{
			//Debug.Log($"Platform Collision ENTER: CurrentState = {CurrentState} platformBelow = {platformBelow}");
			var obstacle = collision.gameObject.GetComponent<Obstacle>();

			GameEvents.OnFigureHitObstacle?.Invoke(obstacle);
		}

		// if in maze zone and collide with the zone's collision layer (ie. obstacle), then reset
		// to zone entry reset point
		if (InMazeZone != null && ! mazeZoneVisitor.EjectingFromMaze)		// collider should be disabled but just in case
		{
			//if (HitMazeResetCollisionLayer(collision.gameObject.layer))
			if (collision.gameObject.IsInLayerMasks(mazeZoneVisitor.ResetCollisionLayers))
			{
				InMazeZone.EjectVisitor(mazeZoneVisitor, collision.contacts[0].point);
				SetAnimationState(AnimationState.Floating);

				float healthCost = mazeZoneVisitor.MazeEjectHealthCost;
				float fuelCost = mazeZoneVisitor.MazeEjectFuelCost;

				if (healthCost > 0f)
				{
					HealthLevel -= healthCost;
					HealthLevel = Mathf.Clamp(HealthLevel, 0f, MaxHealth);

					if (HealthLevel <= 0)
						GameEvents.OnZeroHealth?.Invoke();
				}

				if (fuelCost > 0f)
				{
					FuelLevel -= fuelCost;
					HealthLevel = Mathf.Clamp(HealthLevel, 0f, MaxHealth);

					if (FuelLevel <= 0)
						GameEvents.OnZeroFuel?.Invoke();
				}

				// update UI meters (excludes bonus scores)
				if (healthCost > 0f || fuelCost > 0f)
					GameEvents.OnPlayerHealthFuelChanged?.Invoke(-healthCost, HealthLevel, MaxHealth, -fuelCost, FuelLevel, MaxFuel, true);

				if (mazeZoneVisitor.MazeEjectAudio != null)
				{
					audioSource.clip = mazeZoneVisitor.MazeEjectAudio;
					audioSource.Play();
				}
			}
		}
	}

	// bitwise operation to check if the object we hit is in a layer that causes ejection from maze zone
	//private bool HitMazeResetCollisionLayer(int collisionLayer)
	//{
	//	return mazeZoneVisitor.ResetCollisionLayer == (mazeZoneVisitor.ResetCollisionLayer | (1 << collisionLayer));
	//}


	private void OnCollisionExit2D(Collision2D collision)
	{
		if (GameOver)
			return;

		if (collision.gameObject.CompareTag("Platform"))
		{
			//Debug.Log($"Platform Collision EXIT: CurrentAnimationState = {CurrentAnimationState}");

			var platform = collision.gameObject.GetComponent<Platform>();
			if (platform != null)
			{
				platform.FigureResting(false);
			}
		}
	}

	// add physics impulse to figure according to swipe speed and direction
	// validSwipe is false if 'spamming' (ie. excessive swipes per beat)
	private void OnSwipeEnd(Vector2 endPosition, Vector2 direction, float speed, int touchCount, int tapCount)
	{
		if (GameOver)
			return;

		if (transportable.InTransit)
			return;

		if (InMazeZone != null && mazeZoneVisitor.EjectingFromMaze) 
			return;

		if (speed > 0)
		{
			if (touchCount == 1)    
			{
				float fuelUsed = JetpackBoost(direction, speed);

				if (FuelLevel <= 0)
					GameEvents.OnZeroFuel?.Invoke();
				else
					GameEvents.OnPlayerHealthFuelChanged?.Invoke(0, HealthLevel, MaxHealth, -fuelUsed, FuelLevel, MaxFuel, true);
			}
			else if (touchCount == 2)  
			{
				float fuelUsed = Throw(direction, speed);

				// fire jetpack a small amount
				fuelUsed += JetpackBoost(direction, speed * ThrowJetpackLevel);

				if (FuelLevel <= 0)
					GameEvents.OnZeroFuel?.Invoke();
				else
					GameEvents.OnPlayerHealthFuelChanged?.Invoke(0, HealthLevel, MaxHealth, -fuelUsed, FuelLevel, MaxFuel, true);
			}
		}
	}

	private float JetpackBoost(Vector2 direction, float speed)
	{
		//Debug.Log($"OnSwipeEnd: AddForce {direction * impulse}");
		rigidBody.AddForce(direction * speed, ForceMode2D.Impulse);
		justLanded = false;

		//Jetpack.Play();
		JetpackLeft.Play();
		JetpackRight.Play();

		if (JetpackAudio != null)
		{
			audioSource.clip = JetpackAudio;
			audioSource.Play();
		}

		GameEvents.OnFigureImpulse?.Invoke(direction, speed);

		// use fuel
		var fuelUsed = speed * ThrusterFuelRate;
		FuelLevel -= fuelUsed;
		FuelLevel = Mathf.Clamp(FuelLevel, 0f, MaxFuel);

		return fuelUsed;
	}

	private float Throw(Vector2 direction, float speed)
	{
		if (direction.x< 0)
			StartCoroutine(ThrowFruit(Facing.Left, direction, speed* ThrowFactor));
		else
			StartCoroutine(ThrowFruit(Facing.Right, direction, speed* ThrowFactor));

		// use fuel
		var fuelUsed = speed * ThrowFuelRate;
		FuelLevel -= fuelUsed;
		FuelLevel = Mathf.Clamp(FuelLevel, 0f, MaxFuel);

		return fuelUsed;
	}

	// increase player health if fruit not fully rotten, otherwise reduce health
	// increase player fuel level according to fruit fuel level
	private void OnPlayerTakeFruit(float fruitFuel, float fruitHealth, bool isRotten, float fruitToxicity, float timeIncrease, bool isToxic,
					CardManager.CardBackType cardBackType, Color scoreColour, Vector3 fruitPosition, string scoreMessage)
	{
		//Debug.Log($"OnPlayerTakeFruit: fruitFuel = {fruitFuel}, fruitHealth = {fruitHealth}, fruitToxicity = {fruitToxicity}");

		if (GameOver)
			return;

		if (isToxic && !isRotten)
		{
			fruitFuel = -fruitFuel;
			fruitHealth = -fruitHealth;
		}

		// health

		float healthChange = 0f;

		// fruit reduces health once it is totally rotten
		if (isRotten)
			healthChange = -fruitToxicity;
		else		// otherwise it increases it
			healthChange = fruitHealth;

		HealthLevel += healthChange;
		HealthLevel = Mathf.Clamp(HealthLevel, 0f, MaxHealth);

		if (HealthLevel <= 0)
			GameEvents.OnZeroHealth?.Invoke();

		//fuel

		float fuelChange = fruitFuel;

		FuelLevel += fuelChange;
		FuelLevel = Mathf.Clamp(FuelLevel, 0f, MaxFuel);

		if (FuelLevel <= 0)
			GameEvents.OnZeroFuel?.Invoke();

		// update UI meters (excludes bonus scores)
		GameEvents.OnPlayerHealthFuelChanged?.Invoke(healthChange, HealthLevel, MaxHealth, fruitFuel, FuelLevel, MaxFuel, true);

		// update running score + BonusUI feedback
		GameEvents.OnUpdateGameScore?.Invoke(healthChange, fuelChange, timeIncrease,
						CardManager != null ? CardManager.ShowingCardsFactor(cardBackType) : 1,
						scoreColour, fruitPosition, scoreMessage);
	}

	public void TakeBossDamage(float healthDamage, Vector2 contactPoint, bool animate)
	{
		if (GameOver)
			return;

		HealthLevel -= healthDamage;
		HealthLevel = Mathf.Clamp(HealthLevel, 0f, MaxHealth);

		if (animate)
			AnimateBossDamage(contactPoint);

		if (HealthLevel <= 0)
			GameEvents.OnZeroHealth?.Invoke();

		// update UI meters (excludes bonus scores)
		GameEvents.OnPlayerHealthFuelChanged?.Invoke(-healthDamage, HealthLevel, MaxHealth, 0f, FuelLevel, MaxFuel, animate);
	}

	private void AnimateBossDamage(Vector2 contactPoint)
	{
		// animate damage/contact sprite
		BossDamageSprite.transform.localScale = Vector2.zero;
		BossDamageSprite.transform.position = contactPoint;
		BossDamageSprite.enabled = true;

		LeanTween.scale(BossDamageSprite.gameObject, takeDamageSpriteScale, takeDamageSpriteScaleTime * 0.5f)
					.setEaseOutBack()
					.setOnComplete(() =>
					{
						LeanTween.scale(BossDamageSprite.gameObject, Vector2.zero, takeDamageSpriteScaleTime * 0.5f)
								.setEaseInBack()
								.setOnComplete(() =>
								{
									BossDamageSprite.enabled = false;
								});
					});

		// enlarge figure briefly
		//AnimateDamage();
	}

	//private void AnimateDamage()
	//{
	//	Vector2 startScale = transform.localScale;
	//	figureCollider.enabled = false;

	//	LeanTween.scale(gameObject, startScale * takeDamageScaleFactor, takeDamageSpriteScaleTime * 0.5f)
	//					.setEaseOutBack()
	//					.setOnComplete(() =>
	//					{
	//						LeanTween.scale(gameObject, startScale, takeDamageSpriteScaleTime * 0.5f)
	//									.setEaseInBack()
	//									.setOnComplete(() =>
	//									{
	//										figureCollider.enabled = true;
	//									});
	//					});
	//}

	public void BossDefeatBonus(Boss boss)
	{
		TopUpHealthFuel();

		// update running score + BonusUI feedback
		GameEvents.OnUpdateGameScore?.Invoke(boss.BonusScore, 0, 0, 1,
						BossDefeatColour, boss.transform.position, BossDefeatMessage);
	}

	private void TopUpHealthFuel()
	{
		float healthChange = MaxHealth - HealthLevel;
		HealthLevel = MaxHealth;

		float fuelChange = MaxFuel - FuelLevel;
		FuelLevel = MaxFuel;

		// update UI meters (excludes bonus scores)
		GameEvents.OnPlayerHealthFuelChanged?.Invoke(healthChange, HealthLevel, MaxHealth, fuelChange, FuelLevel, MaxFuel, true);
	}

	// end of Landing animation event
	public void OnFigureLanded()
	{
		if (CurrentAnimationState == AnimationState.Landing)
		{
			IdleStop();
		}
	}

	private void IdleStop()
	{
		Stop();
		justLanded = true;
		SetAnimationState(AnimationState.Idle);
	}

	private void OnPlatformExpired(Platform platform)
	{
		if (CurrentAnimationState == AnimationState.Idle)
		{
			SetAnimationState(AnimationState.Floating);		// TODO: was Flapping 
		}
	}


	private void SetAnimationState(AnimationState newState)
	{
		if (newState == CurrentAnimationState)
			return;

		//GameEvents.OnFigureStateChanged?.Invoke(CurrentAnimationState, newState);
		CurrentAnimationState = newState;

		//Debug.Log($"SetAnimationState {CurrentAnimationState.ToString().ToUpper()}");

		switch (CurrentAnimationState)
		{
			case AnimationState.Floating:
				animator.SetTrigger(FloatTrigger);
				justLanded = false;
				break;
			case AnimationState.Flapping:
				animator.SetTrigger(FlapTrigger);
				justLanded = false;
				break;
			case AnimationState.Falling:
				animator.SetTrigger(FallTrigger);
				justLanded = false;
				AddDownForce();
				break;
			case AnimationState.Landing:
				animator.SetTrigger(LandingTrigger);
				justLanded = false;
				AddDownForce();
				break;
			case AnimationState.Idle:
				animator.SetTrigger(IdleTrigger);
				break;
			case AnimationState.None:
				break;
		}
	}

	private void Stop()
	{
		rigidBody.velocity = Vector3.zero;
		//Debug.Log($"Stop rigidBody.gravityScale = {rigidBody.gravityScale}");
	}


	private void AddDownForce()
	{
		//Debug.Log("AddDownForce");
		rigidBody.AddForce(Vector3.down * landingDownForce, ForceMode2D.Impulse);
	}

	private void SetFacing(Facing facing)
	{
		if (currentFacing == facing)
			return;

		if (flipping)
			return;

		currentFacing = facing;
		flipping = true;

		GameEvents.OnFigureFlip?.Invoke(currentFacing);

		LeanTween.rotateAround(Figure.gameObject, Figure.transform.up, 180f, flipTime)
					.setEaseOutBack()
					.setOnComplete(() => { flipping = false; } );
	}


	// quick flip (if required) and throw new fruit
	private IEnumerator ThrowFruit(Facing facing, Vector2 direction, float force)
	{
		if (flipping)
			yield break;

		animator.SetTrigger(ThrowTrigger);

		// flip if not facing direction of swipe
		if (currentFacing != facing)
		{
			currentFacing = facing;
			flipping = true;

			GameEvents.OnFigureFlip?.Invoke(currentFacing);

			LeanTween.rotateAround(Figure.gameObject, Figure.transform.up, 180f, flipTime * 0.5f)
						.setEaseOutBack()
						.setOnComplete(() =>
						{
							flipping = false;
							ThrowNewFruit(direction, force);
						});
		}
		else
		{
			yield return new WaitForSeconds(ThrowDelay);
			ThrowNewFruit(direction, force);
		}

		yield return null;
	}


	private void ThrowNewFruit(Vector2 direction, float force)
	{
		var newFruit = Instantiate(ThrowFruitPrefab, FruitSpawner);
		newFruit.SetColour();
		newFruit.Throw(ThrowOrigin.position, direction, force);
	}


	private IEnumerator CheckAnimationState()
	{
		if (animationChecking)
			yield break;

		animationChecking = true;
		bool landingBelowChanged = false;

		while (animationChecking)
		{
			if (MovingVertically)
			{
				if (MovingUp)
				{
					SetAnimationState(AnimationState.Floating);     // from any state
					currentLanding = null;
				}
				else        // moving downwards
				{
					if (LookDownForLanding(out landingBelowChanged))		// landing below
					{
						if (CurrentAnimationState == AnimationState.Flapping || CurrentAnimationState == AnimationState.Floating) 
							SetAnimationState(AnimationState.Falling);
						else if (landingBelowChanged) // && CurrentAnimationState != AnimationState.Landing)
							SetAnimationState(AnimationState.Flapping); // from any state (=> Falling next time round loop?)

						// else no animation change (eg. Landing => Idle via animaton event)
					}
					else		// nothing below!
					{
						SetAnimationState(AnimationState.Flapping);     // from any state
					}
				}
			}

			if (! justLanded && MovingHoriz)
			{
				if (MovingLeft)
					SetFacing(Facing.Left);
				else
					SetFacing(Facing.Right);
			}

			yield return new WaitForSeconds(lookDownInterval);
		}

		yield return null;
	}


	private bool LookDownForLanding(out bool landingChanged)
	{
		// raycast to 'see' landing object below
		RaycastHit2D landingRaycastHit = Physics2D.CircleCast(RayCastOrigin.position, sphereCastRadius, -transform.up, PlatformReach, landingLayers);

		if (landingRaycastHit.collider != null)
		{
			//Debug.Log($"LookDownForLanding hit {landingRaycastHit.collider.name}");
			if (landingRaycastHit.collider.gameObject != currentLanding)
			{
				currentLanding = landingRaycastHit.collider.gameObject;
				landingChanged = true;
			}
			else
				landingChanged = false;

			return true;
		}
		else        // no landing below
		{
			currentLanding = null;
			landingChanged = false;
			return false;
		}
	}


	//private IEnumerator CheckAnimationState()
	//{
	//	if (animationChecking)
	//		yield break;

	//	animationChecking = true;
	//	bool landingBelow;

	//	while (animationChecking)
	//	{
	//		//if (!justLanded)
	//		{
	//			if (MovingVertically)
	//			{
	//				if (MovingUp)
	//				{
	//					SetAnimationState(AnimationState.Floating);     // from any state
	//					//justLanded = false;
	//				}
	//				else		// moving downwards
	//				{
	//					if (CurrentAnimationState != AnimationState.Landing)        // allow to land (to idle)
	//						//if (!justLanded)
	//					{
	//						platformBelow = null;
	//						obstacleBelow = null;
	//						bombBelow = null;
	//						groundBelow = null;

	//						if (! (landingBelow = LookDownForPlatform()))
	//						{
	//							if (! (landingBelow = LookDownForObstacle()))
	//							{
	//								if (! (landingBelow = LookDownForBomb()))
	//								{
	//									landingBelow = LookDownForGround();
	//								}
	//							}
	//						}

	//						bool canFall = CurrentAnimationState == AnimationState.Floating ||
	//												CurrentAnimationState == AnimationState.Flapping;

	//						//if (platformBelow != null || obstacleBelow != null || bombBelow != null || groundBelow != null)
	//						if (landingBelow && canFall)
	//						{
	//							//if (canFall)
	//							//if (CurrentAnimationState != AnimationState.Landing)  // allow to land (to idle)
	//							{
	//								SetAnimationState(AnimationState.Falling);      // => Landing on feet trigger entry
	//							}
	//						}
	//						else if (CurrentAnimationState != AnimationState.Falling)    // moving down and nothing below!
	//						{
	//							SetAnimationState(AnimationState.Flapping);     // from any state
	//						}
	//					}
	//				}
	//			}

	//			if (!justLanded && MovingHoriz)
	//			{
	//				if (MovingLeft)
	//					SetFacing(Facing.Left);
	//				else
	//					SetFacing(Facing.Right);
	//			}
	//		}

	//		yield return new WaitForSeconds(lookDownInterval);
	//	}

	//	yield return null;
	//}


	//private bool LookDownForPlatform()
	//{
	//	// raycast to 'see' platform below
	//	RaycastHit2D platformRaycastHit = Physics2D.CircleCast(RayCastOrigin.position, sphereCastRadius, -transform.up, PlatformReach, platformLayer);

	//	if (platformRaycastHit.collider != null)         // platform just below!
	//	{
	//		platformBelow = platformRaycastHit.collider.GetComponent<Platform>();
	//		//Debug.Log($"LookDownForPlatform {platformBelow.name}");
	//		return true;
	//	}
	//	return false;
	//}

	//private bool LookDownForObstacle()
	//{
	//	// raycast to 'see' obstacle below
	//	RaycastHit2D obstacleRaycastHit = Physics2D.CircleCast(RayCastOrigin.position, sphereCastRadius, -transform.up, PlatformReach, obstacleLayer);

	//	if (obstacleRaycastHit.collider != null)         // obstacle just below!
	//	{
	//		obstacleBelow = obstacleRaycastHit.collider.GetComponent<Obstacle>();
	//		//Debug.Log($"LookDownForObstacle {obstacleBelow.name}");
	//		return true;
	//	}
	//	return false;
	//}

	//private bool LookDownForBomb()
	//{
	//	// raycast to 'see' obstacle below
	//	RaycastHit2D bombRaycastHit = Physics2D.CircleCast(RayCastOrigin.position, sphereCastRadius, -transform.up, PlatformReach, bombLayer);

	//	if (bombRaycastHit.collider != null)         // bomb just below!
	//	{
	//		bombBelow = bombRaycastHit.collider.GetComponent<Bomb>();
	//		//Debug.Log($"LookDownForObstacle {obstacleBelow.name}");
	//		return true;
	//	}
	//	return false;
	//}

	//private bool LookDownForGround()
	//{
	//	// raycast to 'see' ground below
	//	RaycastHit2D groundRaycastHit = Physics2D.CircleCast(RayCastOrigin.position, sphereCastRadius, -transform.up, PlatformReach, groundLayer);

	//	if (groundRaycastHit.collider != null)         // ground just below!
	//	{
	//		groundBelow = groundRaycastHit.collider.GetComponent<Ground>();
	//		//Debug.Log($"LookDownForGround {groundBelow.name}");
	//		return true;
	//	}
	//	return false;
	//}


	// called by LandingCollider (feet OnTriggerEnter)
	public void Land(Collider2D collision)
	{
		if (CurrentAnimationState != AnimationState.Falling)
			return;

		if (collision.gameObject.CompareTag("Ground"))
		{
			SetAnimationState(AnimationState.Landing);
		}
		else if (collision.gameObject.CompareTag("Platform"))
		{
			SetAnimationState(AnimationState.Landing);

			var platform = collision.gameObject.GetComponent<Platform>();
			platform.FigureResting(true);
		}
		else if (collision.gameObject.CompareTag("Obstacle"))
		{
			SetAnimationState(AnimationState.Landing);
		}
		else if (collision.gameObject.CompareTag("Bomb"))
		{
			SetAnimationState(AnimationState.Landing);
		}
	}

	public void ResetTeleport(PortalEndPoint portalEntry)
	{
		transportable.Teleport(portalEntry);
		GameEvents.OnMoonFruitTeleport?.Invoke();
	}


	private void OnFigureInCrossHair(bool active, StickFigure targeted, LaserSource laserSource)
	{
		if (CrossHair != null)
		{
			if (active)		// only activate if targeted
				CrossHair.enabled = this == targeted;
			else
				CrossHair.enabled = false;
		}
	}

	private void OnDrawGizmos()
	{
		// raycast
		Gizmos.color = Color.green;
		Gizmos.DrawRay(RayCastOrigin.position,  -transform.up * PlatformReach);
		Gizmos.DrawWireSphere(RayCastOrigin.position - transform.up * PlatformReach, sphereCastRadius);
	}
}
